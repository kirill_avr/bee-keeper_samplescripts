﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Shorka.CellConnect
{
	public class PathFinding : IInitializable
	{
//		private readonly SceneSettings scSettings;
		private readonly GridInitiator gridInit;
		private int lenCells;
		public PathFinding(GridInitiator gridInit)
		{
//			this.scSettings = scSettings;
			this.gridInit = gridInit;
		}

		public void Initialize()
		{
			lenCells = gridInit.ArrCells.Length;
		}

		public List<CellBehaviour> FindPath(CellBehaviour startCell, CellBehaviour targetCell)
		{
			Heap<CellBehaviour> openSet = new Heap<CellBehaviour>(lenCells);
			HashSet<CellBehaviour> closedSet = new HashSet<CellBehaviour>();
			openSet.Add(startCell);

			int iter = 0;

			while(openSet.Count > 0)
			{
//				Debug.Log("Iteration# ".StrItalics().StrColored(DebugColors.cyan) + iter);
				iter++;
				if(iter > 140)
				{
					Debug.LogError("Cycle more than 140 times in while FindPath".StrColored(DebugColors.red));
					break;
				}

				CellBehaviour cell = openSet.RemoveFirst();

				closedSet.Add(cell);

				if (cell == targetCell)
				{
//					Debug.Log("Retutrn: ".StrBold() + cell.name);
					return RetracePath(startCell,targetCell);
				}


				//Cycle through neighbours
				int neighborsQTY = cell.GetNeighbors.Count;
				for (int i = 0; i < neighborsQTY; i++)
				{
					CellBehaviour cellNeighbor = cell.GetNeighbors[i];
					//Debug.Log("neighbour: ".StrColored(DebugColors.blue).StrBold() + cellNeighbor.name);
					if (!cellNeighbor.walkable)
					{
						//Debug.Log("cellNeighbor.is NOT Walkable ".StrColored(DebugColors.red) +  cellNeighbor.name);
						continue;
					}

					if(closedSet.Contains(cellNeighbor))
					{
						//Debug.Log("cellNeighbor.is In ClosedSet ".StrColored(DebugColors.red) +  cellNeighbor.name);
						continue;
					}
					float newCostToNeighbour = cell.gCost + GetDistance(cell, cellNeighbor);
					if (newCostToNeighbour < cellNeighbor.gCost || !openSet.Contains(cellNeighbor)) 
					{
						cellNeighbor.gCost = newCostToNeighbour;
						cellNeighbor.hCost = GetDistance(cellNeighbor, targetCell);

//						string result = string.Format("SetParent to NodeXY:".StrColored(DebugColors.maroon) +"{0}" + 
//							" \nParent is: ".StrColored(DebugColors.navy) + "{1}  ",
//							cellNeighbor.name,
//							cell.name);
						//	Debug.Log(result);

						cellNeighbor.parentNodeCell = cell;
						if (!openSet.Contains(cellNeighbor))
						{
							//Debug.Log("Add neighbour to openSet: ".StrColored(DebugColors.brown).StrBold() + cellNeighbor.name);
							openSet.Add(cellNeighbor);
						}	

						else 
							openSet.UpdateItem(cellNeighbor);					

					}
				}
					
			}

			return null;
		}


		private List<CellBehaviour> RetracePath(CellBehaviour startCell, CellBehaviour endCell)
		{
			List<CellBehaviour> path = new List<CellBehaviour>();
			CellBehaviour currentCell = endCell;

			int countHelper = 0;
			while (currentCell != startCell) 
			{
				countHelper++;
				if(countHelper > 100)
				{
					Debug.LogError("Cycle more than 100 times in while RetracePath".StrColored(DebugColors.red));
					break;
				}
//				Debug.Log("path.Add: ".StrColored(DebugColors.blue) + currentCell.name);
				path.Add(currentCell);
				currentCell = currentCell.parentNodeCell;
			}
			path.Reverse();
			//		Debug.Log("Count : " + path.Count);
//			scSettings.gizmosDrawer.path = path;
//			GizmosDrawer.instance.path = path;
			return path;
		}


		private float GetDistance(CellBehaviour cellA, CellBehaviour cellB) 
		{
			return Vector2.Distance(cellA.PosWorld, cellB.PosWorld);
		}

//		[System.Serializable]
//		public class SceneSettings
//		{
//			public GizmosDrawer gizmosDrawer;

//		}
	}
}