﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;
using MovementEffects;

namespace Shorka.CellConnect
{
	public class CellBehaviour : MonoBehaviour, IHeapItem<CellBehaviour>
	{
		#region Fields
		[SerializeField] private TextMesh txt;
		[SerializeField] private SpriteRenderer spriteRen;
		[SerializeField] private CellType cellType;
		[SerializeField] private CellDescription cellDescription;

		[HideInInspector] public bool walkable;
		[HideInInspector] public float gCost;
		[HideInInspector] public float hCost;
//		[HideInInspector] public int index;

		[HideInInspector] public CellBehaviour parentNodeCell;

		private Transform transTxt;
		private int scoreNumber;

		private float initAlphaCell;
		private float initAlphaTxt;
		private Vector3 initLocalScale;
		private Vector3 initTxtLocalPos;
		private Vector3 initTxtLocalScale;

		private List<CellBehaviour> neighbours;
		private Sequence seqCell;
		private CellType nextCellType = CellType.None;
		private Color cellColorForTxt;
		private Vector3 startScale;
		private int heapIndex;
		#endregion

		#region Properties
		public TextMesh Txt {  get { return txt; }}

		/// <summary>
		/// Gameobject component of cell text
		/// </summary>
		public GameObject TxtGamObj {  get { return txt.transform.gameObject; }}

		public CellType GetCellType {  get { return cellType; }}
		public int ScoreNumber {  get { return scoreNumber; }}

		public SpriteRenderer SpriteRen {  get { return spriteRen; }}

		public float FCost {get {return gCost + hCost;} }
		public Vector3 PosWorld {  get { return transform.position; }}
		public Vector3 PosWorldOfTxt {  get { return transTxt.position; }}

		public List<CellBehaviour> GetNeighbors {  get { return neighbours; }}
		public List<CellBehaviour> SetNeighbors {  set {  neighbours = value; }}
		public Color CellColorForTxt  {  get { return cellColorForTxt; }}
		public Color CellColor {  get { return spriteRen.color; }}
		public string TextDisplayed {  get { return txt.text; }}

		public bool IsAnimed 
		{ 
			get
			{ 
				if(seqCell == null)
					return false;

				return seqCell.IsActive();
					
			}}

		public int HeapIndex 
		{
			get { return heapIndex;	}
			set { heapIndex = value; }
		}

		public int CompareTo(CellBehaviour nodeToCompare) 
		{
			int compare = FCost.CompareTo(nodeToCompare.FCost);
			if (compare == 0) 
				compare = hCost.CompareTo(nodeToCompare.hCost);
			
			return -compare;
		}

		public CellType NextCellType { get {return nextCellType; } }

		public bool IsInitOpacity {	get	{ return spriteRen.color.a == initAlphaCell; } }
		#endregion

		void Awake()
		{
//			List<int> xy = Tools.ConvertToIntArray(transform.name);
//			if(xy.Count >= 2)
//			{
//				xCoor = xy[0];
//				yCoor = xy[1];
//			}
			transTxt = txt.transform;

//			txt.font = cellDescription.FontInCell;
			initLocalScale = transform.localScale;
			startScale = transform.localScale;
			initAlphaCell = spriteRen.color.a;

			initAlphaTxt = txt.color.a;
			initTxtLocalPos = transTxt.localPosition;
			initTxtLocalScale = transTxt.localScale;
//			Debug.Log("initTxtLocalPos: ".StrColored(DebugColors.lime).StrBold() + initTxtLocalPos);
		}

		void OnDisabled()
		{
			if(seqCell!=null)
				seqCell.Kill();
		}

		public void RefreshCellTypeProps()
		{
			//WoodCell isn't part of cellElement
			if(cellType == CellType.WoodCell)
			{
				walkable = false;
				TxtGamObj.SetActive(false);
				spriteRen.sprite = cellDescription.GetRandomWoodSprite;
				nextCellType = cellType;
				return;
			}

			CellDescription.CellElement cellElem = cellDescription.PickCellElement(cellType);
			if(cellElem ==null)
			{
				Debug.LogError("cellElem is NULL");
				return;
			}

//			spriteRen.color = cellElem.CellColor;
			spriteRen.sprite = cellElem.Pic;
			spriteRen.color = spriteRen.color.GetColorAlpha(cellDescription.InitAlphaCell);
			nextCellType = cellElem.NextCellType;

			if(cellType == CellType.Grid)
			{
				walkable = true;
				TxtGamObj.SetActive(false);
				return;
			}

			cellColorForTxt = cellElem.CellColor;

			if(cellDescription.IsEnableTxt)
			{
				TxtGamObj.SetActive(true);
				txt.text = cellElem.NumberInText.ToString();
				txt.fontSize = cellElem.FontSize;

				if(cellDescription.IsOneCellTxtColor)
				{					
					txt.color = cellDescription.IsCellTypeCanChangeTxtColor(cellType) ? 
						cellDescription.ColCellTxtGeneral : cellElem.ColorTxt;
				}
				else
					txt.color =  cellElem.ColorTxt;			


				SetInitTxtPos();
			}			

			scoreNumber = cellElem.NumberInText;
			walkable = false;
		}

		/// <summary>
		/// Set initial alpha color chanel of cell and it's text
		/// </summary>
		public void SetInitAlpha()
		{
			spriteRen.color = spriteRen.color.GetColorAlpha(initAlphaCell);
			txt.color = spriteRen.color.GetColorAlpha(initAlphaTxt);
		}

		/// <summary>
		/// Set desired CellType to cell and refresh all properties
		/// </summary>
		public void SetCellType(CellType desiredCellType)
		{
			cellType = desiredCellType;
			RefreshCellTypeProps();
		}

		public void SetInitTxtPos()
		{
			if(transTxt == null)
				return;

			if(transTxt.parent != transform)
				transTxt.parent = transform;

			if(cellType == CellType.Num_4)
			{				
				Vector2 offSet = cellDescription.OffSetForCellType(cellType);
				Vector3 pos = initTxtLocalPos;
				pos.x += offSet.x;
				pos.y += offSet.y;
				transTxt.localPosition = pos;
			}
			else
			{
				transTxt.localPosition = initTxtLocalPos;
			}
		}

		public void SetCellTypeAndInit(CellType desiredCellType)
		{
			if(IsAnimed)
			{
				SetInitScale();
			}
			SetInitAlpha();

			cellType = desiredCellType;
			RefreshCellTypeProps();
		}

		#region Anim methods
		public void StartAnimScale()
		{
			AnimSize(cellDescription.ShrinkFactor, cellDescription.ShrinkDur);
		}

		public void BlockAnim()
		{
			AnimSize(cellDescription.BlockFactor, cellDescription.BlockDur);
		}

		public void AppearenceAnim()
		{
			Timing.RunCoroutine(IEAppearenceAnim());
		}

		private IEnumerator<float> IEAppearenceAnim()
		{
			transTxt.parent = null;
			transform.SetLocalScale2D(0,0);
			transform.DOScale(startScale, cellDescription.AppearDur);
			txt.SetColorAlpha(0);
			Timing.RunCoroutine(Tools.IEChangeColor(txt, txt.color.GetColorAlpha(1), cellDescription.AppearDur));
			yield return Timing.WaitForSeconds(cellDescription.AppearDur);
			transTxt.parent = transform;
			SetInitTxtPos();
			transTxt.localScale = initTxtLocalScale;
		}

		public void EndAnim()
		{
			spriteRen.DOColor(spriteRen.color.GetColorAlpha(0), cellDescription.ExitDur);
			Timing.RunCoroutine(Tools.IEChangeColor(txt, txt.color.GetColorAlpha(0), cellDescription.ExitDur));
		}

		#endregion

		private void AnimSize(Vector2 factor, float duration)
		{
			if(seqCell != null)
			{
				if(seqCell.IsActive())
					return;
			}

			seqCell = DOTween.Sequence();
			seqCell.SetLoops(-1);

			seqCell.Append(transform.DOScale(	
				initLocalScale.ReturnMultByCoeffXY(factor)
				, cellDescription.ShrinkDur)
			);
			seqCell.Append(transform.DOScale(initLocalScale,duration));
		}

		public void SetInitScale()
		{
			if(seqCell!=null)
			{
				seqCell.Kill();
			}			
			
			transform.localScale = initLocalScale;
		}

		public void SetPos2D(Vector3 pos)
		{
			transform.position = new Vector3(pos.x, pos.y, transform.position.z);
		}

		public void SetPos2D(float xPos, float yPos)
		{
			transform.SetPos2D(xPos, yPos);
		}
		public void ChangeOpacity(float alpha, float duration)
		{
			spriteRen.DOColor(spriteRen.color.GetColorAlpha(alpha), duration);

//			StartCoroutine(
//				Tools.IEChangeAlpha(txt, alpha, duration)
//			);
//			txt.color = txt.color.GetColorAlpha(alpha);
			Timing.RunCoroutine(Tools.IEChangeAlpha(txt, alpha, duration));
		}

		public void SmoothlyToGrid(float timeFade, float wTimeAfterFade, float timeBackInNormal)
		{
			Timing.RunCoroutine(
				IESmoothlyToGrid(timeFade, wTimeAfterFade, timeBackInNormal)
			);
		}

		public void SmoothlyToGrid(FadeToGridSettings fadeToGridSett)
		{
			Timing.RunCoroutine(
				IESmoothlyToGrid(fadeToGridSett.TimeFade, fadeToGridSett.WTimeAfterFade, fadeToGridSett.TimeBackInNormal)
			);
		}

		private IEnumerator<float> IESmoothlyToGrid(float timeFade, float wTimeAfterFade, float timeBackInNormal)
		{
			ChangeOpacity(0,timeFade);
			yield return Timing.WaitForSeconds(wTimeAfterFade);
			//			Debug.Log("After expl");
			SetCellType(CellType.Grid);
			spriteRen.SetColorAlpha(0);
			ChangeOpacity(1, timeBackInNormal);

			SetInitScale();
		}
	}

	[System.Serializable]
	public class FadeToGridSettings
	{
		[SerializeField] private float timeFade = 0.5F;
		[SerializeField] private float wTimeAfterFade = 0.5F;
		[SerializeField] private float timeBackInNormal = 1F;

		public float TimeFade {  get { return timeFade; }}
		public float WTimeAfterFade {  get { return wTimeAfterFade; }}
		public float TimeBackInNormal {  get { return timeBackInNormal; }}

	}
}