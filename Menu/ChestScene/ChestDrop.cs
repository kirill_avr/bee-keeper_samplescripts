﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

namespace Shorka.CellConnect
{
	public class ChestDrop : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
	{
		[SerializeField] private KeyType keyType;
		[SerializeField] private GameObject chestIcon;
		[SerializeField] private DraggedKey dragKey;
		[SerializeField] private ChestPicsSettings chestPics;

		private Animator animator;
		private Image imgChest;

		public KeyType KeyType {  get { return keyType; }}
		public Vector3 Position {  get { return transform.position; }}

		void Awake()
		{	
			animator = GetComponent<Animator>();
			animator.enabled = false;
			CheckImgForInit();
		}

		public void OnPointerEnter(PointerEventData data)
		{
			if (data.pointerDrag == null)
				return;			

			if(animator.enabled)
				animator.enabled = false;
			
//			Debug.Log("OnPointerEnter".StrColored(DebugColors.blue));

			//Condition prevents for opening already opened chests
			if(imgChest.sprite != chestPics.PicChestClose)
			{
				return;
			}

			imgChest.sprite = dragKey.keyType == keyType ?  
				chestPics.PicChestHoverYes : chestPics.PicChestHoverNo;
		}

		public void OnPointerExit(PointerEventData data)
		{
			if (data.pointerDrag == null)
				return;		

			if(animator.enabled)
				animator.enabled = false;
			
			Debug.Log("OnPointerExit".StrColored(DebugColors.red));
			if(imgChest.sprite == chestPics.PicChestHoverYes || imgChest.sprite == chestPics.PicChestHoverNo)
			{
				imgChest.sprite = chestPics.PicChestClose;
				return;
			}

//			if(imgChest.sprite != chestPics.PicChestClose)
//			{
//				imgChest.sprite = chestPics.PicChestClose;
//			}
		}

		public void OnDrop(PointerEventData data)
		{
			if (data.pointerDrag != null)
			{
				Debug.Log ("Dropped object was: ".StrColored(DebugColors.blue)  + data.pointerDrag.name);
			}


			if(dragKey.keyType == keyType)
			{
				Debug.Log("Drop".StrColored(DebugColors.green));

				dragKey.SetActive(false);
				MenuEventsChests.OnOpenChest(keyType);
			}
			else
			{
				MenuEventsChests.OnWrongKey();
//				imgChest.sprite = chestPics.PicChestClose;
				Debug.Log("Wrong key".StrColored(DebugColors.red));
				if(imgChest.sprite == chestPics.PicChestHoverNo)
				{
					imgChest.sprite = chestPics.PicChestClose;
				}
			}
		}

		/// <summary>
		/// Sets active/deactive icon above chest
		/// </summary>
		public void SetIconActive(bool isActive)
		{
			chestIcon.SetActive(isActive);
		}

		public void PlayAnim()
		{
			animator.enabled = true;
			animator.SetBool(Constants.strCanAnim, true);
		}

		public void StopAnim()
		{
			animator.SetBool(Constants.strCanAnim, false);
		}

		public void SetSprite(Sprite sprite)
		{
			CheckImgForInit();
			imgChest.sprite = sprite;
		}

		private void CheckImgForInit()
		{
			if(imgChest == null)
				imgChest = GetComponent<Image>();
		}
	}
}