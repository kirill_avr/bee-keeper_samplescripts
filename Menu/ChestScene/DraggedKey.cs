﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Shorka.CellConnect
{
	/// <summary>
	/// Class the represend dragged key object.
	/// </summary>
	/// <remarks>Must be only one in herechy</remarks>  
	public class DraggedKey : MonoBehaviour
	{

		public KeyType keyType;
		[SerializeField] private Image imgChKey;
		[SerializeField] private RectTransform rectChKey;
		[SerializeField] private KeysWarehouse keyWH;

		public RectTransform RectTrans {  get { return rectChKey; }}
		public bool IsActive {  get { return gameObject.activeInHierarchy; }}

		public void SetSprite(Sprite sprite)
		{
			imgChKey.sprite = sprite;
		}

		public void SetActive(bool isActive)
		{
			imgChKey.gameObject.SetActive(isActive);
		}

		public void SetActiveAndType(bool isActive, KeyType keyType)
		{
			this.keyType = keyType;
			imgChKey.gameObject.SetActive(isActive);
			imgChKey.sprite = keyWH.GetKeyChest(keyType).KeyOn;
		}

		public void SetPos2D(Vector3 pos)
		{
			rectChKey.SetPos2D(pos);
		}
	}
}