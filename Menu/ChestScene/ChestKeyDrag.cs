﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using MovementEffects;

namespace Shorka.CellConnect
{
	public class ChestKeyDrag : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
	{
		[SerializeField] private KeyType keyType;
		[SerializeField] private DraggedKey dragKey;
		[SerializeField] private KeysWarehouse keyWH;
		[SerializeField] private Image imgChKey;
		[SerializeField] private Image imgHolder;

		private KeyItemAdvanced keyitem;
		private Vector2 initPos;
		public KeyType KeyType {  get { return keyType; }}

//		public DraggedKey DraggedKeyObj {  get { return dragKey; }}
//		public Image ImgChKey {  get { return imgChKey; }}
		public Vector3 PosKey {  get { return imgChKey.transform.position; }}


		void Start()
		{	
			Debug.Log("ChestKeyDrag: Start" );
			InitKeyitem();
		}

		#region Drag methods

		public void OnBeginDrag(PointerEventData eventData)
		{
			Debug.Log("OnBeginDrag: ".StrColored(DebugColors.maroon) + eventData.pointerDrag.name);

			SetKeyOn(false);
			dragKey.SetActive(true);
			dragKey.transform.SetAsLastSibling();
		}

		public void OnDrag(PointerEventData eventData)
		{
			dragKey.SetPos2D(eventData.position);

			Vector3 globalMousePos;
			if (RectTransformUtility.ScreenPointToWorldPointInRectangle(dragKey.RectTrans,
				eventData.position, eventData.pressEventCamera, out globalMousePos))
			{
				dragKey.SetPos2D(globalMousePos);
				dragKey.keyType = keyType;
//				rt.rotation = m_DraggingPlanes[eventData.pointerId].rotation;
			}
		}	

		public void OnEndDrag(PointerEventData eventData)
		{
//			Debug.Log("OnEndDrag".StrColored(DebugColors.maroon));
			Timing.RunCoroutine(CheckForEndDrag());
		}

		private IEnumerator<float> CheckForEndDrag()
		{
			yield return Timing.WaitForOneFrame;
			if(dragKey.IsActive)
			{
				dragKey.SetActive(false);
				SetKeyOn(true);
			}

		}
		#endregion

		public void EnableRayCast(bool enableRaycast)
		{
			imgHolder.raycastTarget = enableRaycast;
		}

		public void SetKeyOn(bool isOn)
		{
			InitKeyitem();
			imgChKey.sprite = isOn ? keyitem.KeyOn : keyitem.KeyOff;
		}

		private void InitKeyitem()
		{
			if(keyitem != null)
				return;

			keyitem = keyWH.GetKeyChest(keyType);
		}
	}
}