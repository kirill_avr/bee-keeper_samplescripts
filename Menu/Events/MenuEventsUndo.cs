﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Shorka.CellConnect
{
	public class MenuEventsUndo : MonoBehaviour {

		public static Action OnBtnRewardedAd = delegate { };
		public static Action OnBtnCoinsUse = delegate { };
		public static Action OnBtnClose = delegate { };

		public void BtnRewardedAd ()
		{
			OnBtnRewardedAd();
		}
		public void BtnCoinsUse ()
		{
			OnBtnCoinsUse();
		}
		public void BtnClose ()
		{
			OnBtnClose();
		}
	}
}