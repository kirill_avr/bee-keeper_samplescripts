﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
namespace Shorka.CellConnect
{
	public class MenuEventsDevPanel : MonoBehaviour
	{
		public static Action <int> OnLevelAdd = delegate(int levelAdd) { };
		public static Action <int> OnAddMagicStars = delegate(int magicStarsAdd) { };
		public static Action <int> OnSessions = delegate(int sessions ) { };
		public static Action <int> OnAddHighScore = delegate(int highScore ) { };
		public static Action OnDeleteAll = delegate { };
		public static Action OnBtnBack = delegate { };
		public static Action OnMakeHighScoreNull = delegate { };

		public void MakeHighScoreNull()
		{
			MakeHighScoreNull();
		}

		public void AddLevel(int levelAdd)
		{
			OnLevelAdd(levelAdd);
		}

		public void AddMagicStars(int magicStarsAdd)
		{
			OnAddMagicStars(magicStarsAdd);
		}

		public void AddSessions(int sessions)
		{
			OnSessions(sessions);
		}

		public void AddHighScore(int highScore)
		{
			OnAddHighScore(highScore);
		}

		public void DeleteAll()
		{
			OnDeleteAll();
		}

		public void BtnBack()
		{
			OnBtnBack();
		}
	}
}