﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Shorka.CellConnect
{
	public class MenuTestEvents 
	{

		public static Action OnAnimShakeColorBar = delegate { };

		public static Action<PowerUpTypes> OnOpenPUTutorialWindow = delegate(PowerUpTypes puType) { };

		public static Action<PowerUpTypes> OnUnlockAnim = delegate (PowerUpTypes puType) { };
	}
}