﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Shorka.CellConnect
{
	public class MenuEventsRestartGame : MonoBehaviour
	{
		public static Action OnBtnYes = delegate { };
		public static Action OnBtnNo = delegate { };

		public void BtnYes()
		{
			OnBtnYes();
		}

		public void BtnNo()
		{
			OnBtnNo();
		}
	}
}