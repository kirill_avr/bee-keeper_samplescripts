﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Shorka.CellConnect
{
	public class MenuEventsHome : MonoBehaviour
	{
		public static Action OnBtnPlay = delegate { };
		public static Action OnBtnSettings = delegate { };
		public static Action OnBtnShop = delegate { };
		public static Action OnBtnLeaderBoard = delegate { };
		public static Action OnBtnAchiev = delegate { };

		public void BtnPlay()
		{
			OnBtnPlay();
		}

		public void BtnSettings()
		{
			OnBtnSettings();
		}

		public void BtnShop()
		{
			OnBtnShop();
		}

		public void BtnLeaderBoard()
		{
			OnBtnLeaderBoard();
		}

		public void BtnAchiev()
		{
			OnBtnAchiev();
		}
	}

	public class MenuTestEventsHome 
	{
		public static Action OnBeesAppear = delegate { };
	}
}