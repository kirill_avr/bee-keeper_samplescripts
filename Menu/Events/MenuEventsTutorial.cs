﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Shorka.CellConnect
{
	public class MenuEventsTutorial : MonoBehaviour
	{

		public static Action OnBtnOKTutorial = delegate { };
		public static Action OnBtnSkipTutorial = delegate { };

		public void BtnSkipTutorial()
		{
			OnBtnSkipTutorial();
		}

		public void BtnOKTutorial()
		{
			OnBtnOKTutorial();
		}
	}
}