﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Shorka.CellConnect
{
	public class MenuEventsSettings : MonoBehaviour 
	{

		public static Action OnBtnBack = delegate { };
		public static Action OnBtnSoundSlider = delegate { };
		public static Action<string> OnBtnLanguage= delegate(string id)  { };
		public static Action OnBtnLikeFB = delegate { };
		public static Action OnBtnRestore = delegate { };
		public static Action OnBtnRateUS = delegate { };
		public static Action OnBtnBunnyBanner = delegate { };
		public static Action OnBtnTutorial = delegate { };
		public static Action OnBtnToDevPanel = delegate { };

		public void BtnToDevPanel()
		{
			OnBtnToDevPanel();
		}

		public void BtnTutorial()
		{
			OnBtnTutorial();
		}

		public void BtnBunnyBanner()
		{
			OnBtnBunnyBanner();
		}

		public void BtnBack()
		{
			OnBtnBack();
		}

		public void BtnSoundSlider()
		{
			OnBtnSoundSlider();
		}

		public void BtnLanguage(string id)
		{
			OnBtnLanguage(id);
		}

		public void BtnLikeFB()
		{
			OnBtnLikeFB();
		}

		public void BtnRestore()
		{
			OnBtnRestore();
		}

		public void BtnRateUS()
		{
			OnBtnRateUS();
		}
	}
}