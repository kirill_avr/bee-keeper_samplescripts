﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Shorka.CellConnect
{
	public class MenuEventsOutOf : MonoBehaviour
	{
		public static Action OnBtnCloseOfPU = delegate { };
		public static Action OnBtnGetPUinOutOfPU = delegate { };

		public static Action OnBtnBuy = delegate { };
		public static Action OnBtnAd = delegate { };
		public static Action OnBtnShopForMore = delegate { };

		public void BtnCloseOfPU()
		{
			OnBtnCloseOfPU();
		}

		public void BtnGetPUinOutOfPU()
		{
			OnBtnGetPUinOutOfPU();
		}

		public void BtnBuy()
		{
			OnBtnBuy();
		}

		public void BtnAd()
		{
			OnBtnAd();
		}

		public void BtnShopForMore()
		{
			OnBtnShopForMore();
		}
	}
}