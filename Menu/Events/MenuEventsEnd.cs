﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Shorka.CellConnect
{
	public class MenuEventsEnd : MonoBehaviour
	{
		public static Action OnBtnWatchAd = delegate { };
		public static Action OnBtnReplay = delegate { };
		public static Action OnBtnSettings = delegate { };
		public static Action OnBtnHome = delegate { };
		public static Action OnBtnShop = delegate { };
		public static Action OnBtnLeaderBoard = delegate { };
		public static Action OnBtnAchiev = delegate { };

		public void BtnWatchAd()
		{
			OnBtnWatchAd();
		}

		public void BtnReplay()
		{
			OnBtnReplay();
		}

		public void BtnSettings()
		{
			OnBtnSettings();
		}

		public void BtnHome()
		{
			OnBtnHome();
		}

		public void BtnShop()
		{
			OnBtnShop();
		}

		public void BtnLeaderBoard()
		{
			OnBtnLeaderBoard();
		}

		public void BtnAchiev()
		{
			OnBtnAchiev();
		}
	}

	public class MenuTestEventsEnd
	{
		public static Action OnNewRecordAppear = delegate { };
	}
}