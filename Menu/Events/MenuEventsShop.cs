﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Shorka.CellConnect
{
	public class MenuEventsShop : MonoBehaviour
	{
		public static Action OnBtnBack = delegate { };
		public static Action<string> OnBtnFree = delegate (string id){ };
		public static Action<string> OnBtnGetPU = delegate (string id){ };
		public static Action<string> OnBtnBuyMagicStars = delegate (string id){ };
		public static Action OnBtnBuyInMessage = delegate { };
		public static Action OnBtnCloseMessage = delegate { };
		public static Action OnBtnTutorial = delegate { };

		public static Action OnTabSuperPowers = delegate { };
		public static Action OnTabBuyStars = delegate { };

		public void BtnTutorial()
		{
			OnBtnTutorial();
		}


		public void BtnBack()
		{
			OnBtnBack();
		}

		public void BtnBuyInMessage()
		{
			OnBtnBuyInMessage();
		}

		public void BtnCloseMessage()
		{
			OnBtnCloseMessage();
		}

		public void TabSuperPowers()
		{
			OnTabSuperPowers();
		}

		public void TabBuyStars()
		{
			OnTabBuyStars();
		}

		public void BtnFree(string id)
		{
			OnBtnFree(id);
		}

		public void BtnGetPU(string id)
		{
			OnBtnGetPU(id);
		}

		public void BtnBuyMagicStars(string id)
		{
			OnBtnBuyMagicStars(id);
		}

	}
}