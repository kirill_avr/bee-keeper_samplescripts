﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Shorka.CellConnect
{
	public class MenuEventsChests : MonoBehaviour
	{
		public static Action OnBtnBack = delegate { };

		public static Action <KeyType> OnOpenChest = delegate(KeyType keyType) { };
		public static Action OnWrongKey = delegate { };
		public static Action<Transform> OnGiftTap = delegate(Transform trans) { };
		public static Action OnCollectAll = delegate { };

		public void BtnBack()
		{
			OnBtnBack();
		}
		public void GiftTap(Transform transGift)
		{
			OnGiftTap(transGift);
		}

		public void BtnCollectAll()
		{
			OnCollectAll();
		}
	}
}