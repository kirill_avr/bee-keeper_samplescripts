﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Shorka.CellConnect
{
	public class MenuEventsInGame : MonoBehaviour
	{
		public static Action OnBtnSound = delegate { };
		public static Action OnBtnReplay = delegate { };
		public static Action OnBtnHome= delegate { };
		public static Action OnBtnSettings = delegate { };

		public static Action OnBtnUndo = delegate { };
		public static Action OnBtnSmallBomb = delegate { };
		public static Action OnBtnMagic = delegate { };
		public static Action OnBtnBigBomb = delegate { };
		public static Action OnBtnBear = delegate { };

		public static Action OnBtnContinueAfterReachLvl = delegate { };

		public static Action OnBtnClosePUTutorial = delegate { };
		public static Action OnBtnUSEPUTutorial = delegate { };

		public static Action OnBtnPUHelp = delegate { };

		public void BtnPUHelp()
		{
			OnBtnPUHelp();
		}

		public void BtnClosePUTutorial()
		{
			OnBtnClosePUTutorial();
		}

		public void BtnUSEPUTutorial()
		{
			OnBtnUSEPUTutorial();
		}

		public void BtnContinueAfterReachLvl()
		{
			OnBtnContinueAfterReachLvl();
		}
			


		public void BtnSettings()
		{
			OnBtnSettings();
		}

		public void BtnHome()
		{
			OnBtnHome();
		}

		public void BtnSound()
		{
			OnBtnSound();
		}

		public void BtnReplay()
		{
			OnBtnReplay();
		}

		public void BtnUndo()
		{
			OnBtnUndo();
		}
		public void BtnSmallBomb()
		{
			OnBtnSmallBomb();
		}
		public void BtnMagic()
		{
			OnBtnMagic();
		}
		public void BtnBigBomb()
		{
			OnBtnBigBomb();
		}
		public void BtnBear()
		{
			OnBtnBear();
		}
		public void BtnTestGameOver()
		{
			GameTestEvents.OnGameOver();
		}

		public void BtnTestBee()
		{
			GameTestEvents.OnBeeAppear();
		}
	}
}