﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using System;
using DG.Tweening;
using MovementEffects;

namespace Shorka.CellConnect
{
	public class MenuHandler : IInitializable, ITickable, IDisposable
	{
		public bool doAnimCoverBee = false;
		public bool doSetUpBtnRightInGame = false;

		private MenuState state;
		private MenuStates currStateType;

		private readonly MenuStateFactory stateFactory;
		public readonly SceneSettings scSettings;
		public readonly Settings settings;
		public readonly AsyncProcessor asyncProc;
		public readonly GameInfo gameInfo;
		public readonly AudioPlayer auPlayer;
		public readonly CanvasFadedBackCtrl canvasFadedCtrl;
		public readonly CanvasLvlBarCtrl canvasLvlBarCtrl;
		public readonly LeaderBoardSettings.Settings leaderboardSett;
		public readonly PoweUpsCtrl powerUpsCtrl;
		public readonly CameraCtrl camCtrl;
		public readonly ParticleWarehouse particleWareH;
		public readonly SoundsWarehouse soundWareHouse;

		public MenuStates backStateFromShop = MenuStates.InGame;
		public MenuStates backStateFromSettings = MenuStates.Home;

		public MenuStates CurrStateType {  get { return currStateType; }}
		public SceneInGameSetting ScSettingsInGame {  get { return scSettings.ScSettingsInGame; }}


		public MenuHandler(MenuStateFactory _stateFactory, SceneSettings _scSettings, Settings _settings
			,AsyncProcessor _asyncProc, GameInfo _gameInfo, AudioPlayer _auPlayer, CanvasFadedBackCtrl _canvasFadedCtrl,
			CanvasLvlBarCtrl _canvasLvlBarCtrl, LeaderBoardSettings.Settings _leaderboardSett, PoweUpsCtrl _powerUpsCtrl,
			CameraCtrl _camCtrl,  ParticleWarehouse _particleWareH, SoundsWarehouse _soundWareHouse)
		{
			stateFactory = _stateFactory;
			scSettings = _scSettings;
			settings = _settings;
			asyncProc = _asyncProc;
			gameInfo = _gameInfo;
			auPlayer = _auPlayer;
			canvasFadedCtrl = _canvasFadedCtrl;
			canvasLvlBarCtrl = _canvasLvlBarCtrl;
			leaderboardSett = _leaderboardSett;
			powerUpsCtrl = _powerUpsCtrl;
			camCtrl = _camCtrl;
			particleWareH = _particleWareH;
			soundWareHouse = _soundWareHouse;
			doAnimCoverBee = false;
			doSetUpBtnRightInGame = false;
//			Debug.Log("MenuHandler");
		}

		public void Initialize ()
		{
//			if(settings.DoResetCanv)
//			{
//				ResetCanvases();
//				ChangeState (settings.IsHomeFromStart ? MenuStates.Home : settings.stateDefault );
//			}

			#if UNITY_EDITOR
			StateTestEvents.OnGetMenuState += OnGetMenuState;
			#endif
		}

		public void Tick ()
		{
			if (state != null)
				state.Update();
		}

		public void Dispose ()
		{
			doAnimCoverBee = false;
			doSetUpBtnRightInGame = false;

			if (state != null)
				state.Stop();

			#if UNITY_EDITOR
			StateTestEvents.OnGetMenuState -= OnGetMenuState;
			#endif
		}

		public void ResetCanvases()
		{
			scSettings.CanvasHome.SetActive(false);
			scSettings.CanvasShop.SetActive(false);
			scSettings.CanvasEnd.SetActive(false);
			scSettings.CanvasInGame.SetActive(false);
			scSettings.CanvasOutOfPu.SetActive(false);
			scSettings.CanvasRestart.SetActive(false);
			scSettings.CanvasChest.SetActive(false);

			scSettings.CanvasBackLight.SetActive(false);
			scSettings.CanvasBackBright.SetActive(false);

			scSettings.CanvasReachLvl.SetActive(false);
			scSettings.CanvasPUTutorial.SetActive(false);
		}

		public void ChangeState(MenuStates stateType, params object[] constructorArgs)
		{
			if (state != null)
				state.Stop();
			

			state = stateFactory.Create(stateType,constructorArgs);
			state.Start();
			currStateType = stateType;
		}

		public void PlayOneShotAuClip(AudioClipHolder clipHolder)
		{
			auPlayer.PlayOneShot(scSettings.AuSource, clipHolder);
		}

		public void PlayOneShotAuClip(AudioClip clip, float volume)
		{
			if(scSettings.AuSource.isPlaying)
			{
				scSettings.AuSource.volume = volume;
				return;
			}
			auPlayer.PlayOneShot(scSettings.AuSource, clip, volume);
		}

		public void CountCollScore(Text txt, int startNumb, int endNumb, float duration)
		{
			Timing.RunCoroutine(
				Tools.IECountTo(txt, startNumb, endNumb, duration));
		}

		public void ExecuteSizeUpAnim(Transform trans, Vector3 initScale, Vector3 endScale,
			float duration1, float duration2, float waitTime)
		{
			Timing.RunCoroutine(
				IEExecuteSizeUpAnim(trans, initScale, endScale,duration1, duration2, waitTime));
		}

		private IEnumerator<float> IEExecuteSizeUpAnim(Transform trans, Vector3 initScale, Vector3 endScale,
			float duration1, float duration2, float waitTime)
		{
			trans.DOScale(endScale, duration1);
			yield return Timing.WaitForSeconds(waitTime);
			trans.DOScale(initScale,duration2);
		}

		private void OnGetMenuState()
		{
			Debug.Log("Current MENU state: ".StrColored(DebugColors.blue) + currStateType.ToString());
		}

		public void BtnLeaderBoard()
		{
			Debug.Log("BtnLeaderBoard".StrColored(DebugColors.blue));

			if (!LeaderBoardCtrl.IsAuthenticated)
			{
				LeaderBoardCtrl.Authenticate();
			}

			string leaderId = leaderboardSett.LeaderboardID;
			LeaderBoardCtrl.ReportScore(DataPrefsSaver.HighScore, leaderId);
			LeaderBoardCtrl.ShowLeaderboard(leaderId,  leaderboardSett.DoOpenDirectly);

			if(leaderboardSett.UseAchievs)
				LeaderBoardCtrl.ReportAchievements(leaderboardSett.Achievements);
		}

		public void PlayAuBtnDefault()
		{
			auPlayer.PlayOneShot(scSettings.AuSource,settings.AuBtnDefault);
		}

		[Serializable]
		public class Settings
		{
//			[SerializeField] private LevelsSettings lvlSettings;
			[Space(5)]
			[Tooltip("Do make all canvases inactive on start?")]
			[SerializeField] private bool doResetCanv;
			[SerializeField] private bool isHomeFromStart = true;
			[SerializeField] private AudioClipHolder auBtnDefault;
			[SerializeField] private AudioClipHolder auBtnPlay;
			[SerializeField] private PowerUpSettingsInLevels puSettingsInLvls;

			public MenuStates stateDefault = MenuStates.Home;

			/// <summary>
			///Do make all canvases inactive on start?
			/// </summary>
			/// <value><c>true</c> if do reset canv; otherwise, <c>false</c>.</value>
			public bool DoResetCanv {  get { return doResetCanv; }}
			public bool IsHomeFromStart {  get { return isHomeFromStart; }}
			public AudioClipHolder AuBtnDefault {  get { return auBtnDefault; }}
			public AudioClipHolder AuBtnPlay {  get { return auBtnPlay; }}
			public PowerUpSettingsInLevels PuSettingsInLvls {  get { return puSettingsInLvls; }}
//			public LevelsSettings LvlSettings {  get { return lvlSettings; }}
		}

		[Serializable]
		public class SceneSettings
		{
			[SerializeField] private AudioSource auSource;
			[Space(5)]
			[SerializeField] private GameObject canvasHome;
			[SerializeField] private GameObject canvasInGame;
			[SerializeField] private GameObject canvasOutOfPu;
			[SerializeField] private GameObject canvasReachLvl;
			[SerializeField] private GameObject canvasRestart;

			[SerializeField] private GameObject canvasEnd;
			[SerializeField] private GameObject canvasShop;
			[SerializeField] private GameObject canvasChest;
			[SerializeField] private GameObject canvasSettings;
			[SerializeField] private GameObject canvasSettingsLeaves;
			[SerializeField] private GameObject canvasTutorial;
			[SerializeField] private GameObject canvasPUTutorial;
			[SerializeField] private GameObject canvasDevTest;

			[Space(5)]
			[SerializeField] private GameObject canvasBackLight;
			[SerializeField] private GameObject canvasBackBright;

			[Space(5)]
			[SerializeField] private SceneInGameSetting scSettingsInGame;

			public AudioSource AuSource {  get { return auSource; }}

			public GameObject CanvasHome {  get { return canvasHome; }}
			public GameObject CanvasInGame {  get { return canvasInGame; }}
			public GameObject CanvasRestart {  get { return canvasRestart; }}

			public GameObject CanvasOutOfPu {  get { return canvasOutOfPu; }}
			public GameObject CanvasReachLvl {  get { return canvasReachLvl; }}

			public GameObject CanvasEnd {  get { return canvasEnd; }}
			public GameObject CanvasShop {  get { return canvasShop; }}
			public GameObject CanvasChest {  get { return canvasChest; }}
			public GameObject CanvasTutorial {  get { return canvasTutorial; }}
			public GameObject CanvasPUTutorial {  get { return canvasPUTutorial; }}

			public GameObject CanvasSettings {  get { return canvasSettings; }}
			public GameObject CanvasSettingsLeaves {  get { return canvasSettingsLeaves; }}
			public GameObject CanvasBackLight {  get { return canvasBackLight; }}
			public GameObject CanvasBackBright {  get { return canvasBackBright; }}

			public GameObject CanvasDevTest {  get { return canvasDevTest; }}

			public SceneInGameSetting ScSettingsInGame {  get { return scSettingsInGame; }}
		}

		[Serializable]
		public class SceneInGameSetting
		{
			[SerializeField] private PowerUpInGame[] powerUpsInGame;
			public PowerUpInGame[] PowerUpsInGame {  get { return powerUpsInGame; }}

			public PowerUpInGame GetUIPU(PowerUpTypes puType)
			{
				int len = powerUpsInGame.Length;
				for (int i = 0; i < len; i++) 
				{
					PowerUpInGame iPowerUp= powerUpsInGame[i];
					if(puType.ToString() == iPowerUp.Type.ToString())
						return iPowerUp;					
				}
				return null;
			}

			public void PutScaleOnAllPUBtns(Vector3 scale)
			{
				int len = powerUpsInGame.Length;
				for (int i = 0; i < len; i++)
					powerUpsInGame[i].Img.transform.localScale = scale;				
			}

			public void SetParentForBtnsPU(Transform parent)
			{
				int len = powerUpsInGame.Length;
				for (int i = 0; i < len; i++) 
					powerUpsInGame[i].Img.transform.parent = parent;	
			}
		}

	
	}

	[Serializable]
	public class PowerUpUI
	{
		[SerializeField] protected PowerUpTypes type;
		[SerializeField] protected Text txQty;
		[SerializeField] protected Image img;

		public PowerUpTypes Type {  get { return type; }}
		public Text TxtQty {  get { return txQty; }}
		public Image Img {  get { return img; }}

		public Vector3 Pos {  get { return img.transform.position; }}


	}

	[Serializable]
	public class PowerUpInGame : PowerUpUI
	{
		[SerializeField] private GameObject plus;
		[SerializeField] private RectTransform rectLock;
		[SerializeField] private GameObject blackSquare;
//		[SerializeField] private PowerUpInGameSettings puInGameSettings;
		private RectTransform rectTrans;


		public GameObject Plus {  get { return plus; }}
		public RectTransform RectLock {  get { return rectLock; }}
		public GameObject BlackSquare {  get { return blackSquare; }}

		public RectTransform RectTrans 
		{ 
			get
			{
				if(rectTrans == null)
					rectTrans = img.GetComponent<RectTransform>();
				
				return rectTrans;
			}
		}

		public bool isLocked 
		{  
			get 
			{
				if(rectLock == null)
					return false;
				
				return rectLock.gameObject.activeInHierarchy; 
			}
		}

		public void SetPosY (float yPos)
		{
			img.transform.SetPosY(yPos);
		}

		public void SetLock(bool doLock)
		{
			if(rectLock != null)
				rectLock.gameObject.SetActive(doLock);
		}

		public void AnimateLock(float duration, ShakeSettings shakeSett)
		{
			if(rectLock == null)
				return;
			
			rectLock.DOShakeRotation(
				duration,
				shakeSett.Strength,
				shakeSett.Vibrato,
				shakeSett.Randomness,
				shakeSett.FadeOut
			);
//			Sequence seqLock = DOTween.Sequence();
//			seqLock.SetLoops(1);
//			seqLock.Append(rectLock.DOLocalRotate(vecAngle1, settings.BtnWatch.DurAngles));
//			seqLock.Append(rectLock.DOLocalRotate(vecAngle2, settings.BtnWatch.DurAngles));	
		}
	}

	[Serializable]
	public class PowerUpInShop : PowerUpUI
	{
		[SerializeField] private Text txtPrice;
		[SerializeField] private Text txtNameOfPU;

		public Vector3 InitScale { get; set;}
		public Color InitColor { get; set;}

		public Text TxtPrice {  get { return txtPrice; }}
		public Text TxtNameOfPU {  get { return txtNameOfPU; }}
	}

	[Serializable]
	public class ParticlePosAndID
	{
		[SerializeField] private Transform posPointer;
		[SerializeField] private string idParticle; 

		public Transform PosPointer {  get { return posPointer; }}
		public string IdParticle {  get { return idParticle; }}
	}
}