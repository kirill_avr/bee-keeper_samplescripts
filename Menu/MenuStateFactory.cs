﻿using UnityEngine;
using System.Collections;
using Zenject;
using ModestTree;

namespace Shorka.CellConnect
{
	/// <summary>
	/// Class is responsible for menu states managing
	/// </summary>
	public class MenuStateFactory 
	{
		private readonly DiContainer container;
		public MenuStateFactory(DiContainer container)
		{
			this.container = container;
		}


		public MenuState Create(MenuStates state, params object[] constructorArgs)
		{
			switch (state)
			{
			case MenuStates.Home:
				return container.Instantiate<MenuStateHome>(constructorArgs);

			case MenuStates.InGame:
				return container.Instantiate<MenuStateInGame>(constructorArgs);

			case MenuStates.Restart:
				return container.Instantiate<MenuStateRestart>(constructorArgs);

			case MenuStates.GameOver:
				return container.Instantiate<MenuStateGameOver>(constructorArgs);

			case MenuStates.Undo:
				return container.Instantiate<MenuStateUndo>(constructorArgs);

			case MenuStates.Shop:
				return container.Instantiate<MenuStateShop>(constructorArgs);

			case MenuStates.OutOfPU:
				return container.Instantiate<MenuStateOutOf>(constructorArgs);

			case MenuStates.Settings:
				return container.Instantiate<MenuStateSettings>(constructorArgs);

			case MenuStates.Chests:
				return container.Instantiate<MenuStateChests>(constructorArgs);

			case MenuStates.Tutorial:
				return container.Instantiate<MenuStateTutorial>(constructorArgs);

			case MenuStates.DevTest:
				return container.Instantiate<MenuStateDevTest>(constructorArgs);

			case MenuStates.ReachLevel:
				return container.Instantiate<MenuStateReachLevel>(constructorArgs);

			case MenuStates.PUTutorial:
				return container.Instantiate<MenuStatePUTutorial>(constructorArgs);

			case MenuStates.None:
				return container.Instantiate<MenuStateNone>(constructorArgs);

			}
			return null;
//			throw Assert.CreateException();
		}
	}
}

