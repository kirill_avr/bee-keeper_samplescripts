﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using DG.Tweening;
using MovementEffects;

namespace Shorka.CellConnect
{
	public class BtnSettingsRollOut 
	{
		/// <summary>
		/// Behavior of BtnSettings, that describes all possible actions
		/// </summary>
		public enum States  {	

			///<summary>From single btnSettings to appearance of all buttons with movement animation</summary>
			RollOut, 
			///<summary>From all buttons to single btnSettings with movement animation</summary>
			RollIn, 
			///<summary>Show single BtnSettings with hided all buttons. No Animation</summary>
			SingleBtnSettings, 
			///<summary>Show AllButtons. No Animation</summary>
			AllButtons  
		};
		private States currState;
		private readonly Vector2[] posInit;
		private readonly int qtyRollOutBtns;

		/// <summary>
		/// Current state
		/// </summary>
		public States CurrState {  get { return currState; }}

		private RectTransform[] RollOutBtns {  get { return scSettings.RollOutBtns; }}

		private readonly SceneSettings scSettings;
		private readonly Settings settings;

		public BtnSettingsRollOut(SceneSettings _scSettings, Settings _settings)
		{
			scSettings = _scSettings;
			settings = _settings;

			qtyRollOutBtns = RollOutBtns.Length;
			posInit = new Vector2[qtyRollOutBtns];

			for (int i = 0; i < qtyRollOutBtns; i++) 
			{
				posInit[i] = RollOutBtns[i].anchoredPosition;
			}
		}
			
			
		public void ChangeState(States state)
		{
			currState = state;

			switch(state)
			{

			case States.RollOut:

				if(settings.DoRotate)
				{
//					Debug.Log("Rotate OUT to : " +settings.AngleRollOut );
					scSettings.BtnSettings.DOLocalRotate(settings.AngleRollOut, settings.DurRollUp, settings.RotateMode);
				}					
				
				Vector2 posBtnSettings =  scSettings.BtnSettings.anchoredPosition;
				for (int i = 0; i < qtyRollOutBtns; i++) 
				{
					RectTransform iRollOut = RollOutBtns[i];
					iRollOut.gameObject.SetActive(true);
					iRollOut.anchoredPosition = posBtnSettings;
					iRollOut.DOAnchorPos(posInit[i], settings.DurRollUp);
				}
				
				break;

			case States.RollIn:

				if(settings.DoRotate)
				{
//					Debug.Log("Rotate IN to : " +settings.AngleRollIn );
					scSettings.BtnSettings.DOLocalRotate(settings.AngleRollIn, settings.DurRollUp, settings.RotateMode);
				}					
				
				posBtnSettings =  scSettings.BtnSettings.anchoredPosition;
				for (int i = 0; i < qtyRollOutBtns; i++) 
				{
					RollOutBtns[i].DOAnchorPos(posBtnSettings, settings.DurRollUp);
				}



				Timing.RunCoroutine(IEWaitAndDisable(settings.DurRollUp));

				break;

			case States.SingleBtnSettings:
				
				for (int i = 0; i < qtyRollOutBtns; i++) 
				{
					RollOutBtns[i].gameObject.SetActive(false);
				}

				if(settings.DoRotate)
					SetBtnAngle(settings.AngleRollIn.z);
				
				break;

			case States.AllButtons:
				for (int i = 0; i < qtyRollOutBtns; i++) 
				{
					RectTransform iRollOut = RollOutBtns[i];
					iRollOut.anchoredPosition = posInit[i];
					iRollOut.gameObject.SetActive(true);
				}

				if(settings.DoRotate)
					SetBtnAngle(settings.AngleRollOut.z);

				break;
			}

			 
		}
			
		private IEnumerator<float> IEWaitAndDisable(float wTime)
		{
			yield return Timing.WaitForSeconds(wTime);
			for (int i = 0; i < qtyRollOutBtns; i++) 
			{
				RollOutBtns[i].gameObject.SetActive(false);		
			}
					
		}

		private void SetBtnAngle(float zAngle)
		{
			if(scSettings.BtnSettings.localEulerAngles.z != zAngle)
				scSettings.BtnSettings.SetLocalAngleZ(zAngle);
		}

		[System.Serializable]
		public class SceneSettings
		{
			[SerializeField] private RectTransform btnSettings;
			[SerializeField] private RectTransform[] rollOutBtns;

			public RectTransform BtnSettings {  get { return btnSettings; }}
			public RectTransform[] RollOutBtns {  get { return rollOutBtns; }}
		}

		[System.Serializable]
		public class Settings
		{
			[SerializeField] private float durRollUp = 0.8F;
			[Space(5)]

			[SerializeField] private bool doRotate;
			[SerializeField] private RotateMode rotateMode;
			[SerializeField] private Vector3 angleRollIn;
			[SerializeField] private Vector3 angleRollOut;

			public float DurRollUp {  get { return durRollUp; }}

			public bool DoRotate {  get { return doRotate; }}
			public RotateMode RotateMode {  get { return rotateMode; }}
			public Vector3 AngleRollIn {  get { return angleRollIn; }}
			public Vector3 AngleRollOut {  get { return angleRollOut; }}

		}
	}
}