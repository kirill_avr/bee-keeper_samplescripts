﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shorka.CellConnect
{
	public class MenuStateNone : MenuState 
	{

		public MenuStateNone(MenuHandler menuHandler) : base(menuHandler)
		{

		}

		public override void Start()
		{
		}


		public override void Stop()
		{
		}
	}
}