﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using MovementEffects;

namespace Shorka.CellConnect
{
	public class MenuStateReachLevel : MenuState 
	{
		private const string STR_NUMB_STARS = "NUMB_STARS";
		private const string STR_NEXT_REACH_LVL = "NEXT_REACH_LVL";

		private readonly SceneSettings scSettings;
		private readonly Settings settings;
		private readonly int lvlReached;
		private readonly int nextLvlScore;
		private readonly BeeLevel currLvl;
		private readonly BeeLevel nextLvl;
//		private readonly bool nextLvlScore;
//		private readonly int nextLvlScore;

		private readonly Vector3 initScaleWindowReachLvl;
		private readonly Vector3 initScaleBee;

		public MenuStateReachLevel(SceneSettings _scSettings, Settings _settings, MenuHandler menuHandler, BeeLevel currLvl = null,
			int _nextLvlScore = 0) : base(menuHandler)
		{
			scSettings = _scSettings;
			settings = _settings;

			lvlReached = currLvl.lvlNumb;
			nextLvlScore = _nextLvlScore;

			initScaleWindowReachLvl = scSettings.Window.localScale;
			initScaleBee = scSettings.ImgBee.localScale;
		}

		public override void Start()
		{
			menuHandler.canvasFadedCtrl.SetFadeOut();
			menuHandler.canvasFadedCtrl.SetActive(true);
			menuHandler.canvasFadedCtrl.AnimFadeIn(settings.AlphaChBack);
			menuHandler.scSettings.CanvasInGame.SetActive(true);

			menuHandler.scSettings.CanvasReachLvl.SetActive(true);
			scSettings.TxtLvlNumbReach.text = lvlReached.ToString();

			scSettings.Window.localScale = Constants.v3Zero;
			scSettings.ImgBee.localScale = Constants.v3Zero;

			scSettings.ImgBee.gameObject.SetActive(true);

			scSettings.TxtCollectStarsToReach.text = settings.StrCollectStars
				.Replace(STR_NUMB_STARS, nextLvlScore.ToString())
				.Replace(STR_NEXT_REACH_LVL, (lvlReached+1).ToString())
				.Replace(Constants.STR_NEW_LINE_EDITOR, Constants.STR_NEW_LINE);

			Sequence mySequence = DOTween.Sequence();
			mySequence.Append(scSettings.Window.DOScale(
				initScaleWindowReachLvl,settings.DurBadgeAppear).SetEase(settings.EaseBadgeAppear));
			mySequence.Append(scSettings.ImgBee.DOScale(
				initScaleBee,settings.DurBadgeAppear).SetEase(settings.EaseBadgeAppear));

			menuHandler.soundWareHouse.PlayOneShotById(settings.IdAuClipRechLvl, menuHandler.scSettings.AuSource);

			Timing.RunCoroutine(scSettings.IEPlayFireWorks(
				settings.DelayBeforeFireWorks, settings.MinMaxDelayFireWorks,menuHandler.particleWareH));


			MenuEventsInGame.OnBtnContinueAfterReachLvl += OnBtnContinueAfterReachLvl;

			MenuTestEvents.OnUnlockAnim += OnTestPlayUnlock;
		}

		public override void Stop()
		{
			menuHandler.canvasFadedCtrl.SetActive(false);
			menuHandler.scSettings.CanvasOutOfPu.SetActive(false);
			menuHandler.scSettings.CanvasReachLvl.SetActive(false);

			scSettings.Window.localScale = initScaleWindowReachLvl;
			scSettings.ImgBee.localScale = initScaleBee;

			MenuEventsInGame.OnBtnContinueAfterReachLvl -= OnBtnContinueAfterReachLvl;
			MenuTestEvents.OnUnlockAnim -= OnTestPlayUnlock;
		}


		private void OnBtnContinueAfterReachLvl()
		{
			PowerUpInGame firstLockPu = FirstLockedPu();
			if(firstLockPu != null)
			{
				if(menuHandler.settings.PuSettingsInLvls.IsAllowedLvlToUsePU(firstLockPu.Type , lvlReached))
					Timing.RunCoroutine(IEPlayUnlockAnim(firstLockPu));				
			}

			else
				menuHandler.ChangeState(MenuStates.InGame);
		}

		private void OnTestPlayUnlock(PowerUpTypes puTypes)
		{
			PowerUpInGame firstLockPu = menuHandler.ScSettingsInGame.GetUIPU(puTypes);
			Timing.RunCoroutine(IEPlayUnlockAnim(firstLockPu));
		}

		private IEnumerator<float> IEPlayUnlockAnim(PowerUpInGame firstLockPu)
		{
			menuHandler.canvasFadedCtrl.SetActive(false);
			menuHandler.scSettings.CanvasOutOfPu.SetActive(false);
			menuHandler.scSettings.CanvasReachLvl.SetActive(false);
			yield return Timing.WaitForSeconds(settings.DelayBeforeUnlockAnim);

		}


		private PowerUpInGame FirstLockedPu()
		{
			int len = menuHandler.ScSettingsInGame.PowerUpsInGame.Length;
			for (int i = 0; i < len; i++) 
			{
				PowerUpInGame item =  menuHandler.ScSettingsInGame.PowerUpsInGame[i];
				if(item.isLocked)
					return item;				
			}

			return null;
		}

		[System.Serializable]
		public class Settings
		{
			[SerializeField] private string strCollectStars;
			[SerializeField] private float durBadgeAppear = 1;
			[SerializeField] private Ease easeBadgeAppear;
			[SerializeField] private string idAuClipRechLvl = "HarpMagic";
			[SerializeField] private float delayBeforeFireWorks = 0.2F;
			[SerializeField] private float delayBeforeUnlockAnim = 0.4F;

			[SerializeField] private MinMax minMaxDelayFireWorks;
			[Range(0,1)][SerializeField] private float alphaChBack = 0.8F;

			public string StrCollectStars {  get { return strCollectStars; }}
			public float DurBadgeAppear {  get { return durBadgeAppear; }}
			public Ease EaseBadgeAppear {  get { return easeBadgeAppear; }}

			public string IdAuClipRechLvl {  get { return idAuClipRechLvl; }}
			public float DelayBeforeFireWorks {  get { return delayBeforeFireWorks; }}
			public MinMax MinMaxDelayFireWorks {  get { return minMaxDelayFireWorks; }}

			public float AlphaChBack {  get { return alphaChBack; }}

			public float DelayBeforeUnlockAnim {  get { return delayBeforeUnlockAnim; }}
		}

		[System.Serializable]
		public class SceneSettings
		{
			[SerializeField] private Text txtLvlNumbReach;
			[SerializeField] private Text txtCollectStarsToReach;
			[SerializeField] private RectTransform window;
			[SerializeField] private RectTransform imgBee;
			[Space(5)]
			[SerializeField] private ParticlePosAndID[] fireWorks;

			public Text TxtLvlNumbReach {  get { return txtLvlNumbReach; }}
			public Text TxtCollectStarsToReach {  get { return txtCollectStarsToReach; }}

			public RectTransform Window {  get { return window; }}
			public RectTransform ImgBee {  get { return imgBee; }}

			public ParticlePosAndID[] FireWorks {  get { return fireWorks; }}

			public IEnumerator<float> IEPlayFireWorks(float delayBeforePlay, MinMax minMaxDelayTime, ParticleWarehouse partWarehouse)
			{
				int len = fireWorks.Length;
				yield return Timing.WaitForSeconds(delayBeforePlay);

				for (int i = 0; i < len; i++) 
				{
					ParticlePosAndID item = fireWorks[i];
					ParticleSystemOptimized partOpt = partWarehouse.GetFreeParticleSystem(item.IdParticle);
					partOpt.SetActive(true);
					partOpt.parentTrans.SetPos2D(item.PosPointer.position);
					partOpt.Play();

					yield return Timing.WaitForSeconds(minMaxDelayTime.RandomValue());
				}
			}
		}
	}
}