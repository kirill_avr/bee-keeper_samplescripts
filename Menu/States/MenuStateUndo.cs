﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

namespace Shorka.CellConnect
{
	public class MenuStateUndo : MenuState 
	{
//		private readonly SceneSettings scSettings;
		public MenuStateUndo(MenuHandler menuHandler) : base(menuHandler)
		{
//			scSettings = _scSettings;
		}

		public override void Start()
		{
			menuHandler.scSettings.CanvasInGame.SetActive(true);
//			menuHandler.scSettings.CanvasUndo.SetActive(true);

			MenuEventsUndo.OnBtnCoinsUse += OnBtnCoinsUse;
			MenuEventsUndo.OnBtnRewardedAd += OnBtnRewardedAd;
			MenuEventsUndo.OnBtnClose += OnBtnClose;
		}

		public override void Stop()
		{
			menuHandler.scSettings.CanvasInGame.SetActive(false);
//			menuHandler.scSettings.CanvasUndo.SetActive(false);

			MenuEventsUndo.OnBtnCoinsUse -= OnBtnCoinsUse;
			MenuEventsUndo.OnBtnRewardedAd -= OnBtnRewardedAd;
			MenuEventsUndo.OnBtnClose -= OnBtnClose;
		}

		private void OnBtnRewardedAd()
		{
		}

		private void OnBtnCoinsUse()
		{
		}

		private void OnBtnClose()
		{
		}

//		[Serializable]
//		public class SceneSettings
//		{
//			[SerializeField] private Text txtCoins;
//			public Text TxtCoins {  get { return txtCoins; }}
//		}
	}
}