﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using Shorka.Ads;
using MovementEffects;
using DG.Tweening;

namespace Shorka.CellConnect
{
	public class MenuStateShop : MenuState 
	{
//		public enum StarsPurchase { 100k, 200k, 500k, 1mill };
		public enum StarsPurchase { Hundred_k, TwoHundred_k, FiveHundred_k, OneMill};

		public enum Tab { SuperPower, BuyMagicStars };

		private Tab selTab = Tab.SuperPower;
		private readonly MenuStates stateToBack;
		private PowerUp rewardedPU; 
		private readonly int lenPowerUpsUI;

		private readonly SceneSettings scSettings;
		private readonly Settings settings;
//		private readonly PoweUpsCtrl poweUpsCtrl;

		public MenuStateShop(SceneSettings _scSettings, Settings _settings,
			MenuHandler menuHandler, MenuStates _stateToBack = MenuStates.Home, Tab _selTab = Tab.SuperPower) : base(menuHandler)
		{
			scSettings = _scSettings;
			settings = _settings;
			stateToBack = _stateToBack;
			selTab = _selTab;

			lenPowerUpsUI = scSettings.PowerUpsUI.Length;
				
			if(stateToBack != MenuStates.OutOfPU)
				menuHandler.backStateFromShop = stateToBack;
		}

		public override void Start()
		{
			menuHandler.scSettings.CanvasShop.SetActive(true);
			scSettings.EnableGraphicRayCast(true);
//			scSettings.PanelNoStars.SetActive(false);
			menuHandler.scSettings.CanvasBackBright.SetActive(true);

			CtrlTab(selTab, false);
			RefreshSuperPowerTab();
			scSettings.TxtStarScore.text = DataPrefsSaver.MagicStarsQTY.ToString();

			for (int i = 0; i < lenPowerUpsUI; i++) 
			{
				PowerUpInShop iUiPU = scSettings.PowerUpsUI[i];
				iUiPU.TxtQty.gameObject.SetActive(true);
				iUiPU.TxtQty.text = DataPrefsSaver.GetPowerUpQTY(iUiPU.Type).ToString();
				iUiPU.TxtNameOfPU.text = menuHandler.powerUpsCtrl.GetPowerUpByType(iUiPU.Type).Name.ToUpper();


				iUiPU.InitScale = iUiPU.Img.transform.localScale;
				iUiPU.InitColor = iUiPU.Img.color;
			}

			#region events
			MenuEventsShop.OnTabBuyStars += OnTabBuyStars;
			MenuEventsShop.OnTabSuperPowers += OnTabSuperPowers;
			MenuEventsShop.OnBtnBack += OnBtnBack;
			MenuEventsShop.OnBtnGetPU += OnBtnGetPU;		
			MenuEventsShop.OnBtnFree += OnBtnFree;
//			MenuEventsShop.OnBtnBuyInMessage += OnBtnBuyInMessage;
//			MenuEventsShop.OnBtnCloseMessage += OnBtnCloseMessage;
			MenuEventsShop.OnBtnBuyMagicStars += OnBtnBuyMagicStars;
			AdsBehaviour.OnRewVideoFinished += OnRewVideoFinished;
			Purchaser.OnPurchaseSuccess += OnPurchaseSuccess;
			#endregion
		}

		public override void Stop()
		{
			menuHandler.scSettings.CanvasShop.SetActive(false);
			menuHandler.scSettings.CanvasBackBright.SetActive(false);
			scSettings.EnableGraphicRayCast(false);

			for (int i = 0; i < lenPowerUpsUI; i++) 
			{
				PowerUpInShop iUiPU = scSettings.PowerUpsUI[i];
				iUiPU.Img.transform.localScale = iUiPU.InitScale;
				iUiPU.Img.color = iUiPU.InitColor;
			}

			#region events
			MenuEventsShop.OnTabBuyStars -= OnTabBuyStars;
			MenuEventsShop.OnTabSuperPowers -= OnTabSuperPowers;
			MenuEventsShop.OnBtnBack -= OnBtnBack;
			MenuEventsShop.OnBtnGetPU -= OnBtnGetPU;
			MenuEventsShop.OnBtnFree -= OnBtnFree;
			MenuEventsShop.OnBtnBuyMagicStars -= OnBtnBuyMagicStars;
			AdsBehaviour.OnRewVideoFinished -= OnRewVideoFinished;
			Purchaser.OnPurchaseSuccess -= OnPurchaseSuccess;
			#endregion
		}

		#region on Methods
		private void OnTabBuyStars()
		{
			if(selTab == Tab.BuyMagicStars)
				return;
			
			selTab = Tab.BuyMagicStars;
			CtrlTab(selTab);
		}

		private void OnTabSuperPowers()
		{
			if(selTab == Tab.SuperPower)
				return;
			
			selTab = Tab.SuperPower;
			CtrlTab(selTab);
		}

		private void OnBtnBuyMagicStars(string nameOfPurch)
		{
			int len = settings.BuyStarUnits.Length;

			for (int i = 0; i < len; i++) 
			{
				Settings.BuyStarUnit iBuyUnit = settings.BuyStarUnits[i];
				if(nameOfPurch == iBuyUnit.ID)
				{
					menuHandler.CountCollScore(
						scSettings.TxtStarScore, 
						DataPrefsSaver.MagicStarsQTY, 
						DataPrefsSaver.MagicStarsQTY + iBuyUnit.StarsQTY,
						settings.DurCountScore);

					DataPrefsSaver.MagicStarsQTY += iBuyUnit.StarsQTY;
					break;
				}
			}

//			string nameOfPurch = settings.GetNameOfPurchase(id);
//			if(string.IsNullOrEmpty(nameOfPurch))
//			{
//				Debug.LogError("ID: " + id + " is null or empty");
//				return;
//			}

//			scSettings.PurchaserScr.BuyProductID(nameOfPurch);
		}

		private void OnPurchaseSuccess(Purchaser.Purchase purchase)
		{
			int magicStarsQTY = DataPrefsSaver.MagicStarsQTY;
			int newMagicStarsQTY = magicStarsQTY + purchase.Value;

			menuHandler.CountCollScore(
				scSettings.TxtStarScore, 
				magicStarsQTY, 
				newMagicStarsQTY,
				settings.DurCountScore
			);

			DataPrefsSaver.MagicStarsQTY = newMagicStarsQTY;
		}

		private void OnBtnBack()
		{
			Debug.Log("BtnBack");
			menuHandler.PlayAuBtnDefault();

			if(stateToBack == MenuStates.GameOver)
				menuHandler.ChangeState(menuHandler.backStateFromShop, false);	

			else			
				menuHandler.ChangeState(menuHandler.backStateFromShop);
		}

		private void OnBtnGetPU(string id)
		{
			rewardedPU = menuHandler.powerUpsCtrl.GetPowerUpByType(id);

			#if UNITY_EDITOR
			Debug.Log("GetPU_ ".StrColored(DebugColors.blue) + rewardedPU.Type.ToString());
			#endif

			if(DataPrefsSaver.MagicStarsQTY >= rewardedPU.Price)
			{
				menuHandler.CountCollScore(
					scSettings.TxtStarScore, 
					DataPrefsSaver.MagicStarsQTY, 
					DataPrefsSaver.MagicStarsQTY - rewardedPU.Price,
					settings.DurCountScore);

				DataPrefsSaver.MagicStarsQTY -= rewardedPU.Price;
				PowerUpInShop puInShop = GetUIPU(rewardedPU.Type);
				AddPU(rewardedPU.Type,puInShop, 1);

				CheckAllPuTxtColors();
				menuHandler.soundWareHouse.PlayOneShotById(settings.IdAuClipBuy);
//				menuHandler.PlayOneShotAuClip(settings.AuBuyPu);
			}
			else
			{
				#if UNITY_EDITOR
				Debug.Log("Can not afford".StrColored(DebugColors.red));
				#endif

				menuHandler.PlayOneShotAuClip(settings.AuNoStars);
				menuHandler.ChangeState(MenuStates.OutOfPU, MenuStateOutOf.Case.OutOfMagicStars);
//				scSettings.PanelNoStars.SetActive(true);
			}
		}

		private void OnBtnFree(string id)
		{
			rewardedPU = menuHandler.powerUpsCtrl.GetPowerUpByType(id);

			#if UNITY_EDITOR
//			Debug.Log("GetPU_ ".StrColored(DebugColors.blue) + rewardedPU.Type.ToString());
			#endif

			AdsBehaviour.Instance.RewardedVideo();
		}

		private void OnRewVideoFinished()
		{
			if(rewardedPU == null)
			{
				Debug.LogError("rewardedPU == null");
				return;
			}

			AddPU(rewardedPU.Type, 1);
		}

//		private void OnBtnBuyInMessage()
//		{
//			scSettings.PanelNoStars.SetActive(false);
//			selTab = Tab.BuyMagicStars; 
//			CtrlTab(Tab.BuyMagicStars);
//		}
//
//		private void OnBtnCloseMessage()
//		{
//			scSettings.PanelNoStars.SetActive(false);
//		}

		#endregion

		private void CtrlTab(Tab desireSelTab, bool isPlaySound = true)
		{
			if(isPlaySound)
				menuHandler.PlayOneShotAuClip(settings.AuTabsSwitch);

			switch(desireSelTab)
			{
			case Tab.BuyMagicStars:

				scSettings.ImgTab.sprite = settings.PicTab2;
				scSettings.TxtInTabBuyMagic.color = settings.ColTxtInTabSelect;
				scSettings.TxtInTabSuperPower.color = settings.ColTxtInTabDark;

				scSettings.PanelBuyStars.SetActive(true);
				scSettings.PanelSuperPower.SetActive(false);
				break;

			case Tab.SuperPower:

				scSettings.ImgTab.sprite = settings.PicTab1;
				scSettings.TxtInTabSuperPower.color = settings.ColTxtInTabSelect;
				scSettings.TxtInTabBuyMagic.color = settings.ColTxtInTabDark;

				scSettings.PanelBuyStars.SetActive(false);
				scSettings.PanelSuperPower.SetActive(true);

				CheckAllPuTxtColors();
				break;
			}
		}


		private void RefreshSuperPowerTab()
		{
			for (int i = 0; i < lenPowerUpsUI; i++) 
			{
				PowerUpInShop iPowerUpUI = 	scSettings.PowerUpsUI[i];
				PowerUp powerUp = menuHandler.powerUpsCtrl.GetPowerUpByType(iPowerUpUI.Type);
				if(powerUp == null)
				{
					Debug.LogError("powerUp is NULL");
					break;
				}
				iPowerUpUI.TxtQty.text = powerUp.QTY.ToString();
				iPowerUpUI.TxtPrice.text = powerUp.Price.ToString();

				CheckPuTxtColor(iPowerUpUI, powerUp.Price);
			}
		}

		#region add power-ups methods
		private void AddPU(PowerUpTypes puType, int addCount)
		{
			AddPU(puType, GetUIPU(puType), addCount);						
		}

		private void AddPU(PowerUpTypes puType, PowerUpInShop puInShop, int addCount)
		{
			if(puInShop == null)
			{
				Debug.LogError("PowerUpType in Shop is NULL");
				return;
			}

			int newQTYPU = DataPrefsSaver.GetPowerUpQTY(puType) + addCount;
			DataPrefsSaver.SetPowerUpQTY(puType, newQTYPU);

			puInShop.TxtQty.text = newQTYPU.ToString();		

			menuHandler.ExecuteSizeUpAnim(
				puInShop.Img.transform, 
				puInShop.InitScale,
				puInShop.InitScale* settings.CoeffSizeUp,
				settings.DurSizeUp,	
				settings.DurSizeUp,
				settings.DurSizeUp + settings.WTimeSizeUp
			);

			Timing.RunCoroutine(IEChangeColor(puInShop));
		}
		#endregion

		private IEnumerator<float> IEChangeColor(PowerUpInShop puInShop)
		{
			puInShop.Img.DOColor(settings.ColorSelSizeUp, settings.DurSizeUp);
			yield return Timing.WaitForSeconds(settings.DurSizeUp + settings.WTimeSizeUp);
			puInShop.Img.DOColor(puInShop.InitColor, settings.DurSizeUp);
		}

		private void CheckAllPuTxtColors()
		{
			for (int i = 0; i < lenPowerUpsUI; i++) 
			{
				PowerUpInShop iPowerUpUI = 	scSettings.PowerUpsUI[i];
				CheckPuTxtColor(iPowerUpUI, menuHandler.powerUpsCtrl.GetPowerUpByType(iPowerUpUI.Type).Price);
			}
		}

		/// <summary>
		/// Check power-up's text color. And change color whether player can afford desired power-ups or not
		/// </summary>
		private void CheckPuTxtColor(PowerUpInShop puInShop, int pricePU)
		{
			puInShop.TxtPrice.color = DataPrefsSaver.MagicStarsQTY >= pricePU ?  
				settings.ColTxtPriceNormal : settings.ColTxtPriceCantAfford;
		}

		private PowerUpInShop GetUIPU(PowerUpTypes puType)
		{
			for (int i = 0; i < lenPowerUpsUI; i++) 
			{
				PowerUpInShop iPowerUpUI = 	scSettings.PowerUpsUI[i];
				if(puType.ToString() == iPowerUpUI.Type.ToString())
					return iPowerUpUI;				
			}

			return null;
		}

		[Serializable]
		public class SceneSettings
		{
			[SerializeField] private Purchaser purchaserScr;
			[SerializeField] private GraphicRaycaster graphRayCast;

			[SerializeField] private GameObject panelSuperPower;
			[SerializeField] private GameObject panelBuyStars;
//			[SerializeField] private GameObject panelNoStars;

			[SerializeField] private Text txtStarScore;
			[SerializeField] private Text txtInTabSuperPower;
			[SerializeField] private Text txtInTabBuyMagic;
			[SerializeField] private Image imgTab;
			[Space(5)]
			[SerializeField] private PowerUpInShop[] powerUpsUI;

			public Purchaser PurchaserScr {  get { return purchaserScr; }}

			public GameObject PanelSuperPower {  get { return panelSuperPower; }}
			public GameObject PanelBuyStars {  get { return panelBuyStars; }}
//			public GameObject PanelNoStars {  get { return panelNoStars; }}

			public Text TxtStarScore {  get { return txtStarScore; }}
			public Text TxtInTabSuperPower {  get { return txtInTabSuperPower; }}
			public Text TxtInTabBuyMagic {  get { return txtInTabBuyMagic; }}
			public Image ImgTab {  get { return imgTab; }}

			public PowerUpInShop[] PowerUpsUI {  get { return powerUpsUI; }}

			public void EnableGraphicRayCast(bool doEnable)
			{
				graphRayCast.enabled = doEnable;
			}
		}

		[Serializable]
		public class Settings
		{
			[SerializeField] private Sprite picTab1;
			[SerializeField] private Sprite picTab2;
			[Space(5)]
			[SerializeField] private Color colTxtInTabSelect;
			[SerializeField] private Color colTxtInTabDark;
			[Space(5)]
			[SerializeField] private Color colTxtPriceNormal;
			[SerializeField] private Color colTxtPriceCantAfford;
			[Space(10)]
			[SerializeField] private float durCountScore = 0.8F;
		
			[Header("Anim PU icon sizeUp settings")]
			[SerializeField] private float durSizeUp = 0.8F;
			[SerializeField] private float coeffSizeUp = 1.2F;
			[SerializeField] private float wTimeSizeUp = 0.1F;
			[SerializeField] private Color colorSelSizeUp;
			[Space(5)]

			[SerializeField] private string idAuClipBuy;
//			[SerializeField] private AudioClipHolder auBuyPu;
			[SerializeField] private AudioClipHolder auNoStars;
			[SerializeField] private AudioClipHolder auTabsSwitch;
			[Space(5)]
			[SerializeField] private string[] namesOfPurchases;
			[SerializeField] private BuyStarUnit[] buyStarUnits;

			public Sprite PicTab2 {  get { return picTab2; }}
			public Sprite PicTab1 {  get { return picTab1; }}
			public float DurCountScore {  get { return durCountScore; }}

			public Color ColTxtInTabSelect {  get { return colTxtInTabSelect; }}
			public Color ColTxtInTabDark {  get { return colTxtInTabDark; }}

			public Color ColTxtPriceCantAfford {  get { return colTxtPriceCantAfford; }}
			public Color ColTxtPriceNormal {  get { return colTxtPriceNormal; }}

			public float DurSizeUp {  get { return durSizeUp; }}
			public float CoeffSizeUp {  get { return coeffSizeUp; }}
			public float WTimeSizeUp {  get { return wTimeSizeUp; }}
			public Color ColorSelSizeUp {  get { return colorSelSizeUp; }}

			public string IdAuClipBuy {  get { return idAuClipBuy; }}
//			public AudioClipHolder AuBuyPu {  get { return auBuyPu; }}
			public AudioClipHolder AuNoStars {  get { return auNoStars; }}
			public AudioClipHolder AuTabsSwitch {  get { return auTabsSwitch; }}

			public string[] NamesOfPurchases {  get { return namesOfPurchases; }}

			public BuyStarUnit[] BuyStarUnits {  get { return buyStarUnits; }}

			[Serializable]
			public class BuyStarUnit
			{
				[SerializeField] private string id;
				[SerializeField] private int starsQTY;

				public string ID {  get { return id; }}
				public int StarsQTY {  get { return starsQTY; }}
			}

			public string GetNameOfPurchase(string name)
			{
				int len = namesOfPurchases.Length;
				for (int i = 0; i < len; i++) 
				{
					string iStr = namesOfPurchases[i];
					if(iStr == name)
						return iStr;
				}

				return string.Empty;
			}
		}		

	}
}