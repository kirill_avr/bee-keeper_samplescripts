﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Shorka.CellConnect
{
	public class MenuStateSettings : MenuState 
	{
		private readonly SceneSettings scSettings;
		private readonly Settings settings;
		private readonly MenuStates stateToBack;
		private readonly StoresLinksOpener storeOpener;
		private readonly RateRequester rateReq;

		public MenuStateSettings(SceneSettings _scSettings, StoresLinksOpener _storeOpener, RateRequester _rateReq, Settings _settings, 
			MenuHandler menuHandler, MenuStates _stateToBack = MenuStates.Home) : base(menuHandler)
		{
			scSettings = _scSettings;
			settings = _settings;
			stateToBack = _stateToBack;
			storeOpener = _storeOpener;
			rateReq = _rateReq;

			if(stateToBack != MenuStates.DevTest)
				menuHandler.backStateFromSettings = stateToBack;
		}

		public override void Start()
		{
			menuHandler.scSettings.CanvasSettings.SetActive(true);
			menuHandler.scSettings.CanvasSettingsLeaves.SetActive(true);
			menuHandler.scSettings.CanvasBackBright.SetActive(true);

			scSettings.ActiveLight.anchoredPosition = scSettings.Flags[0].AnchorPos;
			scSettings.ActiveLight.gameObject.SetActive(true);

			if(DataPrefsSaver.IsSoundOn)
				scSettings.SliderValue = DataPrefsSaver.SoundFactor;			

			else
			{
				DataPrefsSaver.SoundFactor = 0;
				scSettings.SliderValue = 0;
			}

			#region event listeners
			MenuEventsSettings.OnBtnSoundSlider += OnSoundSliderChange;
			MenuEventsSettings.OnBtnLanguage += OnFlag;
			MenuEventsSettings.OnBtnLikeFB += OnBtnFBLike;
			MenuEventsSettings.OnBtnRateUS += OnBtnRateUS;
			MenuEventsSettings.OnBtnRestore += OnBtnRestore;
			MenuEventsSettings.OnBtnBack += OnBtnBack;
			MenuEventsSettings.OnBtnBunnyBanner += OnBtnBunnyBanner;
			MenuEventsSettings.OnBtnTutorial += OnBtnTutorial;
			MenuEventsSettings.OnBtnToDevPanel += OnBtnToDevPanel;
			#endregion
		}

		public override void Stop()
		{
			menuHandler.scSettings.CanvasSettings.SetActive(false);
			menuHandler.scSettings.CanvasSettingsLeaves.SetActive(false);
			menuHandler.scSettings.CanvasBackBright.SetActive(false);

			#region event listeners
			MenuEventsSettings.OnBtnSoundSlider -= OnSoundSliderChange;
			MenuEventsSettings.OnBtnLanguage -= OnFlag;
			MenuEventsSettings.OnBtnLikeFB -= OnBtnFBLike;
			MenuEventsSettings.OnBtnRateUS -= OnBtnRateUS;
			MenuEventsSettings.OnBtnRestore -= OnBtnRestore;
			MenuEventsSettings.OnBtnBack -= OnBtnBack;
			MenuEventsSettings.OnBtnBunnyBanner -= OnBtnBunnyBanner;
			MenuEventsSettings.OnBtnTutorial -= OnBtnTutorial;
			MenuEventsSettings.OnBtnToDevPanel -= OnBtnToDevPanel;
			#endregion
		}

		#region On methods
		private void OnSoundSliderChange()
		{				
			menuHandler.PlayOneShotAuClip(
				settings.AuSoundSlider.Sound, settings.AuSoundSlider.Volume * scSettings.SliderValue);

			DataPrefsSaver.SoundFactor = scSettings.SliderValue;
			DataPrefsSaver.IsSoundOn = scSettings.SliderValue == 0 ? false : true;

			GameEvents.OnChangeSoundFactor(DataPrefsSaver.SoundFactor);
		}

		private void OnFlag(string id)
		{
//			Debug.Log("OnFlag ".StrColored(DebugColors.blue) + id);

			bool wasSelected = false;
			int len = scSettings.Flags.Length;
			for (int i = 0; i < len; i++)
			{
				FlagInSettings iFlag = scSettings.Flags[i];

				if(iFlag.Id == id)
				{
					wasSelected = true;

					scSettings.ActiveLight.anchoredPosition = iFlag.AnchorPos;
				}
			}

			if(!wasSelected)
			{
				#if DEVELOPMENT_BUILD
				Debug.Log("Id of flag wasn't finded".StrColored(DebugColors.red));
				#endif
			}
		}

		private void OnBtnFBLike()
		{
			Application.OpenURL(settings.FBLink);
		}

		private void OnBtnRateUS()
		{
			rateReq.RequestRate();
		}

		private void OnBtnRestore()
		{

		}

		private void OnBtnBack()
		{
			menuHandler.PlayAuBtnDefault();

			if(stateToBack == MenuStates.GameOver)
				menuHandler.ChangeState(menuHandler.backStateFromSettings, false);	
			
			else				
				menuHandler.ChangeState(menuHandler.backStateFromSettings);	
		}

		private void OnBtnBunnyBanner()
		{
			Debug.Log("BtnBunnyBanner".StrColored(DebugColors.blue));
			storeOpener.OpenBunnyLink();
		}

		private void OnBtnTutorial()
		{
			GameEvents.OnTutorial();
		}

		private void OnBtnToDevPanel()
		{
			menuHandler.ChangeState(MenuStates.DevTest);
		}

		#endregion

		[System.Serializable]
		public class SceneSettings
		{
			[SerializeField] private Slider soundSlide;
			[SerializeField] private RectTransform activeLight;
			[SerializeField] private Text txtLvlNumber;
			[SerializeField] private Text txtBeeKeeperLvl;
			[SerializeField] private FlagInSettings[] flags;	

			public float SliderValue {
				
				get { return soundSlide.value; }
				set { soundSlide.value = value; }
			}

			public RectTransform ActiveLight {  get { return activeLight; }}
			public Text TxtLvlNumber {  get { return txtLvlNumber; }}
			public Text TxtBeeKeeperLvl {  get { return txtBeeKeeperLvl; }}

			public FlagInSettings[] Flags {  get { return flags; }}

		}

		[System.Serializable]
		public class Settings
		{
			[SerializeField] private AudioClipHolder auSoundSlider;
			[SerializeField] private string fbLink;

			public AudioClipHolder AuSoundSlider {  get { return auSoundSlider; }}
			public string FBLink {  get { return fbLink; }}
		}

		[System.Serializable]
		public class FlagInSettings
		{
			[SerializeField] private string id;
			[SerializeField] private RectTransform rectTrans;

			public string Id {  get { return id; }}
//			public Image FlagImage {  get { return img; }}

			public Vector2 AnchorPos {  get { return rectTrans.anchoredPosition; }}
		}

	}
}