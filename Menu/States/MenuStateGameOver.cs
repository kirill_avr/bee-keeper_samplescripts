﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using DG.Tweening;
using Shorka.Ads;
using MovementEffects;

namespace Shorka.CellConnect
{
	public class MenuStateGameOver : MenuState 
	{
		#region fields
		private Sequence seqBtnWatch;
		private Vector3 initBtnWatchScale;

		private Vector3 initTxtCollScoreScale;
//		private readonly Vector3 initPosNewRecOffSet;
		private readonly Vector3 initLocalPosNewRec;

		private readonly Vector3 initBtnWatchAngles;
		private readonly Vector2 initPosBtnRectReplay;

		private bool canAnimBtnWatch;
		private bool canBtnWatch;

		private readonly int scoreEarnedInGame;
		private readonly int scoreEarnedInLvl;

		private readonly bool doAddEarnedScore;

		private readonly SceneSettings scSettings;
		private readonly Settings settings;
		#endregion

		public MenuStateGameOver(SceneSettings _scSettings, Settings _settings, 
			MenuHandler menuHandler, bool _doAddEarnedScore = false) : base(menuHandler)
		{
			scSettings = _scSettings;
			settings = _settings;
			doAddEarnedScore = _doAddEarnedScore;

			scoreEarnedInGame = menuHandler.gameInfo.GetMagicStarsInGame;
			scoreEarnedInLvl = menuHandler.gameInfo.GetMagicStarsWithinLvl;
				
			initLocalPosNewRec = scSettings.LabelNewRecord.localPosition;
//			Debug.Log("BEF_initPosNewRec: ".StrColored(DebugColors.purple) + initLocalPosNewRec);
			//			
//			initPosNewRecOffSet.x += settings.InitRecordOffSet.x;
//			initPosNewRecOffSet.y += settings.InitRecordOffSet.y;
			//			Debug.Log("Af_initPosNewRec: ".StrColored(DebugColors.purple) + initPosNewRecOffSet);

			initBtnWatchScale = scSettings.TransBtnWatch.localScale;
			initPosBtnRectReplay = scSettings.BtnRectReplay.anchoredPosition;
		}

		public override void Start()
		{
			initTxtCollScoreScale = scSettings.TxtCollScore.transform.localScale;

			canBtnWatch = true;
			canAnimBtnWatch = true;

			scSettings.LabelNewRecord.gameObject.SetActive(false);

			menuHandler.scSettings.CanvasEnd.SetActive(true);
			menuHandler.scSettings.CanvasBackLight.SetActive(true);

			#region Btnw watch settings
			if(doAddEarnedScore)
			{
				if(AdsBehaviour.Instance.IsUnityAdsReady())
				{
					scSettings.TransBtnWatch.gameObject.SetActive(true);
					scSettings.EnableGameObjForBtnWatch(true);
					scSettings.TransBtnWatch.eulerAngles = Constants.v3Zero;
				}
				else
				{
					scSettings.TransBtnWatch.gameObject.SetActive(false);
					scSettings.EnableGameObjForBtnWatch(false);
				}

			}
			else
			{
				if(!scSettings.TransBtnWatch.gameObject.activeInHierarchy)
					scSettings.BtnRectReplay.anchoredPosition = new Vector2(0,initPosBtnRectReplay.y);
			}
			#endregion

			scSettings.TxtAllStars.text = DataPrefsSaver.MagicStarsQTY.ToString();

			if(doAddEarnedScore)
				DataPrefsSaver.MagicStarsQTY += scoreEarnedInGame;

			#region Set High record
			if(scoreEarnedInGame > DataPrefsSaver.HighScore)
			{
				DataPrefsSaver.HighScore = scoreEarnedInGame;
				scSettings.LabelExclamation.text = settings.NewHighScore;
				Timing.RunCoroutine(IEAppearNewRecordWithDelay());
			}
			else 
				scSettings.LabelNewRecord.gameObject.SetActive(false);

			#endregion

			#region exlaim
			string exclam = settings.GetExclamation(scoreEarnedInGame);
			if(string.IsNullOrEmpty(exclam))
				Debug.LogError("Exlamation string is NULL or EMPTY for score: " + scoreEarnedInGame);

			scSettings.LabelExclamation.text = exclam;
			#endregion

//			Debug.Log("scoreEarned:".StrColored(DebugColors.blue) + scoreEarnedInGame);
			if(scoreEarnedInGame != 0)
			{
				scSettings.TransBtnWatch.SetAngleZ(settings.BtnWatch.Angle);
				menuHandler.asyncProc.StartCoroutine(IEAnimBtn(scSettings.TransBtnWatch));

				if(doAddEarnedScore)
					Timing.RunCoroutine(IEPlayAddStarsAnim(0, scoreEarnedInGame, DataPrefsSaver.MagicStarsQTY));
			}
				
			#region Level settings

			int lvlNumb = DataPrefsSaver.LvlNumber;
			int oldLvlStars = DataPrefsSaver.LvlStars;
			if(doAddEarnedScore)
			{
//				Debug.Log("oldLvlStars: " + oldLvlStars + " \n scoreEarnedInLvl: " + scoreEarnedInLvl);
				int newLvlStars = oldLvlStars + scoreEarnedInLvl;
				DataPrefsSaver.LvlStars = newLvlStars;
				menuHandler.canvasLvlBarCtrl.MakeActiveWithAnim(
					scSettings.PosLvlBar, lvlNumb, oldLvlStars, newLvlStars);
			}

			else
			{
				menuHandler.canvasLvlBarCtrl.MakeActive(scSettings.PosLvlBar, lvlNumb, oldLvlStars);
			}

			#endregion

			#region events listeners
			MenuEventsEnd.OnBtnHome += OnBtnHome;
			MenuEventsEnd.OnBtnWatchAd += OnBtnWatchAd;
			MenuEventsEnd.OnBtnReplay += OnBtnReplay;
			MenuEventsEnd.OnBtnSettings += OnBtnSettings;
			MenuEventsEnd.OnBtnShop += OnBtnShop;
			MenuEventsEnd.OnBtnLeaderBoard += OnBtnLeaderBoard;

			#if UNITY_EDITOR
			MenuTestEventsEnd.OnNewRecordAppear += OnTesNewRecordAppear;
			#endif

			AdsBehaviour.OnRewVideoFinished += OnRewVideoFinished;
			#endregion
		}		

		private IEnumerator<float> IEPlayAddStarsAnim(int startNumbColl, int endNumbColl, int endNumbAll)
		{
			menuHandler.CountCollScore(
				scSettings.TxtCollScore,
				startNumbColl, 
				endNumbColl,
				settings.DurCount);

			menuHandler.ExecuteSizeUpAnim(
				scSettings.TxtCollScore.transform, 
				initTxtCollScoreScale, 
				initTxtCollScoreScale * settings.CoeffTxtCollScore,
				settings.DurCount,
				settings.DurCount,
				settings.DurCount + settings.WTimeAfterCount
			);				

			//Enable partycles to emit stars to AllScore text
			scSettings.StarsPtcl.transform.SetPos2D(
				scSettings.TxtCollScore.transform.position
			);
			scSettings.StarsPtcl.gameObject.SetActive(true);
			scSettings.StarsPtcl.Play();
			//=====================================================
			yield return Timing.WaitForSeconds(settings.WTimeAfterPrtl);

			menuHandler.CountCollScore(
				scSettings.TxtAllStars,
				Int32.Parse(scSettings.TxtAllStars.text), 
				endNumbAll,
				settings.DurCount
			);
		}
			

		public override void Stop()
		{
			canAnimBtnWatch = false;
			scSettings.BtnRectReplay.anchoredPosition = initPosBtnRectReplay;
			scSettings.TransBtnWatch.localScale =  initBtnWatchScale;
			scSettings.TxtCollScore.transform.localScale = initTxtCollScoreScale;

			menuHandler.scSettings.CanvasEnd.SetActive(false);
			menuHandler.scSettings.CanvasBackLight.SetActive(false);
			menuHandler.canvasLvlBarCtrl.Disable();

			scSettings.StarsPtcl.gameObject.SetActive(false);
			scSettings.StarsPtcl.Stop();

			scSettings.LabelNewRecord.localPosition = initLocalPosNewRec;
			scSettings.LabelNewRecord.gameObject.SetActive(false);

//			int lenFireWorks = scSettings.FireWorks.Length;
//			for (int i = 0; i < lenFireWorks; i++) 
//			{
//				FireWorkGameOver item = scSettings.FireWorks[i];
//					
//				item.ParticleSys.Stop();
//				item.ParticleSys.gameObject.SetActive(false);
//			}

			if(seqBtnWatch!=null)
				seqBtnWatch.Kill();			


			#region events listeners
			MenuEventsEnd.OnBtnHome -= OnBtnHome;
			MenuEventsEnd.OnBtnWatchAd -= OnBtnWatchAd;
			MenuEventsEnd.OnBtnReplay -= OnBtnReplay;
			MenuEventsEnd.OnBtnSettings -= OnBtnSettings;
			MenuEventsEnd.OnBtnShop -= OnBtnShop;
			MenuEventsEnd.OnBtnLeaderBoard -= OnBtnLeaderBoard;

			AdsBehaviour.OnRewVideoFinished -= OnRewVideoFinished;

			#if UNITY_EDITOR
			MenuTestEventsEnd.OnNewRecordAppear -= OnTesNewRecordAppear;
			#endif

			#endregion
		}

		#region On methods
		private void OnBtnHome()
		{
			menuHandler.PlayAuBtnDefault();
			menuHandler.ChangeState(MenuStates.Home);
		}

		private void OnBtnWatchAd()
		{
			if(!canBtnWatch)
				return;
			
			AdsBehaviour.Instance.RewardedVideo();
			canBtnWatch = false;
		}

		private void OnBtnReplay()
		{
			menuHandler.PlayOneShotAuClip(menuHandler.settings.AuBtnPlay);
			GameEvents.OnRestartGame(true);
		}

		private void OnBtnSettings()
		{
			menuHandler.PlayAuBtnDefault();
			menuHandler.ChangeState(MenuStates.Settings, MenuStates.GameOver);
		}

		private void OnBtnShop()
		{
			menuHandler.PlayAuBtnDefault();
			menuHandler.ChangeState(MenuStates.Shop, MenuStates.GameOver);
		}

		private void OnRewVideoFinished()
		{
			DataPrefsSaver.MagicStarsQTY += scoreEarnedInGame;

			scSettings.TransBtnWatch.DOScale(Constants.v3Zero, settings.DurAnimBtnWatchHide);
			Timing.RunCoroutine(IEDisableBtnWatch(settings.DurAnimBtnWatchHide));
			scSettings.BtnRectReplay.transform.DOLocalMoveX(0, settings.DurAnimBtnReplace);

			Timing.RunCoroutine(IEPlayAddStarsAnim(
				Int32.Parse(scSettings.TxtCollScore.text),
				scoreEarnedInGame * 2,
				DataPrefsSaver.MagicStarsQTY));

			scSettings.EnableGameObjForBtnWatch(false);

			canAnimBtnWatch = false; 
		}
		private IEnumerator<float> IEDisableBtnWatch(float waitTime)
		{
			yield return Timing.WaitForSeconds(waitTime);
			scSettings.TransBtnWatch.gameObject.SetActive(false);
		}

		private void OnTesNewRecordAppear()
		{
			Debug.Log("OnTesNewRecordAppear".StrColored(DebugColors.blue));
			AppearNewRecord();
		}

		private void OnBtnLeaderBoard()
		{
			menuHandler.BtnLeaderBoard();
		}

		#endregion

		private IEnumerator IEAnimBtn(Transform btnTrans)
		{
			float duratAngles = settings.BtnWatch.DurAngles;
			Vector3 vecAngle1 = Constants.v3Z360.ReturnZ(-settings.BtnWatch.Angle);
			Vector3 vecAngle2 = Constants.v3Z360.ReturnZ(settings.BtnWatch.Angle);

			WaitForSeconds wForSecBtnWatch = new WaitForSeconds(settings.BtnWatch.WaitTime);		
			while (canAnimBtnWatch)
			{
				seqBtnWatch = DOTween.Sequence();
				seqBtnWatch.SetLoops(1);

				for (int i = 0; i < settings.BtnWatch.TimesQTY; i++)
				{
					seqBtnWatch.Append(btnTrans.DOLocalRotate(vecAngle1, duratAngles));
					seqBtnWatch.Append(btnTrans.DOLocalRotate(vecAngle2, duratAngles));	
				}

				yield return seqBtnWatch.WaitForCompletion();
//				Debug.Log("Tween completed!");
				seqBtnWatch = DOTween.Sequence();
				seqBtnWatch.Append(btnTrans.DOLocalRotate(Constants.v3Zero, duratAngles));
				yield return wForSecBtnWatch;			
			}
			yield return null;
		}
			
		private IEnumerator<float> IEAppearNewRecordWithDelay()
		{
			yield return Timing.WaitForSeconds(settings.DelayNewRecord);
			AppearNewRecord();
		}

		private void AppearNewRecord()
		{
			Vector3 posAppear = initLocalPosNewRec;
			posAppear.x = scSettings.TransPosObjNewRec.localPosition.x +  settings.NewRecordOffSet.x;

//			Vector3 posMove = new Vector3(initPosNew);
			scSettings.LabelNewRecord.localPosition = posAppear;
			scSettings.LabelNewRecord.DOLocalMoveX(initLocalPosNewRec.x, settings.DurNewRecordAppear).SetEase(settings.EaseNewRec);
			scSettings.LabelNewRecord.gameObject.SetActive(true);
			Timing.RunCoroutine(
				scSettings.IEPlayFireWorks(settings.DelayBeforeFireWorks, settings.MinMaxDelayFireWorks, menuHandler.particleWareH));
		}

		[Serializable]
		public class Settings
		{
			[Tooltip("Duration of adding new score")]
			[SerializeField] private float durCount;
		
			[SerializeField] private float wTimeAfterCount = 0.2F;
			[SerializeField] private float wTimeAfterPrtl = 0.2F;

			[Header("Settings, after rewarded ad was watched")]
			[SerializeField] private float wTimePtcl = 0.7F;
			[Tooltip("Coefficient/multiplier of TxtCollScore scale changing ")]
			[SerializeField] private float coeffTxtCollScore = 1.2F;
			[Tooltip("Duration of scaling animation of  TxtCollScore")]
			[SerializeField] private float durOfScaleTxtCollScore = 0.2F;
			[SerializeField] private float durAnimBtnWatchHide = 0.4F;
			[SerializeField] private float durAnimBtnReplace = 0.2F;

			[Space(5)]
			[Header("NewRecord Settings")]

			[Tooltip("Delay time of new record appearing")]
			[SerializeField] private float delayNewRecord = 0.3F;
			[SerializeField] private float durNewRecordAppear = 0.5F;
			[SerializeField] private Vector2 newRecordOffSet;
//			[SerializeField] private Vector2 initRecordOffSet;
			[SerializeField] private Ease easeNewRec;
			[Space(5)]
			[SerializeField] private float delayBeforeFireWorks = 0.2F;
			[SerializeField] private MinMax minMaxDelayFireWorks;

			[Space(10)]

			[SerializeField] private BtnWatchAnimSettings btnWatchSettings;		
			[Space(5)]
			[SerializeField] private float durFillLvlBar;

			[Space(5)]
			[SerializeField] private string newHighScore;
			[SerializeField] private Exclamation[] exclamations;

			public float DurCount {  get { return durCount; }}
			public float WTimeAfterCount {  get { return wTimeAfterCount; }}
			public float WTimeAfterPrtl {  get { return wTimeAfterPrtl; }}

			public float WTimePtcl {  get { return wTimePtcl; }}
			public float CoeffTxtCollScore {  get { return coeffTxtCollScore; }}
			public float DurOfScaleTxtCollScore {  get { return durOfScaleTxtCollScore; }}
			public float DurAnimBtnWatchHide {  get { return durAnimBtnWatchHide; }}
			public float DurAnimBtnReplace {  get { return durAnimBtnReplace; }}

			public BtnWatchAnimSettings BtnWatch {  get { return btnWatchSettings; }}
			public string NewHighScore {  get { return newHighScore; }}
			public Exclamation[] Exclamations {  get { return exclamations; }}

			/// <summary>
			/// Delay time of new record appearing
			/// </summary>
			public float DelayNewRecord {  get { return delayNewRecord; }}
			public float DurNewRecordAppear {  get { return durNewRecordAppear; }}
			public Ease EaseNewRec {  get { return easeNewRec; }}
			public Vector2 NewRecordOffSet {  get { return newRecordOffSet; }}
//			public Vector2 InitRecordOffSet {  get { return initRecordOffSet; }}

			public float DurFillLvlBar {  get { return durFillLvlBar; }}
			public float DelayBeforeFireWorks {  get { return delayBeforeFireWorks; }}
			public MinMax MinMaxDelayFireWorks {  get { return minMaxDelayFireWorks; }}
//			public LevelsSettings LvlSettings {  get { return lvlSettings; }}

			public string GetExclamation(int score)
			{
				int len = exclamations.Length;
				for (int i = 0; i < len; i++) 
				{
					Exclamation iExcl = exclamations[i];
					if(score >= iExcl.ScoreMin && score <= iExcl.ScoreMax )
						return iExcl.Word;					
				}
					
				return string.Empty;
			}

			[Serializable]
			public class BtnWatchAnimSettings
			{
				[SerializeField] private int timesQTY = 3;
				[SerializeField] private float coeff;
				[SerializeField] private float durAngles;
				[SerializeField] private float duration;
				[SerializeField] private float angle;
				[SerializeField] private float wTime = 0.5F;

				public int TimesQTY {  get { return timesQTY; }}
				public float Coeff {  get { return coeff; }}
				public float DurAngles {  get { return durAngles; }}
				public float Duration {  get { return duration; }}
				public float Angle {  get { return angle; }}
				public float WaitTime {  get { return wTime; }}
			}
		}

		[Serializable]
		public class SceneSettings
		{
			[SerializeField] private Transform transBtnWatch;
			[SerializeField] private Button btnWatch;
			[SerializeField] private GameObject[] gameObjsForBtnWatch;
			[Space(5)]
			[SerializeField] private Transform labelNewRecord;
			[SerializeField] private Transform transPosObjNewRec;
			[Space(5)]
			[SerializeField] private RectTransform btnRectReplay;
			[SerializeField] private Text labelExclamation;
			[SerializeField] private Text txtCollScore;
			[SerializeField] private Text txtAllStars;
			[Space(5)]
			[SerializeField] private ParticleSystem starsPtcl;
			[SerializeField] private Transform posObjLvlBar;
			[Space(5)]
			[SerializeField] private ParticlePosAndID[] fireWorks;


			public Transform TransBtnWatch {  get { return transBtnWatch; }}
			public Button BtnWatch {  get { return btnWatch; }}
			public ParticlePosAndID[] FireWorks {  get { return fireWorks; }}

			public RectTransform BtnRectReplay {  get { return btnRectReplay; }}
			public Text TxtCollScore {  get { return txtCollScore; }}
			public Text LabelExclamation {  get { return labelExclamation; }}
			public Text TxtAllStars {  get { return txtAllStars; }}

			public ParticleSystem StarsPtcl {  get { return starsPtcl; }}

			public Transform LabelNewRecord {  get { return labelNewRecord; }}
			public Transform TransPosObjNewRec {  get { return transPosObjNewRec; }}

			public Vector3 PosLvlBar {  get { return posObjLvlBar.position; }}


			public IEnumerator<float> IEPlayFireWorks(float delayBeforePlay, MinMax minMaxDelayTime, ParticleWarehouse partWarehouse)
			{
				int len = fireWorks.Length;
				yield return Timing.WaitForSeconds(delayBeforePlay);

				for (int i = 0; i < len; i++) 
				{
					ParticlePosAndID item = fireWorks[i];
					ParticleSystemOptimized partOpt = partWarehouse.GetFreeParticleSystem(item.IdParticle);
					partOpt.SetActive(true);
					partOpt.parentTrans.SetPos2D(item.PosPointer.position);
					partOpt.Play();

					yield return Timing.WaitForSeconds(minMaxDelayTime.RandomValue());
				}
			}

			public void EnableGameObjForBtnWatch(bool doEnable)
			{
				int len = gameObjsForBtnWatch.Length;
				for (int i = 0; i < len; i++) 
					gameObjsForBtnWatch[i].SetActive(doEnable);				
			}
		}

		[Serializable]
		public class Exclamation
		{
			[SerializeField] private string word;
			[Range(0,90000)]
			[SerializeField] private int scoreMin;
			[Range(0,90000)]
			[SerializeField] private int scoreMax;

			public string Word {  get { return word; }}
			public int ScoreMin {  get { return scoreMin; }}
			public int ScoreMax {  get { return scoreMax; }}
		}
	}



}