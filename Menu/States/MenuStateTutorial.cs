﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace Shorka.CellConnect
{
	public class MenuStateTutorial : MenuState 
	{
		private readonly string tutText;
		private readonly SceneSettings scSettings;
//		private readonly Settings settings;

		public MenuStateTutorial(SceneSettings _scSettings, MenuHandler menuHandler, string tutText = "NONE") : base(menuHandler)
		{
			scSettings = _scSettings;
//			settings = _settings;
			this.tutText = tutText;
		}
		public override void Start()
		{
			scSettings.Text.text = tutText;

			menuHandler.scSettings.CanvasTutorial.SetActive(true);
			menuHandler.canvasFadedCtrl.SetActive(true);
			menuHandler.canvasFadedCtrl.AnimFade(true);

			MenuEventsTutorial.OnBtnOKTutorial += OnOk;
		}

		public override void Stop()
		{
			menuHandler.scSettings.CanvasTutorial.SetActive(false);
			menuHandler.canvasFadedCtrl.SetActive(false);

			MenuEventsTutorial.OnBtnOKTutorial -= OnOk;
		}

		private void OnOk()
		{
			menuHandler.PlayAuBtnDefault();
			menuHandler.ChangeState(MenuStates.None);
			GameEvents.OnBtnOkInTut();
		}

//		[System.Serializable]
//		public class Settings
//		{
//		}

		[System.Serializable]
		public class SceneSettings
		{
			[SerializeField] private Text text;

			public Text Text {  get { return text; }}
		}
	}
}