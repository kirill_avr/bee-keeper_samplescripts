﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
namespace Shorka.CellConnect
{
	public class MenuStateHome : MenuState 
	{
		private Vector3 posInitBeeL;
		private Vector3 posInitBeeR;

		private readonly SceneSettings scSettings;
		private readonly Settings settings;

		public MenuStateHome(SceneSettings _scSettings, Settings _settings, MenuHandler menuHandler) : base(menuHandler)
		{
			scSettings = _scSettings;
			settings = _settings;
		}

		public override void Start()
		{
//			Debug.Log("MenuStateHome");

			menuHandler.scSettings.CanvasHome.SetActive(true);
			scSettings.TxtHighScore.text = DataPrefsSaver.HighScore.ToString();
			scSettings.TxtStarScore.text = DataPrefsSaver.MagicStarsQTY.ToString();
			posInitBeeL = scSettings.BeeLeft.position;
			posInitBeeR = scSettings.BeeRight.position;

			if(!menuHandler.doAnimCoverBee)
			{
				menuHandler.doAnimCoverBee = true;
				AppearBees();
			}
			menuHandler.canvasLvlBarCtrl.MakeActive(scSettings.PosLvlBar, DataPrefsSaver.LvlNumber, DataPrefsSaver.LvlStars);

			MenuEventsHome.OnBtnPlay += OnBtnPlay;
			MenuEventsHome.OnBtnShop += OnBtnShop;
			MenuEventsHome.OnBtnSettings += OnBtnSettings;
			MenuEventsHome.OnBtnLeaderBoard += OnBtnLeaderBoard;
			MenuTestEventsHome.OnBeesAppear += OnBeesAppear;
		}

		public override void Stop()
		{
			menuHandler.scSettings.CanvasHome.SetActive(false);
			menuHandler.canvasLvlBarCtrl.Disable();

			MenuEventsHome.OnBtnPlay -= OnBtnPlay;
			MenuEventsHome.OnBtnShop -= OnBtnShop;
			MenuEventsHome.OnBtnSettings -= OnBtnSettings;
			MenuEventsHome.OnBtnLeaderBoard -= OnBtnLeaderBoard;
			MenuTestEventsHome.OnBeesAppear -= OnBeesAppear;
		}

		#region On methods

		private void OnBtnPlay()
		{
//			menuHandler.PlayOneShotAuClip(menuHandler.settings.AuBtnPlay);
			menuHandler.soundWareHouse.PlayOneShotById(settings.IdAuBtnPlay);
			GameEvents.OnStartPlay();
		}

		private void OnBtnShop()
		{
			menuHandler.PlayAuBtnDefault();
			menuHandler.ChangeState(MenuStates.Shop);
		}

		private void OnBtnSettings()
		{
			menuHandler.PlayAuBtnDefault();
			menuHandler.ChangeState(MenuStates.Settings, MenuStates.Home);
		}

		private void OnBeesAppear()
		{
			AppearBees();
		}

		private void OnBtnLeaderBoard()
		{
			menuHandler.BtnLeaderBoard();
		}

		private void OnBtnAchiev()
		{
		}

		#endregion

		private void AppearBees()
		{
			MoveBee(scSettings.BeeLeft, posInitBeeL, scSettings.PosObjBeeLeftStart.position, settings.PosOffSetBeeL);
			MoveBee(scSettings.BeeRight, posInitBeeR, scSettings.PosObjBeeRightStart.position, settings.PosOffSetBeeR);
		}

		private void MoveBee(Transform bee, Vector3 initPos, Vector3 posHide, Vector2 offSet)
		{
			posHide.x += offSet.x;
			posHide.y += offSet.y;
			bee.position = posHide;
			bee.DOMove(initPos, settings.DurBeeAnim).SetEase(settings.EaseBeeMove);
		}

		[System.Serializable]
		public class SceneSettings
		{
//			[SerializeField] private Image imgBeeLeft;
			[SerializeField] private RectTransform beeLeft;
			[SerializeField] private RectTransform beeRight;

			[Space(5)]
			[SerializeField] private RectTransform posObjBeeLeftStart;
			[SerializeField] private RectTransform posObjBeeRightStart;

			[Space(10)]
			[SerializeField] private Text txtStarScore;
			[SerializeField] private Text txtHighScore;
//			[SerializeField] private Text txtLevelNumb;

//			[Space(5)]
//			[Header("Lvl bar elements")]
//			[SerializeField] private Image imgBarAmount;
//			[SerializeField] private Text txtLevelNumbLeft;
//			[SerializeField] private Text txtLevelNumbRight;
			[SerializeField] private Transform posObjLvlBar;

			public Text TxtStarScore {  get { return txtStarScore; }}
			public Text TxtHighScore {  get { return txtHighScore; }}
//			public Text TxtLevelNumb {  get { return txtLevelNumb; }}

//			public Image ImgBeeLeft {  get { return imgBeeLeft; }}
			public RectTransform BeeLeft {  get { return beeLeft; }}
			public RectTransform BeeRight {  get { return beeRight; }}

			public RectTransform PosObjBeeLeftStart {  get { return posObjBeeLeftStart; }}
			public RectTransform PosObjBeeRightStart {  get { return posObjBeeRightStart; }}

			public Vector3 PosLvlBar {  get { return posObjLvlBar.position; }}

//			public Text TxtLevelNumbLeft {  get { return txtLevelNumbLeft; }}
//			public Text TxtLevelNumbRight {  get { return txtLevelNumbRight; }}
//			public Image ImgBarAmount {  get { return imgBarAmount; }}
		}

		[System.Serializable]
		public class Settings
		{
			[Tooltip("Bee anim settings")]
			[SerializeField] private float durBeeAnim;
			[SerializeField] private Ease easeBeeMove;
			[SerializeField] private Vector2 posOffSetBeeL;
			[SerializeField] private Vector2 posOffSetBeeR;

			[Space(5)]
			[SerializeField] private string idAuBtnPlay = "HarpMagic";

//			[Space(10)]
//			[SerializeField] private AudioClipHolder auBtnPlay;

//			public AudioClipHolder AuBtnPlay {  get { return auBtnPlay; }}
			public float DurBeeAnim {  get { return durBeeAnim; }}
			public Ease EaseBeeMove {  get { return easeBeeMove; }}

			public Vector2 PosOffSetBeeL {  get { return posOffSetBeeL; }}
			public Vector2 PosOffSetBeeR {  get { return posOffSetBeeR; }}

			public string IdAuBtnPlay {  get { return idAuBtnPlay; }}
		}
	}
}