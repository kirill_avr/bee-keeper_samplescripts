﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using MovementEffects;
using Shorka.Ads;

namespace Shorka.CellConnect
{
	public class MenuStateOutOf : MenuState 
	{
		public enum Case    {	OutOfPU,	OutOfMagicStars, LockPU, NoSpaceForNextSet  };

//		private readonly Vector3[] initLocalPosMultiBtns = new Vector3[3];
		private Vector3 initLocalPosBtnBuy;
		private Vector3 initLocalPosBtnAd;
		private Vector3 initLocalPosBtnShopMore;
		private bool isEnoughMoneyForPU;

		private readonly SceneSettings scSettings;
		private readonly Settings settings;
		private readonly Case caseType;
		private readonly string puName;
		private readonly int lvlNumbAllowed;
		private readonly Vector3 initScaleMessageBox;
		private readonly PowerUp powerUp;

		public MenuStateOutOf(SceneSettings _scSettings, Settings _settings,
			MenuHandler menuHandler, Case _caseType = Case.OutOfPU, string _puName = "Undo Power", int _lvlNumb = 0) : base(menuHandler)
		{
			scSettings = _scSettings;
			settings = _settings;
			puName = _puName;
			caseType = _caseType;
			lvlNumbAllowed = _lvlNumb;

			initScaleMessageBox = scSettings.MessageBox.localScale;

			powerUp = menuHandler.powerUpsCtrl.GetPowerUpByName(puName);
//			if(powerUp == null)
//			{
//				Debug.LogError("PoweUp object is null with id: " + puName);
//			}
		}

		public override void Start()
		{
			#if UNITY_EDITOR
			Debug.Log("MenuStateOutOf: " + caseType);
			#endif

			menuHandler.canvasFadedCtrl.SetFadeOut();
			menuHandler.canvasFadedCtrl.SetActive(true);

			DataInWindow data = settings.GetDataInWindow(caseType);
			data.Init();

			switch (caseType)
			{
			case Case.OutOfPU:
				{
					scSettings.TxtBuyPrice.text = powerUp.Price.ToString();

					data.TxtOfLabel2 = puName.ToUpper();	
					scSettings.BtnClose.SetActive(true);
					scSettings.BtnOk.SetActive(false);
					scSettings.EnableMultiBtns(true);

					initLocalPosBtnBuy = scSettings.BtnBuy.localPosition;
					initLocalPosBtnAd = scSettings.BtnAd.localPosition;
					initLocalPosBtnShopMore = scSettings.BtnShop.localPosition;		

					float rightEd = menuHandler.camCtrl.CameraForeEdgeRight.x + settings.OffSetXForMultiBtn.x;
					float leftEd = menuHandler.camCtrl.CameraForeEdgeLeft.x - settings.OffSetXForMultiBtn.x;
					scSettings.BtnBuy.SetPosX(leftEd);
					scSettings.BtnAd.SetPosX(rightEd);
					scSettings.BtnShop.SetPosX(leftEd);


					Vector3 localPosAppearBtnBuy = initLocalPosBtnBuy;
					Vector3 localPosAppearBtnAd = initLocalPosBtnAd;
					Vector3 localPosAppearBtnShopMore = initLocalPosBtnShopMore;

					//Has NO stars for PowerUp
					if(DataPrefsSaver.MagicStarsQTY < powerUp.Price)
					{
						scSettings.BtnBuy.gameObject.SetActive(false);
						isEnoughMoneyForPU = false;
						localPosAppearBtnAd = initLocalPosBtnBuy;
						localPosAppearBtnShopMore = initLocalPosBtnAd;
					}

					//Has stars for PowerUp
					else
						isEnoughMoneyForPU = true;

//					Debug.Log("isEnoughMoneyForPU; ".StrColored(DebugColors.aqua) + isEnoughMoneyForPU);

					scSettings.BtnBuy.SetLocalPosY(localPosAppearBtnBuy.y);
					scSettings.BtnAd.SetLocalPosY(localPosAppearBtnAd.y);
					scSettings.BtnShop.SetLocalPosY(localPosAppearBtnShopMore.y);

					Timing.RunCoroutine(
						IEMultiBtnsAppear(localPosAppearBtnBuy, localPosAppearBtnAd, localPosAppearBtnShopMore)
					);

					scSettings.Label2.gameObject.SetActive(true);
					menuHandler.canvasFadedCtrl.AnimFade(true);
					break;
				}

			case Case.LockPU:
				{
					
				data.TxtOfLabel1 = puName.ToUpper() + data.TxtOfLabel1;
				data.TxtOfLabel2 += lvlNumbAllowed;
				scSettings.EnableAllBtns(true);
				scSettings.Label2.gameObject.SetActive(true);
				menuHandler.canvasFadedCtrl.AnimFade(true);
				scSettings.EnableMultiBtns(false);	
				break;
				}
			case Case.NoSpaceForNextSet:
			{
				data.TxtOfLabel1 = data.TxtOfLabel1;
				scSettings.EnableAllBtns(false);
				scSettings.Label2.gameObject.SetActive(false);
				scSettings.MessageBox.localScale = Constants.v3Zero;
				scSettings.MessageBox.DOScale(initScaleMessageBox, 0.8F);
				scSettings.EnableMultiBtns(false);
				break;
			}
			default:
				{
				scSettings.EnableAllBtns(true);
				scSettings.Label2.gameObject.SetActive(true);
				menuHandler.canvasFadedCtrl.AnimFade(true);
				scSettings.EnableMultiBtns(false);
				break;
				}
			}

			scSettings.Label1.gameObject.SetActive(true);
			SetActiveBackCanvas(true);
			menuHandler.scSettings.CanvasOutOfPu.SetActive(true);
			scSettings.EnableGraphycsRayCast(caseType == Case.NoSpaceForNextSet ? false : true);


			SetData(data);

			MenuEventsOutOf.OnBtnCloseOfPU += OnClose;
			MenuEventsOutOf.OnBtnGetPUinOutOfPU += OnGet;
			MenuEventsOutOf.OnBtnBuy += OnBuy;
			MenuEventsOutOf.OnBtnAd += OnAd;
			MenuEventsOutOf.OnBtnShopForMore += OnShopForMore;
		}

		private IEnumerator<float> IEMultiBtnsAppear(Vector3 posAppearBuy, Vector3 posAppearAd, Vector3 posAppearShop)
		{
			if(isEnoughMoneyForPU)
			{
				scSettings.BtnBuy.DOLocalMove(posAppearBuy,settings.DurAppearMultiBtn).SetEase(settings.EaseAppearMultiBtn);
				yield return Timing.WaitForSeconds(settings.WTimeBtwAppearMultiBtn);
			}

			scSettings.BtnAd.DOLocalMove(posAppearAd, settings.DurAppearMultiBtn).SetEase(settings.EaseAppearMultiBtn);
			yield return Timing.WaitForSeconds(settings.WTimeBtwAppearMultiBtn);

			scSettings.BtnShop.DOLocalMove(posAppearShop, settings.DurAppearMultiBtn).SetEase(settings.EaseAppearMultiBtn);

		}

		public override void Stop()
		{
//			menuHandler.scSettings.CanvasInGame.SetActive(false);
			menuHandler.canvasFadedCtrl.SetActive(false);
			menuHandler.scSettings.CanvasOutOfPu.SetActive(false);
			scSettings.MessageBox.localScale = initScaleMessageBox;
			SetActiveBackCanvas(false);

			if(caseType == Case.OutOfPU)
			{
				scSettings.EnableMultiBtns(false);
				scSettings.BtnBuy.localPosition = initLocalPosBtnBuy;
				scSettings.BtnAd.localPosition = initLocalPosBtnAd;
				scSettings.BtnShop.localPosition = initLocalPosBtnShopMore;
			}				

			MenuEventsOutOf.OnBtnCloseOfPU -= OnClose;
			MenuEventsOutOf.OnBtnGetPUinOutOfPU -= OnGet;

			MenuEventsOutOf.OnBtnBuy -= OnBuy;
			MenuEventsOutOf.OnBtnAd -= OnAd;
			MenuEventsOutOf.OnBtnShopForMore -= OnShopForMore;
		}

		#region On methods
		private void OnClose()
		{
			Debug.Log("OnClose Btn".StrColored(DebugColors.blue));

			PowerUpsEvents.OnNone();
			menuHandler.PlayAuBtnDefault();
			switch(caseType)
			{
			case Case.OutOfPU:
				menuHandler.ChangeState(MenuStates.InGame);
				break;

			case Case.OutOfMagicStars:
				menuHandler.ChangeState(MenuStates.Shop, MenuStates.OutOfPU);
				break;

			case Case.LockPU:
				menuHandler.ChangeState(MenuStates.InGame);
				break;

			default:
				menuHandler.ChangeState(MenuStates.InGame);
				break;
			}

//			Timing.RunCoroutine(IEClose());
		}
			
		private void OnGet()
		{
			menuHandler.PlayAuBtnDefault();
			PowerUpsEvents.OnNone();

			if(caseType == Case.OutOfPU)
			{
				menuHandler.ChangeState(MenuStates.Shop, MenuStates.InGame);
			}
			else if(caseType == Case.OutOfMagicStars)
			{
				menuHandler.ChangeState(MenuStates.Shop, MenuStates.OutOfPU, MenuStateShop.Tab.BuyMagicStars);
			}

			else if(caseType == Case.LockPU)
			{
				menuHandler.ChangeState(MenuStates.InGame);
			}
		}

		private void OnBuy()
		{
			Debug.Log("Press on Buy btn".StrColored(DebugColors.blue));
			DataPrefsSaver.MagicStarsQTY -= powerUp.Price;
			AddPU(powerUp.Type, 1);
			menuHandler.ChangeState(MenuStates.InGame);
			menuHandler.soundWareHouse.PlayOneShotById(settings.IdAuClipBuy);
		}

		private void OnAd()
		{
			Debug.Log("Press on Ad btn".StrColored(DebugColors.blue));
			AdsBehaviour.Instance.RewardedVideo();
			AddPU(powerUp.Type, 1);
			menuHandler.ChangeState(MenuStates.InGame);
		}

		private void OnShopForMore()
		{
			Debug.Log("Press on OnShopForMore btn".StrColored(DebugColors.blue));

			menuHandler.ChangeState(MenuStates.Shop, MenuStates.InGame,  
				isEnoughMoneyForPU ? MenuStateShop.Tab.SuperPower : MenuStateShop.Tab.BuyMagicStars );
		}

		private void OnRewVideoFinished()
		{
			AddPU(powerUp.Type, 1);
			menuHandler.ChangeState(MenuStates.InGame);
		}

		#endregion

		private void AddPU(PowerUpTypes puType, int addCount)
		{
//			int newQTYPU = DataPrefsSaver.GetPowerUpQTY(puType) + addCount;
			DataPrefsSaver.SetPowerUpQTY(puType,
				DataPrefsSaver.GetPowerUpQTY(puType) + addCount);
		}

		private void SetData(DataInWindow data)
		{
			scSettings.Label1.text = data.TxtOfLabel1.Replace(Constants.STR_NEW_LINE_EDITOR, Constants.STR_NEW_LINE);

			scSettings.Label2.text = data.TxtOfLabel2;
			scSettings.Label2.color = data.ColLabel2;
				
			scSettings.TxtInBtn.text = data.BtnText;
		}

		private void SetActiveBackCanvas(bool isActive)
		{
			switch(caseType)
			{
			case Case.OutOfPU:
				menuHandler.scSettings.CanvasInGame.SetActive(isActive);
				break;

			case Case.OutOfMagicStars:
				menuHandler.scSettings.CanvasShop.SetActive(isActive);
				menuHandler.scSettings.CanvasBackBright.SetActive(isActive);
				break;

			case Case.LockPU:
				menuHandler.scSettings.CanvasInGame.SetActive(isActive);
				break;

			default:
				menuHandler.scSettings.CanvasInGame.SetActive(isActive);
				break;
			}
		}



		[System.Serializable]
		public class SceneSettings
		{
			[SerializeField] private Text label1;
			[SerializeField] private Text label2;
			[SerializeField] private Text txtInBtn;
			[SerializeField] private GameObject btnOk;
			[SerializeField] private GameObject btnClose;
			[SerializeField] private Transform messageBox;
			[SerializeField] private GraphicRaycaster grRayCastOfCanvasOutPU;
			[Space(5)]
			[SerializeField] private Transform btnBuy;
			[SerializeField] private Text txtBuyPrice;
			[SerializeField] private Transform btnAd;
			[SerializeField] private Transform btnShop;

			public Text Label1 {  get { return label1; }}
			public Text Label2 {  get { return label2; }}
			public Text TxtInBtn {  get { return txtInBtn; }}
			public Transform MessageBox {  get { return messageBox; }}

			public GameObject BtnOk {  get { return btnOk; }}
			public GameObject BtnClose {  get { return btnClose; }}

			public Transform BtnBuy {  get { return btnBuy; }}
			public Text TxtBuyPrice {  get { return txtBuyPrice; }}
			public Transform BtnAd {  get { return btnAd; }}
			public Transform BtnShop {  get { return btnShop; }}
	

			public void EnableAllBtns(bool doEnable)
			{
				btnOk.SetActive(doEnable);
				btnClose.SetActive(doEnable);
			}

			public void EnableGraphycsRayCast(bool doEnable)
			{
				grRayCastOfCanvasOutPU.enabled = doEnable;
			}

			public void EnableMultiBtns(bool doEnable)
			{
				btnBuy.gameObject.SetActive(doEnable);
				btnAd.gameObject.SetActive(doEnable);
				btnShop.gameObject.SetActive(doEnable);
			}
		}

		[System.Serializable]
		public class Settings
		{
			[SerializeField] private Vector2 offSetXForMultiBtn;
			[SerializeField] private float durAppearMultiBtn = 0.6F;
			[SerializeField] private float wTimeBtwAppearMultiBtn = 0.5F;
//			[SerializeField] private Orientations orientAppearFrom;

			[SerializeField] private Ease easeAppearMultiBtn;
			[Space(5)]
			[SerializeField] private string idAuClipBuy;

			[SerializeField] private DataInWindow[] arrDataInWindow;

			public DataInWindow[] ArrDataInWindow {  get { return arrDataInWindow; }}
			public Vector2 OffSetXForMultiBtn {  get { return offSetXForMultiBtn; }}
			public float DurAppearMultiBtn {  get { return durAppearMultiBtn; }}
			public float WTimeBtwAppearMultiBtn {  get { return wTimeBtwAppearMultiBtn; }}

			public Ease EaseAppearMultiBtn {  get { return easeAppearMultiBtn; }}
//			public Orientations OrientAppearFrom {  get { return orientAppearFrom; }}
			public string IdAuClipBuy {  get { return idAuClipBuy; }}

			public DataInWindow GetDataInWindow(Case caseType)
			{
				int len = arrDataInWindow.Length;
				for (int i = 0; i < len; i++) 
				{
					DataInWindow iData = arrDataInWindow[i];
					if(iData.CaseType == caseType) 
						return iData;					
				}

				return null;
			}
		}

		[System.Serializable]
		public class DataInWindow
		{
			[SerializeField] private Case caseType;
			[SerializeField] private string btnText;
			[SerializeField] private string txtOfLabel1;
			[SerializeField] private string txtOfLabel2;
			[SerializeField] private Color colLabel2;

			public Case CaseType {  get { return caseType; }}
			public string BtnText {  get { return btnText; }}

			public string TxtOfLabel1 { get; set;}	

			public string TxtOfLabel2  { get;set;}	

			public Color ColLabel2 {  get { return colLabel2; }}

//			public string TxtOfLabel1 {get;set;}
//			public string TxtOfLabel2 {get;set;}
//
			public void Init ()
			{
				TxtOfLabel1 = txtOfLabel1;
				TxtOfLabel2 = txtOfLabel2;
			}
		}

//		public class DataInWindowChangable
//		{
//			public readonly Case caseType;
//			public readonly string btnText;
//			public readonly string txtOfLabel1;
//			public readonly string txtOfLabel2;
//			public readonly Color colLabel2;
//
//			internal DataInWindowChangable(DataInWindow data)
//			{
//				caseType = data.CaseType;
//				btnText = data.BtnText;
//				txtOfLabel1 = data.TxtOfLabel1;
//				txtOfLabel2 = data.TxtOfLabel2;
//				colLabel2 = data.c
//			}
//		}
	}
}