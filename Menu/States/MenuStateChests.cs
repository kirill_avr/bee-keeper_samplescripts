﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using MovementEffects;

namespace Shorka.CellConnect
{
	public class MenuStateChests : MenuState
	{
		private enum OpenChestTypes { Manual, Automatic };

		#region fields
		private readonly SceneSettings scSettings;
		private readonly Settings settings;
		private readonly HashSet<KeyType> catchedKeys;
		private readonly Vector3 initAnchorPosBtnBack;

		private ChestKeyDrag[] arrChestKeys;
		private OpenChestTypes typeOfOpenChest;

		private Vector3 initScaleGift;
		private Vector3 posTxtStars;

		private Vector3[] waypointsLeft = new[]  { new Vector3(-2.581874f,-0.353038f,-6.200195f),
			new Vector3(-2.581874f,1.287805f,-6.200195f), 
			new Vector3(-0.6520978f,2.194176f,-6.200195f) };
		
		private Vector3[] waypointsRight = new[]  { new Vector3(-2.581874f,-0.353038f,-6.200195f),
			new Vector3(2.581874f,1.287805f,-6.200195f), 
			new Vector3(-0.6520978f,2.194176f,-6.200195f) };
		#endregion

		public MenuStateChests(SceneSettings _scSettings, Settings _settings, MenuHandler menuHandler, HashSet<KeyType> _catchedKeys = null) : base(menuHandler)
		{
			scSettings = _scSettings;
			settings = _settings;
			catchedKeys = _catchedKeys;

			initAnchorPosBtnBack = scSettings.BtnBack.anchoredPosition;
		}

		public override void Start()
		{	
			typeOfOpenChest = OpenChestTypes.Manual;
			scSettings.BtnBack.gameObject.SetActive(false);


			scSettings.BtnBack.anchoredPosition = new Vector2(
				scSettings.BtnBack.anchoredPosition.x + settings.OffSetBtnBack.x , 
				scSettings.BtnBack.anchoredPosition.y + settings.OffSetBtnBack.y);
			

			int lenGifts = scSettings.GiftsInScene.Length;
			for (int i = 0; i < lenGifts; i++) 
			{
				GiftInChestScene item = scSettings.GiftsInScene[i];
				item.SetActive(false);	
				item.PsBackLightParent.Stop();
			}
					
						
			scSettings.SetPicToAllChests(settings.PicChestClose);
			scSettings.TxtStars.text = DataPrefsSaver.MagicStarsQTY.ToString();

			#region chest keys initialization
			scSettings.DisableAllChestKeys();

			if(catchedKeys != null)
			{
				scSettings.TxtKeysQTY.text = catchedKeys.Count.ToString();
				arrChestKeys = scSettings.EnableChestKeys(catchedKeys);
			}			
			
			else
			{
				Debug.LogError("catchedKeys is NULL");
				scSettings.TxtKeysQTY.text = 0.ToString();
			}

			#endregion

			menuHandler.scSettings.CanvasChest.SetActive(true);
			menuHandler.scSettings.CanvasBackLight.SetActive(true);
			initScaleGift = scSettings.GiftsInScene[0].TransGift.localScale;
			posTxtStars = scSettings.TxtStars.transform.position;

			scSettings.PsScoreCongrats.transform.SetPos2D(posTxtStars);		


			#region setUp wayPoints
			waypointsLeft[waypointsLeft.Length-1] = scSettings.PosGiftPosEndPointer;
			waypointsRight[waypointsRight.Length-1] = scSettings.PosGiftPosEndPointer;

			int lenChest = scSettings.ChestDrops.Length;
			for (int i = 0; i < lenChest; i++) 
			{
				ChestDrop iChestDrop = scSettings.ChestDrops[i];
				iChestDrop.SetIconActive(true);

				if(iChestDrop.KeyType == KeyType.Key1)
					waypointsLeft[0] = iChestDrop.Position;

				else if (iChestDrop.KeyType == KeyType.Key3)
					waypointsRight[0] = iChestDrop.Position;				
			}
			#endregion
//			Debug.Log("initGiftPos: ".StrBold() + scSettings.PosGiftPosEndPointer);

			scSettings.SetGiftActive(false);
//			scSettings.PSBackLight.Stop();

			#region events 
			MenuEventsChests.OnOpenChest += OnOpenChest;
			MenuEventsChests.OnBtnBack += OnBtnBack;
			MenuEventsChests.OnWrongKey += OnWrongKey;
			MenuEventsChests.OnGiftTap += OnGiftTap;
			MenuEventsChests.OnCollectAll += OnOpenAll;
			#endregion
		}
			
		public override void Stop()
		{
			menuHandler.scSettings.CanvasChest.SetActive(false);
			menuHandler.scSettings.CanvasBackLight.SetActive(false);

//			scSettings.BtnBack.localPosition = initLocalPosBtnBack;
			scSettings.BtnBack.anchoredPosition = initAnchorPosBtnBack;

			scSettings.SetGiftActive(false);
//			scSettings.PSBackLight.Stop();

			int lenGifts = scSettings.GiftsInScene.Length;
			for (int i = 0; i < lenGifts; i++) 
			{
				GiftInChestScene item = scSettings.GiftsInScene[i];
				item.TransGift.localScale = initScaleGift;	
			}

			#region events 
			MenuEventsChests.OnOpenChest -= OnOpenChest;
			MenuEventsChests.OnBtnBack -= OnBtnBack;
			MenuEventsChests.OnWrongKey -= OnWrongKey;
			MenuEventsChests.OnGiftTap -= OnGiftTap;
			MenuEventsChests.OnCollectAll -= OnOpenAll;
			#endregion
		}

		#region On methods
		private void OnOpenChest(KeyType keyType)
		{
			Debug.Log("OnOpenChest".StrColored(DebugColors.aqua) + keyType);

			OpenChest(scSettings.GetChestDrop(keyType), true);
		}

		private void OnBtnBack()
		{
			menuHandler.ChangeState(MenuStates.GameOver, true);
		}

		private void OnWrongKey()
		{
			menuHandler.PlayOneShotAuClip(settings.AuChestClose);
		}

		private void OnGiftTap(Transform transGift)
		{
			Debug.Log("OnGiftTap".StrColored(DebugColors.blue));

//			scSettings.PSBackLight.Stop();
//			scSettings.PSBackLight.gameObject.SetActive(false);
			GiftInChestScene gift = scSettings.GetGiftInScene(transGift);
			gift.PsBackLightParent.Stop();
			gift.PsBackLightParent.gameObject.SetActive(false);

			transGift.DOMove(posTxtStars, settings.DurGiftAfterTap);
			transGift.DOScale(initScaleGift.x*settings.ScaleFactorAfterTap, settings.DurGiftAfterTap);
			Timing.RunCoroutine(IEScoreCongrats(transGift.gameObject, settings.DurGiftAfterTap));
		}

		private void OnOpenAll()
		{
			Debug.Log("OnOpenAll()".StrColored(DebugColors.blue));
			if(typeOfOpenChest == OpenChestTypes.Automatic)
				return;

			typeOfOpenChest = OpenChestTypes.Automatic;

			Timing.RunCoroutine(IEOpenAll());
		}

		#endregion

		private IEnumerator<float> IEOpenAll()
		{
			int len = arrChestKeys.Length;

			for (int i = 0; i < len; i++) 
			{
				ChestKeyDrag iChestKey = arrChestKeys[i];
				iChestKey.SetKeyOn(false);
				scSettings.DraggedKey.SetPos2D(iChestKey.PosKey);
				scSettings.DraggedKey.SetActiveAndType(true, iChestKey.KeyType);

				ChestDrop chestDrop = scSettings.GetChestDrop(iChestKey.KeyType);
				Vector3 posTarget = chestDrop.Position;
				posTarget.z = scSettings.DraggedKey.RectTrans.position.z;

				scSettings.DraggedKey.RectTrans.
						DOMove(posTarget, settings.DurKeyMoveAuto).SetEase(settings.EaseMoveDraggKeyAuto);

				yield return Timing.WaitForSeconds(settings.DurKeyMoveAuto);
				scSettings.DraggedKey.SetActive(false);
				OpenChest(chestDrop, false);

//				if(i != len - 1)
				yield return Timing.WaitForSeconds(settings.WTimeChestOpenAuto);
			}

			scSettings.BtnBack.gameObject.SetActive(true);
			scSettings.BtnBack.DOAnchorPosY(initAnchorPosBtnBack.y,settings.DurBtnBackAppear)
				.SetEase(settings.EaseBtnBackAppear);
			
		}

		private void OpenChest(ChestDrop chest, bool giftMoveToCenter)
		{
			menuHandler.PlayOneShotAuClip(settings.AuChestOpen);
//			scSettings.PSBackLight.Stop();
			scSettings.GetChestKey(chest.KeyType).EnableRayCast(false);

			chest.SetIconActive(false);
			chest.PlayAnim();
			scSettings.PSOpenChest.gameObject.SetActive(true);
			scSettings.PSOpenChest.transform.SetPos2D(chest.Position);
			scSettings.PSOpenChest.Play();

			if(scSettings.PSTrailGift.isPlaying) 
				scSettings.PSTrailGift.Stop();

			Timing.RunCoroutine(IEWaitAndAnimGift(chest.Position, chest.KeyType, giftMoveToCenter));
		}

		private IEnumerator<float> IEScoreCongrats(GameObject gift,float wTime)
		{
			yield return Timing.WaitForSeconds(wTime);
			scSettings.PsScoreCongrats.gameObject.SetActive(true);
			scSettings.PsScoreCongrats.Play();
			gift.SetActive(false);
//			scSettings.GiftImg.DOFade(0, settings.DurGiftFadeTime);
		}
	
		private IEnumerator<float> IEWaitAndAnimGift(Vector3 chestPos, KeyType keyType, bool giftMoveToCenter)
		{
			GiftInChestScene gift = scSettings.GetFreeGiftInScene();
			gift.ImgGift.color = ConstColor.white;
			gift.ImgGift.raycastTarget = false;
			yield return Timing.WaitForSeconds(settings.GiftDelay);
			AnimGift(gift, chestPos, keyType, giftMoveToCenter);
		}

		private void AnimGift(GiftInChestScene gift, Vector3 chestPos, KeyType keyType, bool giftMoveToCenter)
		{
			scSettings.SetGiftActive(true);
			gift.SetActive(true);

			gift.TransGift.SetPos2D(
				chestPos.x - settings.GiftChestOffSet.x, chestPos.y - settings.GiftChestOffSet.y);
			
			gift.TransGift.localScale = settings.ScaleGiftFromChest;

			GiftOutChest rndGift = settings.RndGift();
			if(rndGift == null)
			{
				#if UNITY_EDITOR
				Debug.LogError("GiftOutChest rnd is null".StrColored(DebugColors.red));
				#endif
			}
				

//			Debug.Log("GiftOutChest : ".StrColored(DebugColors.purple).StrBold() + rndGift.PUType);

			DataPrefsSaver.AddOnePowerUp(rndGift.PUType);

			gift.ImgGift.sprite = rndGift.Pic;

			scSettings.PSTrailGift.gameObject.SetActive(true);
			scSettings.PSTrailGift.Play();

			if(giftMoveToCenter)
			{
				switch(keyType)
				{
				case KeyType.Key1:
					MoveGiftByPath(gift.TransGift, waypointsLeft);
					break;

				case KeyType.Key2:
					gift.TransGift.DOMove(
						scSettings.PosGiftPosEndPointer, settings.DurGiftAnim).SetEase(settings.GiftMoveEase);
					break;

				case KeyType.Key3:
					MoveGiftByPath(gift.TransGift, waypointsRight);
					break;

				}
			}

			else
			{
				gift.TransGift.DOMoveY(
					scSettings.PosGiftPosEndPointer.y, settings.DurGiftAnim).SetEase(settings.GiftMoveEase);
			}


			gift.TransGift.DOScale(
				settings.ScaleGiftAuto,settings.DurGiftAnim).SetEase(settings.GiftMoveEase);

			scSettings.SetPosBackLight(gift, 
				new Vector3(gift.TransGift.position.x, scSettings.PosGiftPosEndPointer.y));

			Timing.RunCoroutine(IEWaitAndEnableGiftRaycast(gift.ImgGift, settings.DurGiftAnim));
			Timing.RunCoroutine(IEWaitAndPlayBackLight(gift));
		}

		private IEnumerator<float> IEWaitAndEnableGiftRaycast(Image imgGift, float wTime)
		{		
			yield return Timing.WaitForSeconds(wTime);
			imgGift.raycastTarget = true;
		}

		private IEnumerator<float> IEWaitAndPlayBackLight(GiftInChestScene gift)
		{		
			yield return Timing.WaitForSeconds(settings.WTimeBeforeLightsOn);
			scSettings.SetStartSizeBackLight(gift, settings.SizeAppearL1, settings.SizeAppearL2);		
			scSettings.PlayBlackLight(gift);

			yield return Timing.WaitForSeconds(settings.wTimeBeforeLightsOn2);
			scSettings.SetStartSizeBackLight(gift, settings.SizeNormalL1, settings.SizeNormalL2);
		}	
			
		private void MoveGiftByPath(Transform transGift, Vector3[] path)
		{
			transGift.DOPath(
				path,
				settings.DurGiftAnim,
				PathType.CatmullRom,
				PathMode.Sidescroller2D, 
				10,
				Color.blue).SetEase(settings.GiftMoveEase);
		}

		[System.Serializable]
		public class Settings
		{
			[SerializeField] private Vector3 scaleGiftFromChest;
			[SerializeField] private float giftDelay = 0.5F;
			[SerializeField] private Vector2 giftChestOffSet;
			[Tooltip("Duration of gift animation: from chest to center")]
			[SerializeField] private float durGiftAnim = 1;
			[SerializeField] private float wTimeBeforeLightsOn = 0.1F;

			[Header("Gift tap section")]
			[SerializeField] private float durGiftAfterTap;
			[SerializeField] private float scaleFactorAfterTap;
			[SerializeField] private float durGiftFadeTime = 0.1F;

			[Header("Lights settings")]
			public float wTimeBeforeLightsOn2 = 0.1F;

			[SerializeField] private MinMax sizeAppearL1;
			[SerializeField] private MinMax sizeAppearL2;
			[Space(5)]
			[SerializeField] private MinMax sizeNormal_L1;
			[SerializeField] private MinMax sizeNormal_L2;

			[Space(10)]

			[SerializeField] private Ease giftMoveEase = Ease.InOutQuart;

			[Header("Automatic open settings")]
			[SerializeField] private float durKeyMoveAuto;
			[SerializeField] private float wTimeChestOpenAuto;
			[SerializeField] private Vector3 scaleGiftAuto;
			[SerializeField] private Ease easeMoveDraggKeyAuto;
			[Space(5)]

			[Header("Btn Back settings")]
			[SerializeField] private float durBtnBackAppear;
			[SerializeField] private Ease easeBtnBackAppear;
			[SerializeField] private Vector2 offSetBtnBack;
			[Space(5)]

			[Header("SFX")]
			[SerializeField] private AudioClipHolder auChestOpen;
			[SerializeField] private AudioClipHolder auChestClose;
			[Space(5)]

			[SerializeField] private ChestPicsSettings chestPics;
			[SerializeField] private GiftOutChest[] gifts;

			public float DurBtnBackAppear {  get { return durBtnBackAppear; }}
			public Ease EaseBtnBackAppear {  get { return easeBtnBackAppear; }}
			public Vector2 OffSetBtnBack {  get { return offSetBtnBack; }}

			public float DurKeyMoveAuto {  get { return durKeyMoveAuto; }}
			public float WTimeChestOpenAuto {  get { return wTimeChestOpenAuto; }}
			public Ease EaseMoveDraggKeyAuto {  get { return easeMoveDraggKeyAuto; }}
			public Vector3 ScaleGiftAuto {  get { return scaleGiftAuto; }}

			public GiftOutChest[] Gifts {  get { return gifts; }}
			public float GiftDelay {  get { return giftDelay; }}
			public Vector3 ScaleGiftFromChest {  get { return scaleGiftFromChest; }}
			public Vector2 GiftChestOffSet {  get { return giftChestOffSet; }}

			/// <summary>
			/// Duration of gift animation: from chest to center
			/// </summary>
			/// <value>The dur gift animation.</value>
			public float DurGiftAnim {  get { return durGiftAnim; }}

//			public float DurLightsTurnOn {  get { return durLightsTurnOn; }}
			public float WTimeBeforeLightsOn {  get { return wTimeBeforeLightsOn; }}

			public Ease GiftMoveEase {  get { return giftMoveEase; }}
			public Sprite PicChestClose {  get { return chestPics.PicChestClose; }}

			public float DurGiftAfterTap {  get { return durGiftAfterTap; }}
			public float ScaleFactorAfterTap {  get { return scaleFactorAfterTap; }}
			public float DurGiftFadeTime {  get { return durGiftFadeTime; }}

			public MinMax SizeAppearL1 {  get { return sizeAppearL1; }}
			public MinMax SizeAppearL2 {  get { return sizeAppearL2; }}
			public MinMax SizeNormalL1 {  get { return sizeNormal_L1; }}
			public MinMax SizeNormalL2 {  get { return sizeNormal_L2; }}

			public AudioClipHolder AuChestOpen {  get { return auChestOpen; }}
			public AudioClipHolder AuChestClose {  get { return auChestClose; }}

			/// <summary>
			/// Return random gift based on probability. 
			/// </summary>
			public GiftOutChest RndGift()
			{		
				int len = gifts.Length;

				GiftOutChest rndGift = null;
				int countHelp = 0;
				while(rndGift == null)
				{
					for (int i = 0; i < len; i++) 
					{
						GiftOutChest iGift = gifts[i];
						float rndValue = Random.value;
						//				Debug.Log("irndValue : ".StrColored(DebugColors.purple) + rndValue);
						if(rndValue <= iGift.Probability)
						{
							//					Debug.Log("RndGift ".StrColored(DebugColors.olive) + rndGift.PUType
							//						+ "\n Prob: " + iGift.Probability);
							rndGift = iGift;
						}
					}

					if(countHelp > 50)
					{
						Debug.LogError("Infinite cycled in RndGift()");
						break;
					}
					countHelp ++;
				}

				return rndGift;
			}
		}

		[System.Serializable]
		public class GiftOutChest
		{
			[SerializeField] PowerUpTypes puType;
			[SerializeField] private Sprite pic;
			[Range(0,100) ] 
			[SerializeField] private float probability;

			public PowerUpTypes PUType {  get { return puType; }}
			public Sprite Pic {  get { return pic; }}
			public float Probability {  get { return (probability/100); }}
		}

		[System.Serializable]
		public class SceneSettings
		{
			#region fields
			[SerializeField] private DraggedKey draggedKey;
			[SerializeField] private ParticleSystem psOpenChest;
			[Space(5)]
			[SerializeField] private GameObject giftMask;
			[SerializeField] private RectTransform btnBack;

			[SerializeField] private Text txtKeysQTY;
			[SerializeField] private Text txtStars;

			[SerializeField] private ParticleSystem psTrailGift;
			[SerializeField] private ParticleSystem psScoreCongrats;
			[SerializeField] private Transform giftEndPosPointer;

			[Space(5)]
			[SerializeField] private GiftInChestScene[] giftsInScene;

			[Space(5)]
			[SerializeField] private ChestKeyDrag[] chestKeysDrag;
			[SerializeField] private ChestDrop[] chestDrops;
			#endregion

			#region properties
			public GiftInChestScene[] GiftsInScene {  get { return giftsInScene; }}

			public RectTransform BtnBack {  get { return btnBack; }}
//			public Image[] ImgGifts {  get { return imgGifts; }}

			public DraggedKey DraggedKey {  get { return draggedKey; }}

			public ChestKeyDrag[] ChestKeys {  get { return chestKeysDrag; }}
			public ChestDrop[] ChestDrops {  get { return chestDrops; }}
			public ParticleSystem PSOpenChest {  get { return psOpenChest; }}

			public ParticleSystem PSTrailGift {  get { return psTrailGift; }}
//			public ParticleSystem PSBackLight {  get { return psBackLightParent; }}

			public Text TxtKeysQTY {  get { return txtKeysQTY; }}
			public Text TxtStars {  get { return txtStars; }}

			public Vector3 PosGiftPosEndPointer {  get { return giftEndPosPointer.position; }}
			public ParticleSystem PsScoreCongrats {  get { return psScoreCongrats; }}
			#endregion


			#region methods

			public GiftInChestScene GetGiftInScene(Transform transGift)
			{
				int len = giftsInScene.Length;
				for (int i = 0; i < len; i++)
				{
					GiftInChestScene item = giftsInScene[i];
					if(item.TransGift == transGift)
						return item;
				}

				Debug.LogError("Don't find free image");
				return null;
			}

			public GiftInChestScene GetFreeGiftInScene()
			{
				int len = giftsInScene.Length;
				for (int i = 0; i < len; i++)
				{
					GiftInChestScene item = giftsInScene[i];
					if(!item.IsActive)
						return item;
				}

				Debug.LogError("Don't find free image");
				return null;
			}

			/// <summary>
			/// Enable chest keys, and return array of enabled chest keys
			/// </summary>
			/// <returns>The chest keys.</returns>
			/// <param name="keysEnabled">Keys enabled.</param>
			public ChestKeyDrag[] EnableChestKeys(HashSet<KeyType> keysEnabled)
			{
				if(keysEnabled == null)
				{
					Debug.Log("No catched keys to enable".StrColored(DebugColors.red));
					return null;
				}

				ChestKeyDrag[] keyDrags = new ChestKeyDrag[keysEnabled.Count];
				int i = 0;

				foreach (KeyType iKeyType in keysEnabled)
				{
					ChestKeyDrag iKeyDrag = GetChestKey(iKeyType);
					if(iKeyDrag != null)
					{						
						iKeyDrag.SetKeyOn(true);
						iKeyDrag.EnableRayCast(true);
						keyDrags[i] = iKeyDrag;
						i++;
					}
				}
				return keyDrags;
			}

			public void DisableAllChestKeys()
			{
				int len = chestKeysDrag.Length;
				for (int i = 0; i < len; i++) 
				{
					ChestKeyDrag iChestKey = chestKeysDrag[i];
					iChestKey.SetKeyOn(false);	
					iChestKey.EnableRayCast(false);
				}					
			}

			public ChestKeyDrag GetChestKey(KeyType keyType)
			{
				int len = chestKeysDrag.Length;
				for (int i = 0; i < len; i++) 
				{
					ChestKeyDrag iChestKey = chestKeysDrag[i];
					if(iChestKey.KeyType == keyType)
						return iChestKey;					
				}
				return null;
			}

			public ChestDrop GetChestDrop(KeyType keyType)
			{
				int len = chestDrops.Length;
				for (int i = 0; i < len; i++) 
				{
					ChestDrop iChestDrop = chestDrops[i];
					if(iChestDrop.KeyType == keyType)
						return iChestDrop;					
				}
				return null;
			}
				
			public void SetPicToAllChests(Sprite pic)
			{
				int len = chestDrops.Length;
				for (int i = 0; i < len; i++) 
					chestDrops[i].SetSprite(pic);				
			}

			public void SetGiftActive(bool isActive)
			{
				giftMask.SetActive(isActive);
//				GiftGameObj.SetActive(isActive);
			}

			#region turn ON methods
			public void TurnOnBackLight(GiftInChestScene gift, float duration)
			{
				Debug.Log("TurnOnBackLight".StrColored(DebugColors.blue));
				TurnOnPS(gift.PsBackLight1, duration);
				TurnOnPS(gift.PsBackLight2, duration);
			}

			private void TurnOnPS(ParticleSystem ps, float duration)
			{
				ParticleSystem.MainModule psModule1 = ps.main;
				ParticleSystem.MinMaxGradient ps1Grad = psModule1.startColor;
				ps1Grad = Tools.SetPSGradientAlpha(ps1Grad,1);

				Timing.RunCoroutine(Tools.IEChangeGradient(
					psModule1, ps1Grad.colorMin, ps1Grad.colorMax ,duration)
				);
			}
			#endregion

			#region turn OFF methods
			public void TurnOffBackLight(GiftInChestScene gift)
			{
				Debug.Log("TurnOffBackLight".StrColored(DebugColors.blue));
				TurnOffPS(gift.PsBackLight1);
				TurnOffPS(gift.PsBackLight2);
			}

			private void TurnOffPS(ParticleSystem ps)
			{
				ParticleSystem.MainModule psModule1 = ps.main;
				ParticleSystem.MinMaxGradient ps1Grad = psModule1.startColor;
				psModule1.startColor = Tools.SetPSGradientAlpha(ps1Grad,0);
			}
			#endregion

			public void SetStartSizeBackLight(GiftInChestScene gift, MinMax minMaxL1, MinMax minMaxL2)
			{
//				Debug.Log("SetStartSize".StrColored(DebugColors.purple));
				SetStartSizeBackLight(gift.PsBackLight1, minMaxL1);
				SetStartSizeBackLight(gift.PsBackLight1, minMaxL2);
			}

			private void SetStartSizeBackLight(ParticleSystem ps, MinMax minMax)
			{
				ParticleSystem.MainModule psModule = ps.main;
				psModule.startSize = new ParticleSystem.MinMaxCurve(minMax.MinNumb, minMax.MaxNumb);
			}

			public void SetPosBackLight(GiftInChestScene gift, Vector3 pos)
			{
				gift.PsBackLightParent.transform.SetPos2D(pos);
			}

			public void SetPosBackLight(GiftInChestScene gift, Vector2 pos)
			{
				gift.PsBackLightParent.transform.SetPos2D(pos);
			}

			public void PlayBlackLight(GiftInChestScene gift)
			{
				gift.PsBackLightParent.Play();
			}

			#endregion
		}	

		[System.Serializable]
		public class GiftInChestScene
		{
			[SerializeField] private Image imgGift;
			[SerializeField] private Transform transGift;
			[Space(5)]

			[SerializeField] private ParticleSystem psBackLightParent;
			[SerializeField] private ParticleSystem psBackLight1;
			[SerializeField] private ParticleSystem psBackLight2;

			public Image ImgGift {  get { return imgGift; }}
			public Transform TransGift {  get { return transGift; }}
			public ParticleSystem PsBackLightParent {  get { return psBackLightParent; }}
			public ParticleSystem PsBackLight1 {  get { return psBackLight1; }}
			public ParticleSystem PsBackLight2 {  get { return psBackLight2; }}

			public bool IsActive {  get { return transGift.gameObject.activeInHierarchy; }}

			public void SetActive(bool isActive)
			{
				transGift.gameObject.SetActive(isActive);
			}
		}
	}
}