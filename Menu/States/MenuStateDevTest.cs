﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace Shorka.CellConnect
{
	public class MenuStateDevTest : MenuState 
	{
		private readonly SceneSettings scSettings;
		public MenuStateDevTest(SceneSettings scSettings,MenuHandler menuHandler) : base(menuHandler)
		{
			this.scSettings = scSettings;
		}

		public override void Start()
		{
			menuHandler.scSettings.CanvasDevTest.SetActive(true);
			ShowAllPrefs();

			#region events
			MenuEventsDevPanel.OnLevelAdd += AddLevel;
			MenuEventsDevPanel.OnAddMagicStars += AddMagicStars;
			MenuEventsDevPanel.OnSessions += AddSessions;
			MenuEventsDevPanel.OnAddHighScore += AddHighScore;
			MenuEventsDevPanel.OnMakeHighScoreNull += OnMakeHighScoreNull;
			MenuEventsDevPanel.OnDeleteAll += DeleteAll;
			MenuEventsDevPanel.OnBtnBack += BtnBack;
			#endregion
		}


		public override void Stop()
		{
			menuHandler.scSettings.CanvasDevTest.SetActive(false);

			#region events
			MenuEventsDevPanel.OnLevelAdd -= AddLevel;
			MenuEventsDevPanel.OnAddMagicStars -= AddMagicStars;
			MenuEventsDevPanel.OnSessions -= AddSessions;
			MenuEventsDevPanel.OnAddHighScore -= AddHighScore;
			MenuEventsDevPanel.OnMakeHighScoreNull -= OnMakeHighScoreNull;
			MenuEventsDevPanel.OnDeleteAll -= DeleteAll;
			MenuEventsDevPanel.OnBtnBack -= BtnBack;
			#endregion
		}

		#region On methods
		private void AddLevel(int levelAdd)
		{
			if(DataPrefsSaver.LvlNumber < 0)
				return;
			
			DataPrefsSaver.LvlNumber += levelAdd;
			scSettings.TxtLvlNumb.text = DataPrefsSaver.LvlNumber.ToString();
		}

		private void AddMagicStars(int magicStarsAdd)
		{
			if(DataPrefsSaver.MagicStarsQTY < 0)
				return;
			
			DataPrefsSaver.MagicStarsQTY += magicStarsAdd;
			scSettings.TxtMagicStars.text = DataPrefsSaver.MagicStarsQTY.ToString();
		}

		private void AddSessions(int sessions)
		{
			DataPrefsSaver.Sessions = sessions;
			scSettings.TxtSessions.text = DataPrefsSaver.Sessions.ToString();
		}

		private void AddHighScore(int highScore)
		{
			if(DataPrefsSaver.HighScore < 0)
				return;
			
			DataPrefsSaver.HighScore += highScore;
			scSettings.TxtHighScores.text = DataPrefsSaver.HighScore.ToString();		
		}
		private void OnMakeHighScoreNull()
		{
//			DataPrefsSaver.HighScore = 0;
//			scSettings.TxtHighScores.text = 0;		
		}


		private void DeleteAll()
		{
			DataPrefsSaver.DeleteAll();
			ShowAllPrefs();
		}

		private void BtnBack()
		{
			menuHandler.ChangeState(MenuStates.Settings, MenuStates.DevTest);
		}

		#endregion

		private void ShowAllPrefs()
		{
			scSettings.TxtLvlNumb.text = DataPrefsSaver.LvlNumber.ToString();
			scSettings.TxtHighScores.text = DataPrefsSaver.HighScore.ToString();
			scSettings.TxtMagicStars.text = DataPrefsSaver.MagicStarsQTY.ToString();
			scSettings.TxtSessions.text = DataPrefsSaver.Sessions.ToString();
		}

		[System.Serializable]
		public class SceneSettings
		{
			[SerializeField] private Text txtLvlNumb;
			[SerializeField] private Text txtMagicStars;
			[SerializeField] private Text txtHighScores;
			[SerializeField] private Text txtSessions;

			public Text TxtLvlNumb {  get { return txtLvlNumb; }}
			public Text TxtMagicStars {  get { return txtMagicStars; }}
			public Text TxtHighScores {  get { return txtHighScores; }}
			public Text TxtSessions {  get { return txtSessions; }}
		}
	}
}