﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Shorka.CellConnect
{
	public class MenuStateRestart : MenuState
	{
//		private readonly SceneSettings scSettings;
		public MenuStateRestart(MenuHandler menuHandler): base(menuHandler)
		{
//			scSettings = _scSettings;
		}

		public override void Start()
		{
			menuHandler.canvasFadedCtrl.SetFadeOut();
			menuHandler.canvasFadedCtrl.SetActive(true);
			menuHandler.scSettings.CanvasInGame.SetActive(true);
			menuHandler.scSettings.CanvasRestart.SetActive(true);
			menuHandler.canvasFadedCtrl.AnimFade(true);

			MenuEventsRestartGame.OnBtnNo += OnBtnNo;
			MenuEventsRestartGame.OnBtnYes += OnBtnYes;
		}

		public override void Stop()
		{
			menuHandler.canvasFadedCtrl.AnimFade(false);
//			menuHandler.canvasFadedCtrl.SetActive(false);
			menuHandler.scSettings.CanvasInGame.SetActive(false);
			menuHandler.scSettings.CanvasRestart.SetActive(false);

			MenuEventsRestartGame.OnBtnNo -= OnBtnNo;
			MenuEventsRestartGame.OnBtnYes -= OnBtnYes;
		}

		private void OnBtnYes()
		{
//			Debug.Log("OnBtnYes()".StrColored(DebugColors.blue));
			GameEvents.OnRestartGame(true);
		}

		private void OnBtnNo()
		{
			menuHandler.ChangeState(MenuStates.InGame);
		}


		[Serializable]
		public class SceneSettings
		{
//			[SerializeField] private GameObject canvasRestart;
//
//			public GameObject CanvasRestart {  get { return canvasRestart; }}
		}
	}
}