﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MovementEffects;
using System;
namespace Shorka.CellConnect
{
	public class MenuStatePUTutorial : MenuState
	{
		public static Action<PowerUpTypes, bool> OnEnableBtnPU = delegate (PowerUpTypes puType, bool isOn) { };

		public enum WindowType  {SwipeMore, BtnUse  };

		private readonly SceneSettings scSettings;
		private readonly Settings settings;
		private readonly PowerUpTypes puType;
		private readonly WindowType windowType;
		private readonly Action actionBtnUse;

		public MenuStatePUTutorial(SceneSettings scSettings, Settings settings, MenuHandler menuHandler,
			PowerUpTypes _puType = PowerUpTypes.Undo, WindowType _windowType = WindowType.SwipeMore,
			Action _actionBtnUse = null) : base(menuHandler)
		{
			this.scSettings = scSettings;
			this.settings = settings;
			puType = _puType;
			windowType = _windowType;
			actionBtnUse = _actionBtnUse;
		}

		public override void Start()
		{
			menuHandler.canvasFadedCtrl.SetFadeOut();
			menuHandler.canvasFadedCtrl.SetActive(true);
			menuHandler.canvasFadedCtrl.AnimFade(true);

			menuHandler.scSettings.CanvasInGame.SetActive(true);	

			scSettings.ScrollSnapRect.gameObject.SetActive(true);
			if(scSettings.ScrollSnapRect.IsInited)
				scSettings.ScrollSnapRect.OpenPage(menuHandler.powerUpsCtrl.GetIndexOfPowerUpType(puType));

			else				
				Timing.RunCoroutine(IEOpenPage());

			menuHandler.scSettings.CanvasPUTutorial.SetActive(true);
			DataPrefsSaver.SetPUTutorial(puType,true);

			switch(windowType)
			{
			case WindowType.SwipeMore:
				scSettings.ImgSnapRect.raycastTarget = true;
				scSettings.BtnUSE.SetActive(false);
				scSettings.ContentPageDots.SetActive(true);
				scSettings.LabelSwipeMore.SetActive(true);
				break;

			case WindowType.BtnUse:
				scSettings.ImgSnapRect.raycastTarget = false;
				scSettings.BtnUSE.SetActive(true);
				scSettings.ContentPageDots.SetActive(false);
				scSettings.LabelSwipeMore.SetActive(false);
				break;
			}

			MenuEventsInGame.OnBtnClosePUTutorial += OnBtnClose;
			MenuEventsInGame.OnBtnUSEPUTutorial += OnBtnUSEPU;

			if(windowType == WindowType.SwipeMore)
				ScrollSnapRect.OnLerpToPage += OnLerpToPage;
		}

		public override void Stop()
		{
			menuHandler.canvasFadedCtrl.SetActive(false);
			menuHandler.scSettings.CanvasOutOfPu.SetActive(false);
			menuHandler.scSettings.CanvasPUTutorial.SetActive(false);

			MenuEventsInGame.OnBtnClosePUTutorial -= OnBtnClose;
			MenuEventsInGame.OnBtnUSEPUTutorial -= OnBtnUSEPU;

			if(windowType == WindowType.SwipeMore)
				ScrollSnapRect.OnLerpToPage -= OnLerpToPage;
		}

		private IEnumerator<float> IEOpenPage()
		{
			yield return Timing.WaitForSeconds(0.01F);
			scSettings.ScrollSnapRect.OpenPage(menuHandler.powerUpsCtrl.GetIndexOfPowerUpType(puType));
		}

		#region ON methods
		private void OnBtnClose()
		{
			Debug.Log("[PUTutorial] OnBtnClose".StrColored(DebugColors.navy));
			menuHandler.ChangeState(MenuStates.InGame);
		}

		private void OnBtnUSEPU()
		{
			Debug.Log("[PUTutorial]  OnBtnUSEPU".StrColored(DebugColors.navy));
			menuHandler.ChangeState(MenuStates.InGame);
			actionBtnUse();
			OnEnableBtnPU(puType, true);
		}

		private void OnLerpToPage(int pageNumb)
		{
//			if(pageNumb < 0 || pageNumb >=)
			PowerUp pu = menuHandler.powerUpsCtrl.GetPowerUpByIndex(pageNumb);
			Debug.Log("LerpPutype: " + pu.Type);
			if(pu != null)
				DataPrefsSaver.SetPUTutorial(pu.Type, true);
		}


		#endregion

		[System.Serializable]
		public class SceneSettings
		{
			[SerializeField] private ScrollSnapRect scrollSnapRect;
			[SerializeField] private Image imgSnapRect;

			[SerializeField] private GameObject labelSwipeMore;
			[SerializeField] private GameObject btnUSE;
			[SerializeField] private GameObject contentPageDots;


			public ScrollSnapRect ScrollSnapRect {  get { return scrollSnapRect; }}
			public Image ImgSnapRect {  get { return imgSnapRect; }}

			public GameObject LabelSwipeMore {  get { return labelSwipeMore; }}
			public GameObject BtnUSE {  get { return btnUSE; }}
			public GameObject ContentPageDots {  get { return contentPageDots; }}
		}

		[System.Serializable]
		public class Settings
		{
//			[SerializeField] private PowerUpExplan[] puExplans;

//			public PowerUpExplan[] PUExplans {  get { return puExplans; }}

			/// <summary>
			/// Get string explantion of inputed power-up
			/// </summary>
//			public string GetPUExplan(PowerUpType puType)
//			{
//				int len = puExplans.Length;
//				for (int i = 0; i < len; i++) 
//				{
//					PowerUpExplan item = puExplans[i];
//					if(puType == item.PuType)
//						return item.Explan;
//				}
//				return string.Empty;
//			}
		}

		[System.Serializable]
		public class PowerUpExplan
		{
			[SerializeField] private PowerUpTypes puType;
			[SerializeField] private string explan;

			public PowerUpTypes PuType {  get { return puType; }}
			public string Explan {  get { return explan; }}
		}
	}
}