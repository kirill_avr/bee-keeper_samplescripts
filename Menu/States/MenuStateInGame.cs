﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using DG.Tweening;

namespace Shorka.CellConnect
{
	public class MenuStateInGame : MenuState 
	{
		public static Action<bool> OnBtnSoundClicked = delegate (bool isOn) { };

		#region fields & properties
		private int lastScore;
		private bool canUndo = false;
		private bool isPassAllPuTut = false;
		private PowerUpTypes selPU;
		private Sequence seqCell;
		private Sequence seqActiveMask;

		private PowerUpInGame selUIPU;
		private PowerUpInGame uiPuUndo;
		private PowerUpInGame uiPuSmallBomb;
		private PowerUpInGame uiPuMagic;
		private PowerUpInGame uiPuBigBomb;
		private PowerUpInGame uiPuBear;

		private Image imgUndo; 
		private Vector3 initScaleBtnPu;
		private Vector3 shrinkScaleBtnPu;
		private Vector3 initScaleActiveMask;
		private Vector3 shrinkScaleActiveMask;
		private readonly Vector3 initTxtScoreScale;

		private readonly SceneSettings scSettings;
		private readonly Settings settings;
		private readonly BtnSettingsRollOut btnSettings;
		private readonly ParticleSystemOptimized psScoreCongratsOpt;
		#endregion

		public MenuStateInGame(SceneSettings _scSettings, Settings _settings, MenuHandler menuHandler
			, BtnSettingsRollOut _btnSettings) : base(menuHandler)
		{
			scSettings = _scSettings;
			settings = _settings;
			btnSettings = _btnSettings;
			initTxtScoreScale = scSettings.TxtScore.transform.localScale;

//			if(menuHandler.doSetUpBtnRightInGame)
//			{
//				initScaleActiveMask = scSettings.ActiveMask.localScale;
//				shrinkScaleActiveMask = initScaleActiveMask;
//				shrinkScaleActiveMask.x *= settings.FactorScaleBtnPU;
//				shrinkScaleActiveMask.y *= settings.FactorScaleBtnPU;
//			}


			psScoreCongratsOpt = menuHandler.particleWareH.GetFreeParticleSystem(settings.IdParticleScoreCongr);
		}

		public override void Start()
		{
//			Debug.Log("MenuStateInGame".StrColored(DebugColors.yellow));

//			currLvl = menuHandler.gameInfo.CurrLvl;
			scSettings.ImgSound.sprite = DataPrefsSaver.IsSoundOn  ? settings.SoundOn : settings.SoundOff;

			menuHandler.scSettings.CanvasInGame.SetActive(true);
			menuHandler.canvasFadedCtrl.SetActive(false);

			scSettings.EnableRayCastBtnSettings(true);

			int magStars = menuHandler.gameInfo.GetMagicStarsInGame;
			scSettings.TxtScore.text = magStars.ToString();
			lastScore = magStars;

			uiPuUndo = menuHandler.ScSettingsInGame.GetUIPU(PowerUpTypes.Undo);
			imgUndo = uiPuUndo.Img;

			uiPuSmallBomb = menuHandler.ScSettingsInGame.GetUIPU(PowerUpTypes.SmallBomb);
			uiPuMagic = menuHandler.ScSettingsInGame.GetUIPU(PowerUpTypes.Magic);
			uiPuBigBomb = menuHandler.ScSettingsInGame.GetUIPU(PowerUpTypes.BigBomb);
			uiPuBear = menuHandler.ScSettingsInGame.GetUIPU(PowerUpTypes.Bear);

			EnableUIPU(PowerUpTypes.None,true);
	
//			Debug.Log("yDiff: " + yDiff);
			int lenPU = menuHandler.ScSettingsInGame.PowerUpsInGame.Length;
			for (int i = 0; i < lenPU; i++) 
			{
				PowerUpInGame iPU = menuHandler.ScSettingsInGame.PowerUpsInGame[i];
				if(menuHandler.settings.PuSettingsInLvls.IsAllowedLvlToUsePU(iPU.Type,DataPrefsSaver.LvlNumber))
				{
					iPU.SetLock(false);
					iPU.BlackSquare.gameObject.SetActive(true);
					PutPlusOrTxt(iPU, DataPrefsSaver.GetPowerUpQTY(iPU.Type));
				}
				else
				{
					iPU.SetLock(true);
					iPU.BlackSquare.gameObject.SetActive(false);
				}
			}
				
			OnCanUndo(false);
			psScoreCongratsOpt.parentTrans.SetPos2D(scSettings.TxtScore.transform.position);	
			btnSettings.ChangeState(BtnSettingsRollOut.States.SingleBtnSettings);

			SetUpKeys();
			SetUpBtnsRightPos(menuHandler.ScSettingsInGame.PowerUpsInGame[0]);

			initScaleBtnPu = menuHandler.ScSettingsInGame.PowerUpsInGame[0].Img.transform.localScale;
			shrinkScaleBtnPu = initScaleBtnPu;
			shrinkScaleBtnPu.x *= settings.FactorScaleBtnPU;
			shrinkScaleBtnPu.y *= settings.FactorScaleBtnPU;

//			Vector3 scaleBtnPuInGame = scSettings.PowerUpsInGame[0].Img.transform.localScale;
//			scSettings.ActiveMask.gameObject.SetActive(true);
//			initScaleActiveMask = scSettings.PowerUpsInGame[0].Img.transform.localScale * settings.FactorScaleActiveMaskToBtnPU;
//			scSettings.ActiveMask.localScale = initScaleBtnPu;
//			scSettings.ActiveMask.gameObject.SetActive(false);

			initScaleActiveMask = scSettings.ActiveMask.localScale;
			shrinkScaleActiveMask = initScaleActiveMask;
			shrinkScaleActiveMask.x *= settings.FactorScaleBtnPU;
			shrinkScaleActiveMask.y *= settings.FactorScaleBtnPU;

			isPassAllPuTut = menuHandler.powerUpsCtrl.IsAllPuTutPassed();
//			Debug.Log("isPassAllPuTut: ".StrBold() + isPassAllPuTut);
//			scSettings.BtnsRight[scSettings.BtnsRight.Length - 1].gameObject.SetActive(!isPassAllPuTut);				
			scSettings.BtnHelp.gameObject.SetActive(!isPassAllPuTut);

			#region event listeners
			GameEvents.OnAddScore += OnAddScore;
			GameEvents.OnCatchedKeyItem += OnCatchKeyItem;

			PowerUpsEvents.OnCanUndo += OnCanUndo;
			PowerUpsEvents.OnUsePU += OnUsePU;
			PowerUpsEvents.OnCannotAfford += OnCannotAffordPU;

			MenuEventsInGame.OnBtnHome += OnBtnHome;
			MenuEventsInGame.OnBtnReplay += OnReplay;
			MenuEventsInGame.OnBtnUndo += OnBtnUndo;
			MenuEventsInGame.OnBtnSmallBomb += OnBtnSmallBomb;
			MenuEventsInGame.OnBtnMagic += OnBtnMagic;
			MenuEventsInGame.OnBtnBigBomb += OnBtnBigBomb;
			MenuEventsInGame.OnBtnBear += OnBtnBear;
			MenuEventsInGame.OnBtnSettings += OnBtnSettings;
			MenuEventsInGame.OnBtnSound += OnBtnSound;
			MenuEventsInGame.OnBtnPUHelp += OnBtnPUHelp;

			MenuStatePUTutorial.OnEnableBtnPU += EnableUIPU;

			#if UNITY_EDITOR
			MenuTestEvents.OnOpenPUTutorialWindow += OnOpenPUTutorialWindow;
			#endif
			#endregion
		}

		private void SetUpKeys()
		{
			int len = scSettings.KeysInMenuSett.ArrKeysInMenu.Length;
			for (int i = 0; i < len; i++) 
			{
				KeyInMenu iKeyInMenu = scSettings.KeysInMenuSett.ArrKeysInMenu[i];
				if(menuHandler.gameInfo.CatchedKeys.Contains(iKeyInMenu.Type))
				{
					iKeyInMenu.SetActive(true);
//					iKeyInMenu.SetSprite(settings.KeysWarehouse.GetSpriteKeyGame(iKeyInMenu.Type));
				}
				else iKeyInMenu.SetActive(false);
			}
		}

		private void SetUpBtnsRightPos(PowerUpInGame firstPUInGame)
		{
			if(menuHandler.doSetUpBtnRightInGame)
				return;

			menuHandler.doSetUpBtnRightInGame = true;
			float dist = menuHandler.camCtrl.CameraForeEdgeRight.x - scSettings.AlignedPUpoint.x;

		
			Orientations orien = (dist < 1) ? Orientations.Horizontal : Orientations.Vertical;
			PUBtnInGameOrient puInGameOrient = settings.GetPUBtnSettings(orien);
			scSettings.ActiveMask.SetScaleOneValue(puInGameOrient.ScalePUBtn * settings.FactorScaleActiveMaskToBtnPU);

			Transform panelPU = scSettings.GetPanelForPU(orien);

			menuHandler.ScSettingsInGame.SetParentForBtnsPU(panelPU);
			if(orien == Orientations.Horizontal)
			{
				firstPUInGame.RectTrans.SetAnchor(AnchorPresets.MiddleCenter, puInGameOrient.PosOfFirstBtn);
			}
			else
			{
				firstPUInGame.RectTrans.SetAnchor(AnchorPresets.TopRight,  puInGameOrient.PosOfFirstBtn);
				firstPUInGame.RectTrans.SetPosY(
					(scSettings.AlignedPUpoint.y - Tools.GetWorldSize(firstPUInGame.Img).y/2));
			}
			Debug.Log("ORIEN: ".StrColored(DebugColors.brown) + orien);


			Vector2 fistPos = firstPUInGame.Pos;
			fistPos.x -= puInGameOrient.StepBtwBtns.x;
			fistPos.y -= puInGameOrient.StepBtwBtns.y;
//
			int lenPUs = menuHandler.ScSettingsInGame.PowerUpsInGame.Length;
			for (int i = 0; i < lenPUs; i++) 
			{
				fistPos.x += puInGameOrient.StepBtwBtns.x;
				fistPos.y += puInGameOrient.StepBtwBtns.y;

				Transform iBtnPUInGame = menuHandler.ScSettingsInGame.PowerUpsInGame[i].Img.transform;
				iBtnPUInGame.SetPos2D(fistPos);
				iBtnPUInGame.localScale = new Vector3(puInGameOrient.ScalePUBtn, puInGameOrient.ScalePUBtn, iBtnPUInGame.localScale.z);
//				iBtnPUInGame.SetScaleOneValue(puInGameOrient.ScalePUBtn);
			}

			Vector3 posLast = menuHandler.ScSettingsInGame.PowerUpsInGame[lenPUs-1].Pos;
			if(orien == Orientations.Horizontal)
			{
				menuHandler.ScSettingsInGame.SetParentForBtnsPU(menuHandler.scSettings.CanvasInGame.transform);

				Vector3 posFirst = menuHandler.ScSettingsInGame.PowerUpsInGame[0].Pos;
				float middlePosX = (posFirst.x + posLast.x)/2;
				
				panelPU.SetPosX(middlePosX);
				menuHandler.ScSettingsInGame.SetParentForBtnsPU(panelPU);
				panelPU.SetPosX(0);


				scSettings.BtnHelp.SetPos2D(scSettings.AlignedPUpoint.x + settings.XOffSetBtnHelpWhenHor,
					posFirst.y);
			}

			else if(orien == Orientations.Vertical)
			{
				scSettings.BtnHelp.SetPos2D(posLast.x, posLast.y + puInGameOrient.StepBtwBtns.y);
			}

		}

		public override void Stop()
		{
			menuHandler.scSettings.CanvasInGame.SetActive(false);
			scSettings.EnableRayCastBtnSettings(false);

			if(psScoreCongratsOpt.particleSysMain.isPlaying)
				psScoreCongratsOpt.Stop();

			DOTweenExtraTools.KillSafeSequence(seqCell);
			DOTweenExtraTools.KillSafeSequence(seqActiveMask);

			menuHandler.ScSettingsInGame.PutScaleOnAllPUBtns(initScaleBtnPu);
			scSettings.ActiveMask.localScale = initScaleActiveMask;
			#region event listeners
			GameEvents.OnAddScore -= OnAddScore;
			GameEvents.OnCatchedKeyItem -= OnCatchKeyItem;

			PowerUpsEvents.OnCanUndo -= OnCanUndo;
			PowerUpsEvents.OnUsePU -= OnUsePU;
			PowerUpsEvents.OnCannotAfford -= OnCannotAffordPU;

			MenuEventsInGame.OnBtnSettings -= OnBtnSettings;
			MenuEventsInGame.OnBtnHome -= OnBtnHome;
			MenuEventsInGame.OnBtnReplay -= OnReplay;
			MenuEventsInGame.OnBtnUndo -= OnBtnUndo;
			MenuEventsInGame.OnBtnSmallBomb -= OnBtnSmallBomb;
			MenuEventsInGame.OnBtnMagic -= OnBtnMagic;
			MenuEventsInGame.OnBtnBigBomb -= OnBtnBigBomb;
			MenuEventsInGame.OnBtnBear -= OnBtnBear;
			MenuEventsInGame.OnBtnSound -= OnBtnSound;
			MenuEventsInGame.OnBtnPUHelp -= OnBtnPUHelp;

			MenuStatePUTutorial.OnEnableBtnPU -= EnableUIPU;

			#if UNITY_EDITOR
			MenuTestEvents.OnOpenPUTutorialWindow -= OnOpenPUTutorialWindow;
			#endif
			#endregion
		}
			
		#region Event response methods

		#region On BtnEvents

		private void OnBtnSettings()
		{
//			Debug.Log("OnBtnSettings".StrColored(DebugColors.blue));

			if(btnSettings.CurrState == BtnSettingsRollOut.States.RollOut)
				btnSettings.ChangeState(BtnSettingsRollOut.States.RollIn);

			else
				btnSettings.ChangeState(BtnSettingsRollOut.States.RollOut);
		}

		private void OnBtnHome()
		{
			menuHandler.PlayAuBtnDefault();
			GameEvents.OnHome();
		}			

		private void OnReplay()
		{
			menuHandler.ChangeState(MenuStates.Restart);
		}

		private void OnBtnUndo()
		{			
			Debug.Log("Btn UNDO".StrColored(DebugColors.fuchsia));
			if(!canUndo)
			{
				Debug.Log("Can't undo".StrColored(DebugColors.red));
				return;
			}
				

			BtnPUResponse(PowerUpTypes.Undo, uiPuUndo, PowerUpsEvents.OnClickUndo);
		}

		private void OnBtnSmallBomb()
		{
			BtnPUResponse(PowerUpTypes.SmallBomb, uiPuSmallBomb, PowerUpsEvents.OnClickSmallBomb);
		}

		private void OnBtnMagic()
		{
			Debug.Log("OnBtnMagic ".StrColored(DebugColors.blue));
			BtnPUResponse(PowerUpTypes.Magic, uiPuMagic, PowerUpsEvents.OnClickMagic);
		}

		private void OnBtnBigBomb()
		{
			BtnPUResponse(PowerUpTypes.BigBomb, uiPuBigBomb, PowerUpsEvents.OnClickBigBomb);
		}

		private void OnBtnBear()
		{
			BtnPUResponse(PowerUpTypes.Bear, uiPuBear, PowerUpsEvents.OnClickBear);
		}

		private void OnBtnSound()
		{
			//			Debug.Log("OnBtnSound()".StrColored(DebugColors.blue));
			scSettings.ImgSound.sprite  =  DataPrefsSaver.IsSoundOn  ? settings.SoundOff : settings.SoundOn;

			bool isSoundOn = !DataPrefsSaver.IsSoundOn;
			DataPrefsSaver.IsSoundOn = isSoundOn;
			if(isSoundOn)
				DataPrefsSaver.SoundFactor = settings.SoundFactorAtSoundOn;

			OnBtnSoundClicked(isSoundOn);
		}

		private void OnBtnPUHelp()
		{
			menuHandler.ChangeState(MenuStates.PUTutorial, PowerUpTypes.Undo, MenuStatePUTutorial.WindowType.SwipeMore);
		}

		#endregion

		private void OnAddScore(int addScore)
		{
//			Debug.Log("OnAddScore".StrColored(DebugColors.blue));
			int result = lastScore + addScore;
			if(result == lastScore)
			{
				Debug.Log("addedScore == lastScore ".StrColored(DebugColors.red) + addScore);
				return;	
			}					

			menuHandler.CountCollScore(
				scSettings.TxtScore,
				lastScore, 
				result,
				settings.DurCount
			);
				
			menuHandler.ExecuteSizeUpAnim(
				scSettings.TxtScore.transform, 
				initTxtScoreScale, 
				initTxtScoreScale * settings.CoeffTxtAllScore,
				settings.DurInAnimTxtScore,
				settings.DurOutAnimTxtScore,
				settings.DurInAnimTxtScore + settings.WTimeInAnimMiddle
			);

			psScoreCongratsOpt.parentGameObj.SetActive(true);
			psScoreCongratsOpt.Play();
//			scSettings.PsScoreCongrats.gameObject.SetActive(true);
//			scSettings.PsScoreCongrats.Play();
			lastScore = result;
		}

		private void OnCanUndo(bool _canUndo)
		{
//			Debug.Log("OnCanUndo ".StrColored(DebugColors.blue) + _canUndo);
			canUndo = _canUndo;
			imgUndo.color = canUndo ? settings.ColUndoNormal : settings.ColUndoNOT;
		}

		private void OnUsePU(PowerUpTypes type)
		{
//			Debug.Log("Use PU: ".StrColored(DebugColors.yellow) + type);
			int qty = DataPrefsSaver.GetPowerUpQTY(type) - 1;
			DataPrefsSaver.SetPowerUpQTY(type,qty);
			PowerUpInGame uiPU = menuHandler.ScSettingsInGame.GetUIPU(type);
			PutPlusOrTxt(uiPU, qty);

//			if(type == PowerUpType.Magic || type == PowerUpType.Undo || type == PowerUpType.Bear)
//			{
//				EnableUIPU(type,false);
//			}
			EnableUIPU(type,false);
			OnCanUndo(false);
		}

		private void OnCannotAffordPU(PowerUpTypes type)
		{
			Debug.Log("OnCannotAffordPU ".StrColored(DebugColors.blue) + type);
			ShowOutOfPU(type);
		}

		private void OnCatchKeyItem(KeyType keyType)
		{
			KeyInMenu iKeyInMenu = scSettings.KeysInMenuSett.GetKeyInMenu(keyType);
			iKeyInMenu.SetActive(true);
		}

		private void OnOpenPUTutorialWindow(PowerUpTypes puType)
		{
			Debug.Log("[INGame]_".StrColored(DebugColors.navy) + "OnOpenPUTutorialWindow_" + puType.ToString());
			menuHandler.ChangeState(MenuStates.PUTutorial,puType);
		}

		#endregion

		private void BtnPUResponse(PowerUpTypes puType, PowerUpInGame uiPU, Action act)
		{
//			Debug.Log("BtnPUResponse ".StrColored(DebugColors.purple) + puType);

			if(uiPU.isLocked)
			{
				menuHandler.PlayOneShotAuClip(settings.AuClickOnLock);
				uiPU.AnimateLock(settings.DurShakeBtnPU, settings.ShakeBtnPUSettings);
				menuHandler.ChangeState(MenuStates.OutOfPU, 
					MenuStateOutOf.Case.LockPU,
					menuHandler.powerUpsCtrl.GetPowerUpByType(puType).Name,
					menuHandler.settings.PuSettingsInLvls.GetAllowedLvlToUsePU(puType)
				);
				return;
			}

			// If it has been enabled	
			if(selPU == puType)
			{
				PowerUpsEvents.OnNone();
				EnableUIPU(puType, false);
				return;
			}		

			//Check if power- up is passed, if not all power-ups are passed
//			if(!isPassAllPuTut)
//			{
//				if(!DataPrefsSaver.GetIsPUTutorialPassed(puType))
//				{
//					menuHandler.ChangeState(MenuStates.PUTutorial, puType, MenuStatePUTutorial.WindowType.BtnUse, act);
//					return;
//				}
//			}

			//if player hasn't enought power-ups quantity 
			if(DataPrefsSaver.GetPowerUpQTY(puType) <= 0)
			{
				ShowOutOfPU(puType);
				return;
			}
				
			act();
			EnableUIPU(puType, true);				
		}

		private void ShowOutOfPU(PowerUpTypes type)
		{
			menuHandler.ChangeState(MenuStates.OutOfPU, MenuStateOutOf.Case.OutOfPU, 
				menuHandler.powerUpsCtrl.GetPowerUpByType(type).Name);
		}

		/// <summary>
		/// Enable/Disable selected power-up UI btn. Deselected others
		/// </summary>
		private void EnableUIPU(PowerUpTypes puType, bool doEnable)
		{
//			Debug.Log("EnableUIPU method ".StrColored(DebugColors.purple) + puType + " " + doEnable);
			if(puType == PowerUpTypes.None)
			{
				scSettings.ActiveMask.gameObject.SetActive(false);
//				scSettings.ActiveMask.transform.parent = scSettings.PanelPU; 
				selPU = PowerUpTypes.None;
				DOTweenExtraTools.KillSafeSequence(seqCell);
				DOTweenExtraTools.KillSafeSequence(seqActiveMask);

//				puBtn.DOScale(initScaleBtnPu,settings.DurScaleBackBtnPU);
				return;
			}

			DOTweenExtraTools.KillSafeSequence(seqCell);
			DOTweenExtraTools.KillSafeSequence(seqActiveMask);
//			Transform puInGame = scSettings.GetUIPU(puType).Img.transform;
			Transform puInGame = menuHandler.ScSettingsInGame.GetUIPU(puType).Img.transform;

			//"Disable" case
 			if(!doEnable)
			{
//				Debug.Log("Disable PU: ".StrColored(DebugColors.blue) + puType);
				scSettings.ActiveMask.gameObject.SetActive(false);
				selPU = PowerUpTypes.None;
				puInGame.localScale = initScaleBtnPu;
				return;
			}

			//If is enabled

			selPU = puType;
			scSettings.ActiveMask.position = puInGame.position;
			scSettings.ActiveMask.gameObject.SetActive(true);

			seqCell = DOTweenExtraTools.InfiniteBounceBackSeq(seqCell, puInGame,
				shrinkScaleBtnPu,initScaleBtnPu,
				settings.DurScaleBackBtnPU,settings.DurScaleBackBtnPU2 );

			seqActiveMask = DOTweenExtraTools.InfiniteBounceBackSeq(seqActiveMask, scSettings.ActiveMask, 
				shrinkScaleActiveMask, initScaleActiveMask,
				settings.DurScaleBackBtnPU,settings.DurScaleBackBtnPU2 );
		}
			
		/// <summary>
		/// Manage power-up icon. Put txt or plus according to quantity of power-up
		/// </summary>
		private void PutPlusOrTxt(PowerUpInGame uiPU, int qty)
		{
			if(qty <= 0 )
			{
				uiPU.Plus.gameObject.SetActive(true);
				uiPU.TxtQty.gameObject.SetActive(false);
			}
			else
			{
				uiPU.Plus.gameObject.SetActive(false);
				uiPU.TxtQty.gameObject.SetActive(true);
				uiPU.TxtQty.text = qty.ToString();
			}
		}
			

		[Serializable]
		public class SceneSettings
		{
			[Tooltip("Gets the Text component of game score")]
			[SerializeField] private Text txtScore;
			[SerializeField] private Image imgBtnSettings;
			[SerializeField] private Image iconSound;
			[SerializeField] private Transform btnHelp;

			[SerializeField] private KeysInMenuSettings keysInMenu;
			[SerializeField] private Transform alignedTrans;

			[Space(5)]
			[SerializeField] private Transform activeMask;
//			[SerializeField] private PowerUpInGame[] powerUpsInGame;
			[SerializeField] private PanelOfBtnsPUOrient[] panelsForBtnPU;

			public Text TxtScore {  get { return txtScore; }}
			public Transform BtnHelp {  get { return btnHelp; }}

//			public PowerUpInGame[] PowerUpsInGame {  get { return powerUpsInGame; }}
			public KeysInMenuSettings KeysInMenuSett {  get { return keysInMenu; }}
			public Transform ActiveMask {  get { return activeMask; }}
			public Image ImgSound {	get { return iconSound; } }

			public Vector3 AlignedPUpoint {  get { return alignedTrans.position; }}

			#region methods

			/// <summary>
			/// Set position of active mask and enable it
			/// </summary>
			public void SetPosActiveMask(Vector3 pos)
			{
				activeMask.transform.SetPos2D(pos);
				EnableActiveMask(true);
			}

			public void EnableActiveMask(bool makeActive)
			{
				activeMask.gameObject.SetActive(makeActive);
			}

			public void EnableRayCastBtnSettings(bool doEnable)
			{
				imgBtnSettings.raycastTarget = doEnable;
			}

			public Transform GetPanelForPU(Orientations orient)
			{
				int len = panelsForBtnPU.Length;
				for (int i = 0; i < len; i++) 
				{
					PanelOfBtnsPUOrient item = panelsForBtnPU[i];
					if(item.Orient == orient)
					{
						return item.Panel;
					}
				}
				return null;
			}


			#endregion
		}


		[Serializable]
		public class Settings
		{
			[SerializeField] private float durCount;
			[SerializeField] private Color colUndoNormal;
			[SerializeField] private Color colUndoNOT;
			[SerializeField] private float coeffTxtAllScore = 1.2F;
			[SerializeField] private float durInAnimTxtScore = 0.8F;
			[SerializeField] private float durOutAnimTxtScore = 0.8F;
			[SerializeField] private float wTimeInAnimMiddle = 0.8F;
			[SerializeField] private string idParticleScoreCongr = "ScoreCongratsInGame";
			[Space(5)]
			[Range(0,1)]
			[Tooltip("Sound factor when play enable sound")]
			[SerializeField] private float soundFactorAtSoundOn = 0.7F;
			[SerializeField] private Sprite soundOn;
			[SerializeField] private Sprite soundOff;
			[Space(5)]

			[SerializeField] private PUBtnInGameOrient[] btnsPUSett;

			[Header("Pu btn scale bounce")]
			[SerializeField] private float durScaleBackBtnPU;
			[SerializeField] private float durScaleBackBtnPU2;
			[SerializeField] private float factorScaleBtnPU;
			[SerializeField] private float factorScaleActiveMaskToBtnPU = 1.1F;
			[Space(10)]
			[SerializeField] private float durShakeBtnPU;
			[SerializeField] private ShakeSettings shakeBtnPUSettings;
			[Space(5)]
			[SerializeField] private float xOffSetBtnHelpWhenHor = -0.4F;
			[Space(10)]
			[SerializeField] private KeysWarehouse keysWarehouse;
//			[SerializeField] private PowerUpSettingsInLevels puSettingsInLvls;
			[Space(5)]
			[SerializeField] private AudioClipHolder auClickOnLock;

			public float DurShakeBtnPU {  get { return durShakeBtnPU; }}
			public ShakeSettings ShakeBtnPUSettings {  get { return shakeBtnPUSettings; }}

			public float DurCount {  get { return durCount; }}
			public Color ColUndoNormal {  get { return colUndoNormal; }}
			public Color ColUndoNOT {  get { return colUndoNOT; }}
			public float CoeffTxtAllScore {  get { return coeffTxtAllScore; }}
			public float DurInAnimTxtScore {  get { return durInAnimTxtScore; }}
			public float DurOutAnimTxtScore {  get { return durOutAnimTxtScore; }}
			public float WTimeInAnimMiddle {  get { return wTimeInAnimMiddle; }}

			public float FactorScaleActiveMaskToBtnPU {  get { return factorScaleActiveMaskToBtnPU; }}

			public string IdParticleScoreCongr {  get { return idParticleScoreCongr; }}
//			public string StrIsAvailable {  get { return strIsAvailable; }}

			public KeysWarehouse KeysWarehouse {  get { return keysWarehouse; }}
//			public PowerUpSettingsInLevels PuSettingsInLvls {  get { return puSettingsInLvls; }}

			public AudioClipHolder AuClickOnLock {  get { return auClickOnLock; }}

			public float SoundFactorAtSoundOn {  get { return soundFactorAtSoundOn; }}
			public Sprite SoundOn { get { return soundOn; } }
			public Sprite SoundOff { get { return soundOff; } }

			public float DurScaleBackBtnPU { get { return durScaleBackBtnPU; } }
			public float DurScaleBackBtnPU2 { get { return durScaleBackBtnPU2; } }
			public float FactorScaleBtnPU { get { return factorScaleBtnPU; } }

			public float XOffSetBtnHelpWhenHor { get { return xOffSetBtnHelpWhenHor; } }

			public PUBtnInGameOrient GetPUBtnSettings(Orientations orient)
			{
				int len = btnsPUSett.Length;
				for (int i = 0; i < len; i++)
				{
					PUBtnInGameOrient item = btnsPUSett[i];
					if(item.Orient == orient)
						return item;
				}

				return null;
			}
		}

		[Serializable]
		public class PanelOfBtnsPUOrient
		{
			[SerializeField] private Orientations orient;
			[SerializeField] private Transform panel;

			public Orientations Orient { get { return orient; } }
			public Transform Panel { get { return panel; } }
		}

		[Serializable]
		public class PUBtnInGameOrient
		{
			[SerializeField] private Orientations orient;
			[SerializeField] private float scalePUBtn;
			[SerializeField] private Vector2 stepBtwBtns;
			[SerializeField] private Vector2 posOfFirstBtn;
//			[SerializeField] private Vector3 scaleMask;

			public Orientations Orient { get { return orient; } }
			public float ScalePUBtn { get { return scalePUBtn; } }

			public Vector2 StepBtwBtns { get { return stepBtwBtns; } }
			public Vector2 PosOfFirstBtn { get { return posOfFirstBtn; } }

//			public Vector3 ScaleMask { get { return scaleMask; } }
		}
	}
}