﻿using UnityEngine;
using System.Collections;

namespace Shorka.CellConnect
{
	/// <summary>
	/// All menu states in the game
	/// </summary>
	public enum MenuStates
	{
		Home,
		InGame,
		GameOver,
		Restart,
		Undo,
		Shop,
		OutOfPU,
		Settings,
		Chests,
		Tutorial,
		PUTutorial,
		DevTest,
		ReachLevel,
		None
	}

	/// <summary>
	/// Basic class of all menu states in the game
	/// </summary>
	public abstract class MenuState
	{
		protected MenuHandler menuHandler;

		public MenuState(MenuHandler menuHandler)
		{
			this.menuHandler = menuHandler;
		}
			
		/// <summary>
		/// Execute, when state starts
		/// </summary>
		public abstract void Start();

		/// <summary>
		/// Execute in runtime, when state is on
		/// </summary>
		public virtual void Update()
		{
		}
		/// <summary>
		/// Execute, when state stops and change 
		/// </summary>
		public virtual void Stop()
		{
		}

		/// <summary>
		/// Execute, when scene exits
		/// </summary>
		public virtual void Dispose()
		{
		}
	}
}

