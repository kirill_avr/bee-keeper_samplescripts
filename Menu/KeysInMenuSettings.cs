﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

namespace Shorka.CellConnect
{
	public class KeysInMenuSettings : MonoBehaviour 
	{
		[SerializeField] private KeyInMenu[] keysInMenu;
		private Dictionary<KeyType, KeyInMenu> dicKeysInMenu;
//		private Vector3 keyMenuScale;

		public KeyInMenu[] ArrKeysInMenu {  get { return keysInMenu; }}

		void Awake()
		{
			dicKeysInMenu = keysInMenu.ToDictionary(item => item.Type,
				item => item);

//			keyMenuScale = keysInMenu[0].RectTrans.transform.localScale;
//			Debug.Log("keyMenuScale: " + keyMenuScale);
		}


		public KeyInMenu GetKeyInMenu(KeyType type)
		{
			KeyInMenu keyInMenu;
			if (dicKeysInMenu.TryGetValue(type, out keyInMenu)) // Returns true.
				return keyInMenu;

			//If not return true
			Debug.LogError("Can NOT get value dicKeysInMenu from key: ".StrColored(DebugColors.red) + type );
			return null;
		}
			
	}

	[System.Serializable]
	public class KeyInMenu
	{
		[SerializeField] private KeyType type;
		[SerializeField] private Image img;
		[SerializeField] private RectTransform rectTrans;

		public KeyType Type {  get { return type; }}
//		public Image Img {  get { return img; }}
		public RectTransform RectTrans {  get { return rectTrans; }}

		public void SetSprite(Sprite sprite)
		{
			img.sprite = sprite;
		}

		public void SetActive(bool isActive)
		{
			img.gameObject.SetActive(isActive);
		}
	}
}
