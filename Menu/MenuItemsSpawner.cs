﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Shorka.CellConnect
{
	public class MenuItemsSpawner : IInitializable
	{
		private readonly PUTutorialSceneSettings puTutScSettings;
		private readonly PUTutorialSettings puTutSettings;
		public readonly PoweUpsCtrl powerUpsCtrl;
		public MenuItemsSpawner(PUTutorialSceneSettings _puTutScSettings, PUTutorialSettings _puTutSettings,
			PoweUpsCtrl _powerUpsCtrl)
		{
			puTutScSettings = _puTutScSettings;
			puTutSettings = _puTutSettings;
			powerUpsCtrl= _powerUpsCtrl;
//			SpawnPUTutorial();
		}

		public void Initialize ()
		{
//			Debug.Log("SpawnPUTutorial".StrColored(DebugColors.navy));
//			SpawnPUTutorial();
			SpawnPUTutorial();
		}

		private void SpawnPUTutorial()
		{
			PanelTutorialPU[] panelsPUTut = new PanelTutorialPU[puTutSettings.SpawnQTY + 1];
			
			PanelTutorialPU instPanelsPUTut = new PanelTutorialPU(puTutScSettings.PanelPU, puTutSettings);
			SetUpPUTutorial(instPanelsPUTut, powerUpsCtrl.PowerUps[0].Type );
			panelsPUTut[0] = instPanelsPUTut;

			Vector3 instPanelPos = instPanelsPUTut.panelPU.position;
			Quaternion instPanelRotation = instPanelsPUTut.panelPU.rotation;
			Transform instPanelParent = instPanelsPUTut.panelPU.parent;

			Quaternion instDotRotation = puTutScSettings.PageDot.rotation;
			Transform instDotParent = puTutScSettings.PageDot.parent;

			puTutScSettings.CanvasPuTut.SetActive(true);
			Vector3 lastDotPos = puTutScSettings.PageDot.position;

			for (int i = 0; i < puTutSettings.SpawnQTY; i++)
			{
//				Debug.Log("puTut_i: " + i + "\nlastDotPos.y: " + lastDotPos.y);
				RectTransform spawnedPanelPU = RectTransform.Instantiate(puTutScSettings.PanelPU, 
					instPanelPos, instPanelRotation, instPanelParent);

				PanelTutorialPU iPuTut = new PanelTutorialPU(spawnedPanelPU, puTutSettings);
				SetUpPUTutorial(iPuTut, powerUpsCtrl.PowerUps[i+1].Type);
				panelsPUTut[i+1] = iPuTut;

				lastDotPos.x += puTutSettings.DotsOffSet.x;
//				lastDotPos.y += puTutSettings.DotsOffSet.y;

				 RectTransform.Instantiate(puTutScSettings.PageDot, 
					lastDotPos, instDotRotation, instDotParent);
			}

			puTutScSettings.CanvasPuTut.SetActive(false);
		}

		private void SetUpPUTutorial(PanelTutorialPU panelPuTut, PowerUpTypes puType)
		{
			PowerUp pu = powerUpsCtrl.GetPowerUpByType(puType);
			panelPuTut.imgPU.sprite = pu.PowerUpBigPic;
			panelPuTut.txtPuName.text = pu.Name;
//			panelPuTut.txtPuExplain.text = puTutSettings.GetPUExplan(puType);
			panelPuTut.txtPuExplain.text = puTutSettings.GetPUExplan(puType)
				.Replace(Constants.STR_NEW_LINE_EDITOR, Constants.STR_NEW_LINE);
		}

		[System.Serializable]
		public class PUTutorialSceneSettings
		{
			[SerializeField] private GameObject canvasPuTut;
			[SerializeField] private RectTransform panelPU;
			[SerializeField] private RectTransform pageDot;

			public GameObject CanvasPuTut {	get { return canvasPuTut; } }
			public RectTransform PanelPU {	get { return panelPU; } }
			public RectTransform PageDot {	get { return pageDot; } }
		}

		[System.Serializable]
		public class PUTutorialSettings
		{
			[SerializeField] private int spawnQTY;
			[SerializeField] private string namePuImageObj;
			[SerializeField] private string nameTxtPuNameObj;
			[SerializeField] private string nameTxtExplanObj;
			[SerializeField] private PowerUpExplan[] puExplans;
			[Space(5)]
			[Header("Page Dots")]
			[SerializeField] private Vector2 dotsOffSet;

			public int SpawnQTY {	get { return spawnQTY; } }
			public string NamePuImageObj {	get { return namePuImageObj; } }
			public string NameTxtPuNameObj {	get { return nameTxtPuNameObj; } }
			public string NameTxtExplanObj {	get { return nameTxtExplanObj; } }

			public Vector2 DotsOffSet {	get { return dotsOffSet; } }
//			public PowerUpExplan[] PUExplains {	get { return puExplans; } }

			/// <summary>
			/// Get string explantion of inputed power-up
			/// </summary>
			public string GetPUExplan(PowerUpTypes puType)
			{
				int len = puExplans.Length;
				for (int i = 0; i < len; i++) 
				{
					PowerUpExplan item = puExplans[i];
					if(puType == item.PuType)
						return item.Explan;
				}
				return string.Empty;
			}

			[System.Serializable]
			public class PowerUpExplan
			{
				[SerializeField] private PowerUpTypes puType;
				[SerializeField] private string explan;

				public PowerUpTypes PuType {  get { return puType; }}
				public string Explan {  get { return explan; }}
			}
		}

		public class PanelTutorialPU
		{
			public readonly RectTransform panelPU;
			public readonly Image imgPU;
			public readonly Text txtPuName;
			public readonly Text txtPuExplain;

			public PanelTutorialPU(RectTransform panelPU, PUTutorialSettings puTutSettings)
			{
				this.panelPU = panelPU;

				RectTransform[] children = panelPU.GetComponentsInChildren<RectTransform>();
				int len = children.Length;
				for (int i = 0; i < len; i++) 
				{
					RectTransform item = children[i];
					if(item.name == puTutSettings.NamePuImageObj)
					{
						imgPU = item.GetComponent<Image>();
					}

					else if(item.name == puTutSettings.NameTxtPuNameObj)
					{
						txtPuName = item.GetComponent<Text>();
					}

					else if(item.name == puTutSettings.NameTxtExplanObj)
					{
						txtPuExplain = item.GetComponent<Text>();
					}
				}
			}
		}
	}
}