﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Shorka.CellConnect
{
	public class CanvasFadedBackCtrl 
	{
		private readonly Color colBackOut;
		private readonly Color colBackIn;

		private readonly Settings settings;
		private readonly SceneSettings scSettings;

		public CanvasFadedBackCtrl(SceneSettings scSettings, Settings settings)
		{
			this.scSettings = scSettings;
			this.settings = settings;

			Color backCol = scSettings.Back.color;
			colBackOut = backCol.GetColorAlpha(settings.AlphaBlackOut);
			colBackIn = backCol.GetColorAlpha(settings.AlphaBlackIn);
		}

		public void SetFadeOut()
		{
			scSettings.Back.color = colBackOut;
		}

		public void SetFadeIn()
		{
			scSettings.Back.color = colBackIn;
		}

		public void SetActive(bool isActive)
		{
			scSettings.SetActiveCanvas(isActive);
		}

		public void AnimFade(bool isFadeIn)
		{
			scSettings.Back.DOColor(isFadeIn ? colBackIn : colBackOut, settings.DurBackAnim)
				.SetEase(settings.EaseColorAnim);
		}

		public void AnimFadeIn(float alphaChannel)
		{
			Color col = colBackIn.GetColorAlpha(alphaChannel);
			scSettings.Back.DOColor(col, settings.DurBackAnim)
				.SetEase(settings.EaseColorAnim);
		}

		[System.Serializable]
		public class SceneSettings
		{
			[SerializeField] private GameObject canvas;
			[SerializeField] private Image back;

			public Image Back {	get { return back; } }

			public void SetActiveCanvas(bool isActive)
			{
				canvas.SetActive(isActive);
			}
		}

		[System.Serializable]
		public class Settings
		{			
			[SerializeField] private float durBackAnim;
			[SerializeField] private Ease easeColorAnim;

			[Range(0,1)] [SerializeField] private float alphaBlackIn;
			[Range(0,1)] [SerializeField] private float alphaBlackOut;

			public Ease EaseColorAnim {  get { return easeColorAnim; }}

			public float AlphaBlackIn {  get { return alphaBlackIn; }}
			public float AlphaBlackOut {  get { return alphaBlackOut; }}
			public float DurBackAnim {  get { return durBackAnim; }}
		}
	}
}