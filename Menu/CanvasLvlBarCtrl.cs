﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using MovementEffects;
using Zenject;
using System;
namespace Shorka.CellConnect
{
	public class CanvasLvlBarCtrl : IInitializable, IDisposable
	{
//		private int scoreEnd;
//		private int lvlNumbRight;

//		private readonly string strLevel = "Level ";
		private readonly SceneSettings scSettings;
		private readonly Settings settings;
		private readonly LevelManager lvlManager;

		public CanvasLvlBarCtrl(SceneSettings _scSettings, Settings _settings,
			LevelManager _lvlManager)
		{
			scSettings = _scSettings;
			settings = _settings;
			lvlManager = _lvlManager;
		}

		public void Initialize ()
		{
			#if UNITY_EDITOR
			MenuTestEvents.OnAnimShakeColorBar += OnAnimShakeColorBar;
			#endif
		}

		public void Dispose ()
		{
			#if UNITY_EDITOR
			MenuTestEvents.OnAnimShakeColorBar -= OnAnimShakeColorBar;
			#endif
		}

		//TODO: Make optimization. Send BeeLvl vars to parameters
		public void MakeActiveWithAnim(Vector3 pos, int lvlNumbLeft, int lvlStarsOld, int lvlStarsNew)
		{
			//Set bar at old stars value
			MakeActive(pos, lvlNumbLeft, lvlStarsOld);

			//Then, animate bar fillAmount to the new(just obtained) stars value
			int endScore = GetScoreEnd(lvlNumbLeft, lvlNumbLeft+1);
			float fillAmout = GetFillAmount(lvlStarsNew, endScore);

//			Debug.Log("1_fillAmout: ".StrColored(DebugColors.blue) + fillAmout);
			
			if(fillAmout >= 1)
			{		
				float remain = lvlStarsNew - endScore;
				#if UNITY_EDITOR
				Debug.Log("fillAmout >= 1. Remain: ".StrColored(DebugColors.green) + remain);
				#endif

//				scSettings.ImgBarAmount.DOFillAmount(1,	settings.DurFillLvlBar);
				Timing.RunCoroutine(IEFill(lvlNumbLeft, lvlStarsNew, endScore));
			}

			else
				DoFillAmount(fillAmout);
//			scSettings.ImgBarAmount.DOFillAmount(fillAmout+ settings.OffSetRight,	settings.DurFillLvlBar);
//			
		}

		public void Disable()
		{
			scSettings.SetActiveCanvas(false);
		}

		private void OnAnimShakeColorBar()
		{
			Debug.Log("OnAnimShakeColorBar".StrColored(DebugColors.navy));
			FlashAnimToText(scSettings.TxtLevelNumbLeft);
			FlashAnimToText(scSettings.TxtLevelNumbRight);
		}

		private IEnumerator<float> IEFill(int lvlNumbLeft,int lvlStarsNew, int endScore)
		{
			DoFillAmount(1);
			yield return Timing.WaitForSeconds(settings.DurFillLvlBar);

			float remain = lvlStarsNew - endScore;
			int countHelper = 0;

//			if(remain > 0)
//			{
//
//			}
			
			while(remain > 0)
			{
				Debug.Log("countHelper: ".StrColored(DebugColors.navy) + countHelper + "\n Remain: "+ remain);
				scSettings.ImgBarAmount.fillAmount = 0 + settings.OffSetLeft;
				FlashAnimToText(scSettings.TxtLevelNumbLeft);
				FlashAnimToText(scSettings.TxtLevelNumbRight);

				//Increment levelNumber
				lvlNumbLeft++;
				int lvlNumbRight = lvlNumbLeft+1;
				SetTxtLvlNumbs(lvlNumbLeft, lvlNumbRight);
				DataPrefsSaver.LvlNumber = lvlNumbLeft;

				endScore = GetScoreEnd(lvlNumbLeft, lvlNumbRight);
				float fillAmout = remain/ endScore;
				if(fillAmout >=1) 
					fillAmout = 1;

				yield return Timing.WaitForSeconds(0.01F);

				DoFillAmount(fillAmout);	
				DataPrefsSaver.LvlStars = (int)remain;
				remain -= endScore;

				if(remain <= 0)
					break;

				countHelper++;
				if(countHelper > 50)
				{
					Debug.LogError("Cycle in while more than 50 times");
					break;
				}	
				yield return Timing.WaitForSeconds(settings.DurFillLvlBar);
			}
//			Debug.Log("fillAmout >= 1. Remain: ".StrColored(DebugColors.green) + remain);
			yield break;
		}

		public void MakeActive(Vector3 pos, int lvlNumbLeft, int lvlStars)
		{
//			Debug.Log("LvlPos: " + pos);
			int lvlNumbRight = lvlNumbLeft+1;
			scSettings.SetActiveCanvas(true);
			scSettings.LvlProgressHolder.SetPos2D(pos);
			SetTxtLvlNumbs(lvlNumbLeft, lvlNumbRight);		

			scSettings.ImgBarAmount.fillAmount = GetFillAmount(lvlStars,  GetScoreEnd(lvlNumbLeft, lvlNumbRight)) + settings.OffSetLeft;
//			Debug.Log("0_fillAmout: ".StrColored(DebugColors.blue) + fillAmout);
		}
			

						
		private void DoFillAmount(float value)
		{
			scSettings.ImgBarAmount.DOFillAmount(value + settings.OffSetRight,	settings.DurFillLvlBar);	
		}

		private float GetFillAmount(int lvlStars, int scoreEnd)
		{
			float fillAmout = 0;
			if(lvlStars > 0)
				fillAmout = (float) lvlStars/scoreEnd;

			return fillAmout;
		}

		private void SetTxtLvlNumbs(int lvlNumbLeft, int lvlNumbRight)
		{
			scSettings.TxtLevelNumbLeft.text = lvlNumbLeft.ToString();
			scSettings.TxtLevelNumbRight.text = lvlNumbRight.ToString();
		}			

		/// <summary>
		/// Get score limit between defined levels
		/// </summary>
		private int GetScoreEnd(int lvlLeft, int lvlRight)
		{
			BeeLevel lvlLoadR = lvlManager.GetLvl(lvlRight, lvlLeft);
			if(lvlLoadR == null)
				Debug.LogError("LevelLoad is null. Can't find LeveLoad with lvlNumb: " + lvlRight);

			return lvlLoadR.scoreEnter;
		}

		private void FlashAnimToText(Text txt)
		{
			txt.DOColor(settings.ColorFlash, settings.DurFlashAnim)
				.SetEase(settings.EaseColorAnim, settings.OverShootFlashAnim);

			txt.transform.DOShakeRotation(settings.DurFlashAnim,
				settings.ShakeTxtSett.Strength,
				settings.ShakeTxtSett.Vibrato,
				settings.ShakeTxtSett.Randomness,
				settings.ShakeTxtSett.FadeOut);
		}

		[System.Serializable]
		public class SceneSettings
		{			
			[SerializeField] private GameObject canvas;
			[SerializeField] private Transform lvlProgressHolder;
			[SerializeField] private Image imgBarAmount;
			[SerializeField] private Text txtLevelNumbLeft;
			[SerializeField] private Text txtLevelNumbRight;

			public Transform LvlProgressHolder {	get { return lvlProgressHolder; } }
			public Text TxtLevelNumbLeft {	get { return txtLevelNumbLeft; } }
			public Text TxtLevelNumbRight {	get { return txtLevelNumbRight; } }
			public Image ImgBarAmount {  get { return imgBarAmount; }}

			public void SetActiveCanvas(bool isActive)
			{
				canvas.SetActive(isActive);
			}
		}

		[System.Serializable]
		public class Settings
		{			
			[SerializeField] private float durFillLvlBar;

			[Space(5)]
			[Range(0,1)] [SerializeField] private float offSetLeft;
			[Range(0,1)] [SerializeField] private float offSetRight;
			[Space(5)]
			[SerializeField] private float durFlashAnim;
			[SerializeField] private Color colorFlash = Color.white;
			[SerializeField] private Ease easeColorAnim;
			[SerializeField] private int overShootFlashAnim = 22;
			[Space(5)]
			[SerializeField] private ShakeSettings shakeTxtSett;

			public float DurFillLvlBar {  get { return durFillLvlBar; }}
			public float OffSetLeft {  get { return offSetLeft; }}
			public float OffSetRight {  get { return  1 - offSetRight; }}

			public int OverShootFlashAnim {  get { return  overShootFlashAnim; }}
			public float DurFlashAnim {  get { return  durFlashAnim; }}

			public Color ColorFlash {  get { return colorFlash; }}
			public Ease EaseColorAnim {  get { return easeColorAnim; }}

			public ShakeSettings ShakeTxtSett {  get { return shakeTxtSett; }}
		}
	}
}